#include <iostream>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <cmath>
#include <limits>
#include <TROOT.h>
#include <TStyle.h>

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        std::cout << "Requires 3 parameters\n";
        return 1;
    }

    SetAtlasStyle();
    TCanvas *c1=new TCanvas("c1", "c1", 600, 400);
    TFile *inf = new TFile(argv[1], "READ");
    TTree *tree_data = inf->Get<TTree>("CollectionTree");
    size_t nentries = tree_data->GetEntries();

    bool LOGkey;
    std::string arg1(argv[2]);

    if (arg1=="log") {LOGkey = true;}
    if (arg1=="lin") {LOGkey = false;}

    //setup

    auto n_bin = 400;

    auto names = std::vector<std::string>{"0A","0B","0C","0D","1A","1B","1C","1D","2A","2B","2C","2D","3A","3B","3C","3D"};
    auto channels = std::vector<int>{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};

    //start

    std::vector<float> *toFHit_channel = nullptr, *aFPTrack_xLocal = nullptr, *aFPTrack_yLocal = nullptr, 
                        *toFHit_trainID = nullptr, *toFHit_stationID = nullptr, *aFPTrack_stationID = nullptr, 
                        *toFHit_time = nullptr;

    int runNumber;

    tree_data->SetBranchAddress("ToFHit_channel", &toFHit_channel);
    tree_data->SetBranchAddress("AFPTrack_xLocal", &aFPTrack_xLocal);
    tree_data->SetBranchAddress("AFPTrack_yLocal", &aFPTrack_yLocal);
    tree_data->SetBranchAddress("ToFHit_trainID", &toFHit_trainID);
    tree_data->SetBranchAddress("ToFHit_stationID", &toFHit_stationID);
    tree_data->SetBranchAddress("AFPTrack_stationID", &aFPTrack_stationID);
    tree_data->SetBranchAddress("ToFHit_time", &toFHit_time);
    tree_data->SetBranchAddress("RunNumber", &runNumber);

    std::vector<int> N_sit_tof_A_ON(16, 0), N_sit_tof_C_ON(16, 0), N_sitA(4, 0), N_sitC(4, 0), N_sit_tof_A_OFF(16, 0), N_sit_tof_C_OFF(16, 0);
    std::vector<float> eff_A_ON(16, 0), eff_C_ON(16, 0), eff_A_OFF(16, 0), eff_C_OFF(16, 0);

    TH2D *h2_corr_A = new TH2D("corr_A", "corr_A", n_bin, -19, -1, n_bin, -22, 7);
    TH2D *h2_corr_C = new TH2D("corr_C", "corr_C", n_bin, -19, -1, n_bin, -2, 27);

    for (size_t i = 0; i < nentries; i++)
    //for (size_t i = 0; i < 10; i++)
    {
        auto vecToFA_ON = std::vector<std::vector<float>>(16, std::vector<float>());
        auto vecToFC_ON = std::vector<std::vector<float>>(16, std::vector<float>());
        auto vecToFA_OFF = std::vector<std::vector<float>>(16, std::vector<float>());
        auto vecToFC_OFF = std::vector<std::vector<float>>(16, std::vector<float>());
        auto vecSiTA = std::vector<std::vector<float>>(4, std::vector<float>());
        auto vecSiTC = std::vector<std::vector<float>>(4, std::vector<float>());
        
        if (i % 1000000 == 0)
            std::cout << i/1000000 << " from " << nentries/1000000 << "\n";
        tree_data->GetEntry(i);
        if (toFHit_channel->size() == 0 || aFPTrack_xLocal->size() == 0)
            continue;

        //cut in only 1 track per event
        int sum_tracks_A = 0, sum_tracks_C = 0;
        for (size_t j = 0; j < aFPTrack_stationID->size(); j++)
        {
            if (aFPTrack_stationID->at(j) == 0)
                sum_tracks_A++;
            if (aFPTrack_stationID->at(j) == 3)
                sum_tracks_C++;
        }
        if (sum_tracks_A > 1 || sum_tracks_C > 1)
            continue;

        //main loop
        for (size_t k = 0; k < aFPTrack_stationID->size(); ++k)
        {
            if (aFPTrack_stationID->at(k)==0 && sum_tracks_A==1)
                h2_corr_A->Fill(aFPTrack_xLocal->at(k), aFPTrack_yLocal->at(k));
            if (aFPTrack_stationID->at(k)==3 && sum_tracks_C==1)
                h2_corr_C->Fill(aFPTrack_xLocal->at(k), aFPTrack_yLocal->at(k));
        }

    }
    

    /*
    //write in root file
    TFile *outf = new TFile("out.root", "RECREATE");
    for (size_t general = 0; general < names_for_drawBox.size(); general++)
    {
        hists2d_for_drawBox[general]->SetStats(0);
        hists2d_for_drawBox[general]->Draw("text colz");
        hists2d_for_drawBox[general]->Write();
    }   
    outf->Close();
    */

    auto drawATLASstuff = [&](float label_x, float label_y, std::string side)
    {
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        ATLASLabel_WithoutATLAS(label_x,(label_y+0.11),"Internal");
        latex.DrawLatex(label_x,(label_y),(std::string("Run ") + std::to_string(runNumber)).c_str());
        latex.DrawLatex(label_x,(label_y-0.055),(std::string("Side ") + side).c_str());
        c1->SaveAs((std::string("Sit_xy_corr_") + arg1 + "_" + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    };

    h2_corr_A->GetYaxis()->SetTitle("Y_{track}");
    h2_corr_A->GetXaxis()->SetTitle("X_{track}");
    h2_corr_C->GetYaxis()->SetTitle("Y_{track}");
    h2_corr_C->GetXaxis()->SetTitle("X_{track}");

    // write in pdf
    c1->SaveAs((std::string("Sit_xy_corr_") + arg1 + "_" + std::to_string(runNumber) + std::string(".pdf[")).c_str());

    h2_corr_A->Draw("colz");
    if (LOGkey==1) {gPad->SetLogz();}
    drawATLASstuff(0.19,0.83,"A");
    h2_corr_C->Draw("colz");
    if (LOGkey==1) {gPad->SetLogz();}
    drawATLASstuff(0.19,0.83,"C");  

    c1->SaveAs((std::string("Sit_xy_corr_") + arg1 + "_" + std::to_string(runNumber) + std::string(".pdf]")).c_str());
}