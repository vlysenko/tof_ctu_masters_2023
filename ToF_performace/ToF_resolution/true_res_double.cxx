
#include "matrix.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <cmath>
#include <limits>
#include <TROOT.h>
#include <TStyle.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLatex.h>

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

int main(int argc, char **argv)
{

    SetAtlasStyle();
    TCanvas *c1 = new TCanvas("c1", "c1", 0., 0., 800, 600);

    // setup

    auto n_bin = 200;
    //auto n_bin = 100;

    auto names = std::vector<std::string>{
        "0AB", "0AC", "0AD", "0BC", "0BD", "0CD",
        "1AB", "1AC", "1AD", "1BC", "1BD", "1CD",
        "2AB", "2AC", "2AD", "2BC", "2BD", "2CD",
        "3AB", "3AC", "3AD", "3BC", "3BD", "3CD"};
    
    //for 429027
    auto sigma_A = std::vector<double>{
        31.76, 38.58, 49.22, 30.57, 50.63, 37.45,
        43.84, 56.40, 49.93, 34.71, 39.01, 35.69,
        79.82, 61.24, 53.42, 115.34, 155.56, 62.32,
        58.73, 73.03, 59.89, 70.20, 60.85, 46.77};

    auto error_A = std::vector<double>{
        0.07,  0.07,  0.11,  0.05,  0.07,  0.15,
        0.10,  0.08,  0.13,  0.05,  0.08,  0.05,
        0.14,  0.24,  0.29,  0.16,  0.26,  0.13,
        0.28,  0.35,  0.57,  0.40,  0.43,  0.48};

    auto sigma_C = std::vector<double>{
        32.50, 36.72, 53.46, 27.42, 57.99, 76.59,
        64.08, 48.42, 63.73, 33.37, 36.96, 31.39,
        81.07, 50.23, 45.37, 63.84, 81.32, 48.51,
        49.12, 45.72, 48.31, 41.04, 37.06, 40.13};

    auto error_C = std::vector<double>{
        0.05,  0.07,  0.10,  0.04,  0.14,  0.15,
        0.13,  0.10,  0.10,  0.06,  0.06,  0.08,
        0.12,  0.11,  0.07,  0.15,  0.13,  0.07,
        0.18,  0.17,  0.21,  0.13,  0.26,  0.12};
    
    /*
    //for 428770
    auto sigma_A = std::vector<double>{
        20.62, 36.81, 55.54, 29.26, 36.51, 32.15,
        37.74, 43.49, 49.25, 27.85, 33.52, 30.48,
        45.23, 46.86, 54.33, 53.71, 61.77, 61.18,
        49.61, 53.48, 62.25, 67.42, 51.69, 38.40};

    auto error_A = std::vector<double>{
        0.19,  0.07,  0.19,  1.71,  0.18,  0.12,
        0.18,  0.15,  0.16,  0.09,  0.08,  0.06,
        0.24,  0.20,  0.10,  0.35,  0.18,  0.31,
        0.28,  0.24,  0.23,  0.32,  0.19,  0.38};

    auto sigma_C = std::vector<double>{
        29.40, 36.25, 53.72, 27.12, 36.54, 47.88,
        57.47, 66.68, 60.56, 31.33, 40.06, 18.55,
        89.99, 58.92, 44.66, 78.36, 83.51, 46.82,
        47.63, 48.94, 54.20, 32.66, 41.22, 38.63};

    auto error_C = std::vector<double>{
        0.09,  0.31,  0.41,  0.10,  0.62,  0.17,
        0.34,  0.36,  1.03,  0.20,  0.20,  0.13,
        0.32,  0.28,  0.30,  0.29,  0.27,  0.16,
        0.79,  0.76,  0.60,  0.39,  0.51,  0.14};
    */
    // start
    //int runNumber = 428770;
    int runNumber=429027;

    std::vector<TH1D *> h_means_A(names.size()), h_means_C(names.size());
    for (size_t i = 0; i < 4; i++)
    { 
        h_means_A[i] = new TH1D((std::string("h_means_A_")+std::to_string(i)).c_str(), "", 6, 0, 6);
        h_means_C[i] = new TH1D((std::string("h_means_C_")+std::to_string(i)).c_str(), "", 6, 0, 6);
        h_means_A[i]->GetXaxis()->SetTitle("Channels combination");
        h_means_A[i]->GetYaxis()->SetTitle("Width [ps]");
        h_means_C[i]->GetXaxis()->SetTitle("Channels combination");
        h_means_C[i]->GetYaxis()->SetTitle("Width [ps]");
    }

    for (size_t i = 0; i < names.size(); ++i)
    {
        if (i<6)
        {
            h_means_A[0]->SetBinContent(i+1, sigma_A[i]); 
            h_means_A[0]->SetBinError(i+1, error_A[i]);
            h_means_C[0]->SetBinContent(i+1, sigma_C[i]); 
            h_means_C[0]->SetBinError(i+1, error_C[i]); 
        }
        if (i>5 && i<12)
        {
            h_means_A[1]->SetBinContent(i-5, sigma_A[i]); 
            h_means_A[1]->SetBinError(i-5, error_A[i]);
            h_means_C[1]->SetBinContent(i-5, sigma_C[i]); 
            h_means_C[1]->SetBinError(i-5, error_C[i]); 
        }
        if (i>11 && i<18)
        {
            h_means_A[2]->SetBinContent(i-11, sigma_A[i]); 
            h_means_A[2]->SetBinError(i-11, error_A[i]);
            h_means_C[2]->SetBinContent(i-11, sigma_C[i]); 
            h_means_C[2]->SetBinError(i-11, error_C[i]); 
        }
        if (i>17 && i<24)
        {
            h_means_A[3]->SetBinContent(i-17, sigma_A[i]); 
            h_means_A[3]->SetBinError(i-17, error_A[i]);
            h_means_C[3]->SetBinContent(i-17, sigma_C[i]); 
            h_means_C[3]->SetBinError(i-17, error_C[i]); 
        }
    }  


    auto vecA = std::vector<std::vector<float>>(4, std::vector<float>(4, 0));
    auto vecC = std::vector<std::vector<float>>(4, std::vector<float>(4, 0));

    auto drawATLASstuff = [&](float label_x, float label_y, std::string side, int train)
    {
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        ATLASLabel_WithoutATLAS(label_x,(label_y+0.11),"Internal");
        //ATLASLabel(label_x,(label_y+0.11), "work in progress");
        latex.DrawLatex(label_x,(label_y),(std::string("Run ") + std::to_string(runNumber)).c_str());
        latex.DrawLatex(label_x,(label_y-0.055),(std::string("Side ") + side + std::string(", ToF train ") + std::to_string(train)).c_str());
        c1->SaveAs((std::string("real_resolutions_double_") + side + std::string("_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    };


    c1->SaveAs((std::string("real_resolutions_double_A_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    TF1 * tf_means_A0 = new TF1("tf_means_A0","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_A0->SetParameter(0, 40); 
    tf_means_A0->SetParameter(1, 40); 
    tf_means_A0->SetParameter(2, 40); 
    tf_means_A0->SetParameter(3, 40);
    tf_means_A0->SetNpx(1000);
    tf_means_A0->SetLineColor(2);
    //h_means_A[0]->SetStats(0);
    h_means_A[0]->Fit(tf_means_A0,"R");
    for (size_t i = 0; i < 4; i++) 
        vecA[0][i]=tf_means_A0->GetParameter(i);
    h_means_A[0]->Draw(); 
    tf_means_A0->Draw("same"); 
    drawATLASstuff(0.19,0.82,"A",0);
    
    TF1 * tf_means_A1 = new TF1("tf_means_A1","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_A1->SetParameter(0, 40); 
    tf_means_A1->SetParameter(1, 40); 
    tf_means_A1->SetParameter(2, 40); 
    tf_means_A1->SetParameter(3, 40);
    tf_means_A1->SetNpx(1000);
    tf_means_A1->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_A[1]->Fit(tf_means_A1,"R");
    for (size_t i = 0; i < 4; i++) 
        vecA[1][i]=tf_means_A1->GetParameter(i);
    h_means_A[1]->Draw(); 
    tf_means_A1->Draw("same"); 
    drawATLASstuff(0.7,0.82,"A",1);

    TF1 * tf_means_A2 = new TF1("tf_means_A2","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_A2->SetParameter(0, 40); 
    tf_means_A2->SetParameter(1, 40); 
    tf_means_A2->SetParameter(2, 40); 
    tf_means_A2->SetParameter(3, 40);
    tf_means_A2->SetNpx(1000);
    tf_means_A2->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_A[2]->Fit(tf_means_A2,"R");
    for (size_t i = 0; i < 4; i++) 
        vecA[2][i]=tf_means_A2->GetParameter(i);
    h_means_A[2]->Draw(); 
    tf_means_A2->Draw("same"); 
    drawATLASstuff(0.2,0.82,"A",2);

    TF1 * tf_means_A3 = new TF1("tf_means_A3","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_A3->SetParameter(0, 40); 
    tf_means_A3->SetParameter(1, 40); 
    tf_means_A3->SetParameter(2, 40); 
    tf_means_A3->SetParameter(3, 40);
    tf_means_A3->SetNpx(1000);
    tf_means_A3->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_A[3]->Fit(tf_means_A3,"R");
    for (size_t i = 0; i < 4; i++) 
        vecA[3][i]=tf_means_A3->GetParameter(i);
    h_means_A[3]->Draw(); 
    tf_means_A3->Draw("same"); 
    drawATLASstuff(0.7,0.82,"A",3);
    //drawATLASstuff(0.2,0.25,"A",3);

    c1->SaveAs((std::string("real_resolutions_double_A_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());

    c1->SaveAs((std::string("real_resolutions_double_C_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    TF1 * tf_means_C0 = new TF1("tf_means_C0","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_C0->SetParameter(0, 40); 
    tf_means_C0->SetParameter(1, 40); 
    tf_means_C0->SetParameter(2, 40); 
    tf_means_C0->SetParameter(3, 40);
    tf_means_C0->SetNpx(1000);
    tf_means_C0->SetLineColor(2);
    //h_means_A[0]->SetStats(0);
    h_means_C[0]->Fit(tf_means_C0,"R");
    for (size_t i = 0; i < 4; i++) 
        vecC[0][i]=tf_means_C0->GetParameter(i);
    h_means_C[0]->Draw(); 
    tf_means_C0->Draw("same"); 
    drawATLASstuff(0.19,0.82,"C",0);
    
    TF1 * tf_means_C1 = new TF1("tf_means_C1","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_C1->SetParameter(0, 40); 
    tf_means_C1->SetParameter(1, 40); 
    tf_means_C1->SetParameter(2, 40); 
    tf_means_C1->SetParameter(3, 40);
    tf_means_C1->SetNpx(1000);
    tf_means_C1->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_C[1]->Fit(tf_means_C1,"R");
    for (size_t i = 0; i < 4; i++) 
        vecC[1][i]=tf_means_C1->GetParameter(i);
    h_means_C[1]->Draw(); 
    tf_means_C1->Draw("same"); 
    drawATLASstuff(0.7,0.82,"C",1);

    TF1 * tf_means_C2 = new TF1("tf_means_C2","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_C2->SetParameter(0, 40); 
    tf_means_C2->SetParameter(1, 40); 
    tf_means_C2->SetParameter(2, 40); 
    tf_means_C2->SetParameter(3, 40);
    tf_means_C2->SetNpx(1000);
    tf_means_C2->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_C[2]->Fit(tf_means_C2,"R");
    for (size_t i = 0; i < 4; i++) 
        vecC[2][i]=tf_means_C2->GetParameter(i);
    h_means_C[2]->Draw(); 
    tf_means_C2->Draw("same"); 
    //drawATLASstuff(0.7,0.84,"C",2);
    drawATLASstuff(0.30,0.82,"C",2);

    TF1 * tf_means_C3 = new TF1("tf_means_C3","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_C3->SetParameter(0, 40); 
    tf_means_C3->SetParameter(1, 40); 
    tf_means_C3->SetParameter(2, 40); 
    tf_means_C3->SetParameter(3, 40);
    tf_means_C3->SetNpx(1000);
    tf_means_C3->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_C[3]->Fit(tf_means_C3,"R");
    for (size_t i = 0; i < 4; i++) 
        vecC[3][i]=tf_means_C3->GetParameter(i);
    h_means_C[3]->Draw(); 
    tf_means_C3->Draw("same"); 
    drawATLASstuff(0.7,0.82,"C",3);

    c1->SaveAs((std::string("real_resolutions_double_C_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());

    
    // drawBox with res
    int Nbin_drawBox = 4;
    TCanvas *c2 = new TCanvas("c2", "c2", 400, 400);
    auto trains = std::vector<std::string>{"0", "1", "2", "3"};
    auto bars = std::vector<std::string>{"A", "B", "C", "D"};
    auto names_for_drawBox = std::vector<std::string>{"A", "C"};
    auto good_names_for_drawBox = std::vector<std::string>{"Resolutions, side-A,", "Resolutions, side-C,"};

    std::vector<TH2D *> hists2d_for_drawBox(names_for_drawBox.size());

    for (size_t i = 0; i < names_for_drawBox.size(); i++)
        hists2d_for_drawBox[i] = new TH2D((names_for_drawBox[i] + std::string("_h2d")).c_str(), (names_for_drawBox[i] + std::string("_res_2d")).c_str(), Nbin_drawBox, 0, 4, Nbin_drawBox, 0, 4);

    for (size_t i = 1; i < 5; i++)
        for (size_t j = 1; j < 5; j++)
        {
            float a=100*vecA[i-1][j-1];
            a=round(a);
            a=a/100;
            hists2d_for_drawBox[0]->SetBinContent(j, i, a);
            float b=100*vecC[i-1][j-1];
            b=round(b);
            b=b/100;
            hists2d_for_drawBox[1]->SetBinContent(j, i, b);
        }

    for (size_t general = 0; general < names_for_drawBox.size(); general++)
    {
        for (size_t i = 1; i < 5; i++)
        {
            hists2d_for_drawBox[general]->GetXaxis()->SetBinLabel(i, (bars[i-1]).c_str());
            hists2d_for_drawBox[general]->GetYaxis()->SetBinLabel(i, (trains[i-1]).c_str());
            hists2d_for_drawBox[general]->GetXaxis()->SetLabelSize(0.1);
            hists2d_for_drawBox[general]->GetYaxis()->SetLabelSize(0.1);
            hists2d_for_drawBox[general]->GetXaxis()->SetTitleSize(0.05);
            hists2d_for_drawBox[general]->GetYaxis()->SetTitleSize(0.05);
            hists2d_for_drawBox[general]->GetZaxis()->SetRangeUser(10,100);
            hists2d_for_drawBox[general]->SetMarkerSize(2);
        }
        hists2d_for_drawBox[general]->GetYaxis()->SetTitle("Trains");
        hists2d_for_drawBox[general]->GetXaxis()->SetTitle("Bars");
        hists2d_for_drawBox[general]->SetTitle((good_names_for_drawBox[general] + std::string(" Run ") + std::to_string(runNumber)  + std::string(", [ps]") ).c_str());
    }
    /*
    // write in pdf
    gStyle->SetPalette(55);
    c2->Clear();
    c2->SaveAs((std::string("DrawBox_resolutions_double_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t general = 0; general < names_for_drawBox.size(); general++)
    {
        hists2d_for_drawBox[general]->SetStats(0);
        hists2d_for_drawBox[general]->Draw("text colz");
        c2->SaveAs((std::string("DrawBox_resolutions_double_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c2->Clear();
    }   
    c2->SaveAs((std::string("DrawBox_resolutions_double_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    */
}