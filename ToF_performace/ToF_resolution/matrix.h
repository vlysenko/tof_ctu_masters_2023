
#include <initializer_list>
#include <cstddef>
#include <iostream>
#include <vector>
#include <exception>
#include <stdexcept>

template <class T>
class Matrix : public std::vector<std::vector<T>>
{
public:
    // Constructors
    Matrix(size_t rows, size_t cols, bool fill_default = true)
        : std::vector<std::vector<T>>(rows, std::vector<T>(cols))
    {
        if (rows == 0 || cols == 0)
            throw std::invalid_argument("Matrix size must be non-zero");
        if (fill_default)
            for (auto &row : *this)
                for (auto &elem : row)
                    elem = T();
    }
    Matrix(const std::initializer_list<std::initializer_list<T>>& init)
    {
        for (const auto &row : init)
            this->push_back(std::vector<T>(row));
    }
    Matrix(const std::vector<T>& init): Matrix(1, init.size())
    {
        for(size_t count = 0; count < this->cols(); ++count)
            (*this)[0][count] = init.at(count);
    }

    // const size operations
    size_t rows() const
    {
        return this->size();
    }
    size_t cols() const
    {
        return this->at(0).size();
    }

    // Advanced assesors
    std::vector<T> &row(size_t row)
    {
        return this->at(row);
    }
    const std::vector<T> &row(size_t row) const
    {
        return this->at(row);
    }
    std::vector<T> &col(size_t col)
    {
        std::vector<T> col_vec(this->size());
        for (size_t i = 0; i < this->size(); ++i)
            col_vec[i] = this->at(i).at(col);
        return col_vec;
    }
    const std::vector<T> &col(size_t col) const
    {
        std::vector<T> col_vec(this->size());
        for (size_t i = 0; i < this->size(); ++i)
            col_vec[i] = this->at(i).at(col);
        return col_vec;
    }
    const std::vector<T> extract_by_mask(const Matrix<bool> &mask) const
    {
        std::vector<T> extracted_vec;
        for (size_t i = 0; i < this->size(); ++i)
            for (size_t j = 0; j < this->at(i).size(); ++j)
                if (mask[i][j])
                    extracted_vec.push_back(this->at(i).at(j));
        return extracted_vec;
    }
    void set_by_mask(const std::vector<T> &values, const Matrix<bool> &mask)
    {
        size_t i = 0;
        for (size_t row = 0; row < this->size(); ++row)
            for (size_t col = 0; col < this->at(row).size(); ++col)
                if (mask[row][col])
                    this->at(row).at(col) = values[i++];
        if (i != values.size())
            throw std::invalid_argument("set_by_mask: values size does not match mask size");
    }

    // Reshaper
    Matrix<T> reshape(size_t newrow, size_t newcol) const
    {
        if (newrow * newcol != this->cols() * this->rows())
            throw std::invalid_argument("reshape: cannot perform reshape on provided rows and cols values");
        Matrix<T> newmatr{newrow, newcol, false};
        for (size_t row = 0; row < this->rows(); ++row)
            for (size_t col = 0; col < this->cols(); ++col)
                newmatr[(row * this->cols() + col) / newcol]
                       [(row * this->cols() + col) % newcol] = (*this)[row][col];
            return newmatr;
    }

    // Stream operators
    friend std::ostream &operator<<(std::ostream &os, const Matrix<T> &m)
    {
        os << "Matrix(" << m.rows() << ", " << m.cols() << "):\n";
        for (auto &row : m)
        {
            for (auto &elem : row)
                os << elem << " ";
            os << '\n';
        }
        return os;
    }
    friend std::vector<T> from_matrix_to_vector(const Matrix<T> &m)
    {
        std::vector<T> m_flatten(m.rows()*m.cols());
        for(size_t i = 0; i < m.rows(); ++i)
            for(size_t j = 0; j < m.cols(); ++j)
                m_flatten[i*m.cols()+j] = m[i][j];
        return m_flatten;
    }
};

