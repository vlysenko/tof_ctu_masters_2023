#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <limits>

#include <TRandom3.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TFormula.h>
#include <TF1.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TColor.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TLatex.h>
#include <TMath.h>
#include <TVirtualFFT.h> 

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

//FFT function doesn't work om lxplus, but works on home machine, so on lxplus only after-analysis
void DoFFTstandalone(const char * tag, TH1D * h1_rawtimes, int magcut, double nominalbw, TH1D *& h1_binwidths, TH1D *& h1_bincenters){
        // 1) tag - is just the an optional string that will be used in histogram names' defintions
        //             ROOT does not like the same names of histograms
        // 2) h1_rawtimes is the input histogram (1024, 0, 1024) of raw times
        // 3) magcut - is the index of Fourier expansion which where cut is applied
        //                    the index can be recalculated to frequency or oscillation length value
        //                    see the documentaion in the link below
        // 4) nominalbw - the nominal (design) width of the HPTDC bin 25000 [ps] / 1024
        // 5) h1_binwidths - resulting histogram of extracted widths
        // 6) h2_bincenters - resulting histogram of corrected bin center positions of the HPTDC bins

        // e.g. magcut = 40 corresponds to wave or oscillation length 25000[ps] / 40 = 625 ps in the equidistant
        //        binning, so anything with shorter period than that will not be in the (filtered) backward transform
        //        the backward transform represents a reference distribution from which the binwidths are deduced

	TString hname;
	
	Double_t numberofhistoentires_orig = h1_rawtimes->Integral(); 
	Int_t nHPTDCbins = h1_rawtimes->GetNbinsX();
	
        // FFT .... Compute the transform 
	// https://root.cern.ch/root/html/tutorials/fft/FFT.C.html
	// https://root.cern/doc/master/FFT_8C.html

	TH1 * hm = 0;		
	TVirtualFFT::SetTransform(0);
	hm = h1_rawtimes->FFT(hm, "MAG");
	hname = TString::Format("h1_magnitudes_%s_magcut%d", tag, magcut);
	hm->SetName(hname);
	hm->SetTitle("Magnitude of the 1st transform");
	hm->GetXaxis()->SetTitle("index of harmonics");
	hm->GetYaxis()->SetTitle("mag(coef)");
	
	//////////////////////////////
	// .. use the FFTMAGCUT
	//////////////////////////////
	TVirtualFFT * fft = TVirtualFFT::GetCurrentTransform();
	Int_t n = nHPTDCbins;
	Double_t *re_cut = new Double_t[n];
	Double_t *im_cut = new Double_t[n];
	fft->GetPointsComplex(re_cut,im_cut);
	for(int ipfft = 0; ipfft < n; ++ipfft){
		Double_t re  = re_cut[ipfft];
		Double_t im  = im_cut[ipfft];
		Double_t mag = TMath::Sqrt(re*re+im*im);
		if(ipfft>magcut && ipfft<(n-magcut)){
		  	re = 0;
	  		im = 0;
		}
		re_cut[ipfft] = re;
		im_cut[ipfft] = im;
	}
	// ... Now let's make a backward transform:
	TVirtualFFT *fft_back_cut= TVirtualFFT::FFT(1, &n, "C2R M K");
	fft_back_cut->SetPointsComplex(re_cut,im_cut);
	fft_back_cut->Transform();
	TH1 * hb_cut = 0;
	//Let's look at the output
	hb_cut = TH1::TransformHisto(fft_back_cut,hb_cut,"Re");
	hname = TString::Format("h1_BFFT_%s_magcut%d", tag, magcut);
	hb_cut->SetName(hname);
	hb_cut->SetTitle("The backward transform result with hard CUT");
	// NEW 18.08.2022 ... corrects the normalization
	hb_cut->Scale(1./hb_cut->GetNbinsX()); 
		
	// ... calculate the widths
	if(h1_binwidths){
		std::cout << "DoFFTstandalone ... the output h1_binwidths must be undefined" << std::endl;
		exit(0);
	}
	hname = TString::Format("h1_binwidthsBFFT_%s_magcut%d", tag, magcut);
	h1_binwidths = new TH1D(hname, hname, nHPTDCbins, 0, nHPTDCbins);
	for(int ihptdc = 1; ihptdc <= nHPTDCbins; ++ihptdc){
		double bc = h1_rawtimes->GetBinContent(ihptdc);
		double be = h1_rawtimes->GetBinError  (ihptdc);
		if(bc > 0 && be >= 0){
			h1_binwidths->SetBinContent(ihptdc, bc / hb_cut->GetBinContent(ihptdc));	
			h1_binwidths->SetBinError  (ihptdc, be / hb_cut->GetBinContent(ihptdc));
		}else{
			h1_binwidths->SetBinContent(ihptdc, 1.);
			h1_binwidths->SetBinError  (ihptdc, 0.);
		}
	}
	h1_binwidths->Scale(nominalbw);

	// .. calculate the centers
	if(h1_bincenters){
		std::cout << "DoFFTstandalone ... the output h1_binwidths must be undefined" << std::endl;
		exit(0);
	}
	hname = TString::Format("h1_bincentersBFFT_%s_magcut%d", tag, magcut);
	h1_bincenters = new TH1D(hname, hname, nHPTDCbins, 0, nHPTDCbins);
	double lastlowedge  = 0.;
	for(int ibin = 1; ibin <= h1_binwidths->GetNbinsX(); ++ibin){
		double bwval = h1_binwidths->GetBinContent(ibin);
		double bwerr = h1_binwidths->GetBinError  (ibin);
		double bincenterval = lastlowedge + bwval * 0.5;
		double bincentererr = bwerr * 0.5;
		h1_bincenters->SetBinContent(ibin, bincenterval);
		h1_bincenters->SetBinError  (ibin, bincentererr);
		lastlowedge += bwval;
	}
}

int main(int argc, char **argv)
{
    if (argc != 5)
    {
        std::cout << "Requires 5 parameters\n";
        return 1;
    }

    // setup
    SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c1","Cc1",0.,0.,800,600);
    
    TFile *inf_width = new TFile(argv[1], "READ"); //width produced with FFT function
	TFile *inf_centers = new TFile(argv[2], "READ"); //centers produced with FFT function
	TFile *inf_raw = new TFile(argv[3], "READ"); //raw times before calib, full region, like in centers (can be out of 1-1024)
	TFile *inf = new TFile(argv[4], "READ"); //data file
    TTree *tree_data = inf->Get<TTree>("CollectionTree");
    size_t nentries = tree_data->GetEntries();

	
    auto hists_raw_times = std::vector<std::vector<TH1D *>>(2, std::vector<TH1D *>(16));
    auto stations = std::vector<std::string>{"A", "C"};
    auto channels = std::vector<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

	auto hists_width = std::vector<std::vector<TH1D *>>(2, std::vector<TH1D *>(16));
    auto hists_centers = std::vector<std::vector<TH1D *>>(2, std::vector<TH1D *>(16));
	auto hists_out_raw_times = std::vector<std::vector<TH1D *>>(2, std::vector<TH1D *>(16));

	auto h_deltas_A = std::vector<std::vector<TH1D *>>(4, std::vector<TH1D *>(6));
    auto h_deltas_C = std::vector<std::vector<TH1D *>>(4, std::vector<TH1D *>(6));
	auto combinations = std::vector<std::vector<std::string>>{
        {"0AB", "0AC", "0AD", "0BC", "0BD", "0CD"},
        {"1AB", "1AC", "1AD", "1BC", "1BD", "1CD"},
        {"2AB", "2AC", "2AD", "2BC", "2BD", "2CD"},
        {"3AB", "3AC", "3AD", "3BC", "3BD", "3CD"}};
	
    for (size_t i = 0; i < 2; i++)
        for (size_t j = 0; j < 16; j++)
		{
			hists_width[i][j] = (TH1D*)inf_width->Get(("h1_binwidthsBFFT_" + stations[i] + std::to_string(channels[j]) + "_magcut40").c_str());
			hists_centers[i][j] = (TH1D*)inf_centers->Get(("h1_bincentersBFFT_" + stations[i] + std::to_string(channels[j]) + "_magcut40").c_str());
		}
	for (size_t i = 0; i < 2; i++)
        for (size_t j = 0; j < 16; j++)
            hists_raw_times[i][j] = (TH1D*)inf_raw->Get((std::to_string(channels[j]) + "_" + stations[i] + std::string("_rawTime")).c_str());
	
    for (size_t j = 0; j < 16; j++)
        hists_out_raw_times[0][j] = new TH1D((std::to_string(channels[j]) + "_" + stations[0] + std::string("_out_rawTime")).c_str(), (std::to_string(channels[j]) + "_" + stations[0] + std::string("_out_rawTime")).c_str(), 1200, 0, 1200);

    for (size_t j = 0; j < 16; j++)
        hists_out_raw_times[1][j] = new TH1D((std::to_string(channels[j]) + "_" + stations[1] + std::string("_out_rawTime")).c_str(), (std::to_string(channels[j]) + "_" + stations[1] + std::string("_out_rawTime")).c_str(), 3600, -2400, 1200);

	for (size_t i = 0; i < 4; i++)
        for (size_t j = 0; j < 6; j++)
        {
            h_deltas_A[i][j] = new TH1D((combinations[i][j] + "_A_delta_t").c_str(), (combinations[i][j] + "_A_delta_t").c_str(), 1300, -6500, 6500); //429027
            h_deltas_C[i][j] = new TH1D((combinations[i][j] + "_C_delta_t").c_str(), (combinations[i][j] + "_C_delta_t").c_str(), 3600, -13500, 22500); //429027
            //h_deltas_A[i][j] = new TH1D((combinations[i][j] + "_A_delta_t").c_str(), (combinations[i][j] + "_A_delta_t").c_str(), 700, -4500, 2500); //428770
            //h_deltas_C[i][j] = new TH1D((combinations[i][j] + "_C_delta_t").c_str(), (combinations[i][j] + "_C_delta_t").c_str(), 12000, -60000, 60000); //428770
        }

	std::vector<float> *toFHit_channel = nullptr, *toFHit_bar = nullptr, *aFPTrack_xLocal = nullptr, *toFHit_trainID = nullptr,
                       *toFHit_stationID = nullptr, *aFPTrack_stationID = nullptr, *toFHit_time = nullptr;

    int runNumber;
    
    tree_data->SetBranchAddress("ToFHit_channel", &toFHit_channel);
    tree_data->SetBranchAddress("AFPTrack_xLocal", &aFPTrack_xLocal);
    tree_data->SetBranchAddress("ToFHit_trainID", &toFHit_trainID);
    tree_data->SetBranchAddress("ToFHit_bar", &toFHit_bar);
    tree_data->SetBranchAddress("ToFHit_stationID", &toFHit_stationID);
    tree_data->SetBranchAddress("AFPTrack_stationID", &aFPTrack_stationID);
    tree_data->SetBranchAddress("ToFHit_time", &toFHit_time);
    tree_data->SetBranchAddress("RunNumber", &runNumber);
	TRandom3 * random = new TRandom3();
	
	
	//analysis
	for (size_t i = 0; i < nentries; i++)
    //for (size_t i = 0; i < nentries/100; i++)
    {
        if (i % 1000000 == 0)
            std::cout << i/1000000 << " from " << nentries/1000000 << "\n";
        tree_data->GetEntry(i);

        if (toFHit_channel->size() == 0 || aFPTrack_xLocal->size() == 0)
            continue;

        //cut on only 1 train per event
        std::vector<int> vec_train_A(4, 0), vec_train_C(4, 0);
        for (size_t j = 0; j < toFHit_trainID->size(); j++)
        {
            for (size_t h = 0; h < 4; h++)
            {
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 0)
                    vec_train_A[h] = 1;
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 3)
                    vec_train_C[h] = 1;
            }
        }

        int sum_train_A = 0, sum_train_C = 0;
        for (size_t h = 0; h < 4; h++)
        {
            sum_train_A += vec_train_A[h];
            sum_train_C += vec_train_C[h];
        }

        //cut in only 1 track per event
        int sum_tracks_A = 0, sum_tracks_C = 0;
        for (size_t j = 0; j < aFPTrack_stationID->size(); j++)
        {
            if (aFPTrack_stationID->at(j) == 0)
                sum_tracks_A++;
            if (aFPTrack_stationID->at(j) == 3)
                sum_tracks_C++;
        }
        if (sum_tracks_A > 1 || sum_tracks_C > 1)
            continue;

        //cut on only 1 hit in 1 channel
        auto count_chans_A = std::vector<int>{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        auto count_chans_C = std::vector<int>{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        for (size_t j = 0; j < toFHit_channel->size(); j++)
        {
            if (toFHit_stationID->at(j) == 0)
            {
                size_t k = toFHit_channel->at(j);
                count_chans_A[k]++;
            }
            if (toFHit_stationID->at(j) == 3)
            {
                size_t k = toFHit_channel->at(j);
                count_chans_C[k]++;
            }
        }
        int count_hitsInChans=0;
        for (size_t k = 0; k < 16; k++)              
            if (count_chans_A[k] > 1 || count_chans_C[k] > 1)
                count_hitsInChans++;
        if (count_hitsInChans>0)
            continue;

		auto times_A = std::vector<std::vector<float>>(4, std::vector<float>(4, -10000));
        auto times_C = std::vector<std::vector<float>>(4, std::vector<float>(4, -10000));
		for (size_t j = 0; j < toFHit_channel->size(); j++)
        {
            int chan = (int)toFHit_channel->at(j);
			int train = (int)toFHit_trainID->at(j);
            int bar = (int)toFHit_bar->at(j);
            if (chan==16)
                continue;
            float rawTime=(toFHit_time->at(j))/(25.0/1024.0);
            float rawTimePs=(toFHit_time->at(j))*1000;
			
            if ((rawTime>200 && rawTime<300) || (rawTime>900 && rawTime<1000))
            {
                if ((toFHit_stationID->at(j))==0 && rawTime>900 && rawTime<1000 && sum_tracks_A==1 && sum_train_A==1 && chan < 16)
                {
					//std::cout<<rawTime<<std::endl;
					double uncalibin = hists_raw_times[0][chan]->FindBin(rawTime);
					float bin_center = hists_centers[0][chan]->GetBinContent(uncalibin);
              		float bin_width = hists_width[0][chan]->GetBinContent(uncalibin);
					bin_center = bin_center + random->Uniform(-0.5,0.5)*bin_width;
                    bin_center = bin_center/(25.0/1024.0*1000);
					hists_out_raw_times[0][chan]->Fill(bin_center);
					times_A[train][bar]=bin_center;
                }
                if ((toFHit_stationID->at(j))==3 && rawTime>200 && rawTime<300 && sum_tracks_C==1 && sum_train_C==1 && chan < 16)
                {
                    double uncalibin = hists_raw_times[1][chan]->FindBin(rawTime);
					float bin_center = hists_centers[1][chan]->GetBinContent(uncalibin);
              		float bin_width = hists_width[1][chan]->GetBinContent(uncalibin);
					bin_center = bin_center + random->Uniform(-0.5,0.5)*bin_width;
                    bin_center = bin_center/(25.0/1024.0*1000);
					hists_out_raw_times[1][chan]->Fill(bin_center);
					times_C[train][bar]=bin_center;
                }
				
            }

        }

        //fill histos for resolutions
        for (size_t train = 0; train < 4; ++train)
            for (size_t bar1 = 0; bar1 < 4; ++bar1)
                for (size_t bar2 = 0; bar2 < 4; bar2++)
                    if (bar2>bar1)
                    {
                        int comb = bar1*bar2+bar2-1;
                        if (comb==5) {comb=4;}
                        if (comb==8) {comb=5;}
                        if (times_A[train][bar1]>-10000 && times_A[train][bar2]>-10000)
                        {
                            h_deltas_A[train][comb]->Fill((times_A[train][bar1] - times_A[train][bar2])*(25.0/1024.0)*(-1000));
                        }
                        if (times_C[train][bar1]>-10000 && times_C[train][bar2]>-10000)
                        {
                            h_deltas_C[train][comb]->Fill((times_C[train][bar1] - times_C[train][bar2])*(25.0/1024.0)*(-1000));
                        }   
                    } 

        for (size_t j = 0; j < 4; ++j)
        {
            times_A[j].clear();
            times_C[j].clear();
        } 
	}

	for (size_t i = 0; i < 4; i++)
        for (size_t j = 0; j < 6; j++)
        {
            int bar1=-1, bar2=-1;
            if (j==0) 
            {
                bar1=0;
                bar2=1;
            }
            if (j==1) 
            {
                bar1=0;
                bar2=2;
            }
            if (j==2) 
            {
                bar1=0;
                bar2=3;
            }
            if (j==3) 
            {
                bar1=1;
                bar2=2;
            }
            if (j==4) 
            {
                bar1=1;
                bar2=3;
            }
            if (j==5) 
            {
                bar1=2;
                bar2=3;
            }

            h_deltas_A[i][j]->GetXaxis()->SetTitle("#Delta t [ps]");
            h_deltas_A[i][j]->GetYaxis()->SetTitle("Events");
            h_deltas_A[i][j]->SetFillColor(kBlue);

            h_deltas_C[i][j]->GetXaxis()->SetTitle("#Delta t [ps]");
            h_deltas_C[i][j]->GetYaxis()->SetTitle("Events");
            h_deltas_C[i][j]->SetFillColor(kBlue);
        }
	
	auto ranges = [&](TH1D *hist, int userRangeDown, int userRangeUp)
    {
        hist->GetXaxis()->SetRangeUser(userRangeDown, userRangeUp);
    };

	auto drawATLASstuff = [&](std::string fileName, float label_x, float label_y, std::string side, std::string combination)
    {
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        ATLASLabel_WithoutATLAS(label_x,(label_y+0.11),"Internal");
        latex.DrawLatex(label_x,(label_y),(std::string("Run ") + std::to_string(runNumber)).c_str());
        latex.DrawLatex(label_x,(label_y-0.055),(std::string("Side ") + side + ", " + combination).c_str());
        c1->SaveAs((fileName + "_" + side + "_" + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    };

    auto drawATLASstuff2 = [&](std::string fileName, float label_x, float label_y, std::string side, std::string channel)
    {
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        ATLASLabel_WithoutATLAS(label_x,(label_y+0.11),"Internal");
        latex.DrawLatex(label_x,(label_y),(std::string("Run ") + std::to_string(runNumber)).c_str());
        latex.DrawLatex(label_x,(label_y-0.055),(std::string("Side ") + side + ", " + channel).c_str());
        c1->SaveAs((fileName + "_" + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    };

    /*
    //deltas 429027
    ranges(h_deltas_A[0][0],-100,1400);
    ranges(h_deltas_A[0][1],-500,1000);
    ranges(h_deltas_A[0][2],-500,1000);
    ranges(h_deltas_A[0][3],-1200,300);
    ranges(h_deltas_A[0][4],-1100,400);
    ranges(h_deltas_A[0][5],-800,700);
    ranges(h_deltas_A[1][0],-1600,-100);
    ranges(h_deltas_A[1][1],-1800,-300);
    ranges(h_deltas_A[1][2],-1500,0);
    ranges(h_deltas_A[1][3],-900,600);
    ranges(h_deltas_A[1][4],-700,800);
    ranges(h_deltas_A[1][5],-500,1000);
    ranges(h_deltas_A[2][0],-1600,-100);
    ranges(h_deltas_A[2][1],-900,600);
    ranges(h_deltas_A[2][2],-1000,500);
    ranges(h_deltas_A[2][3],-100,1400);
    ranges(h_deltas_A[2][4],-400,1100);
    ranges(h_deltas_A[2][5],-1100,400);
    ranges(h_deltas_A[3][0],3900,5400);
    ranges(h_deltas_A[3][1],900,2400);
    ranges(h_deltas_A[3][2],-1000,500);
    ranges(h_deltas_A[3][3],-3800,-2300);
    ranges(h_deltas_A[3][4],-5600,-4100);
    ranges(h_deltas_A[3][5],-2600,-1100);

    ranges(h_deltas_C[0][0],-9200,-7700);
    ranges(h_deltas_C[0][1],-10300,-8800);
    ranges(h_deltas_C[0][2],-10000,-8500);
    ranges(h_deltas_C[0][3],-1900,-400);
    ranges(h_deltas_C[0][4],-1700,-200);
    ranges(h_deltas_C[0][5],-500,1000);
    ranges(h_deltas_C[1][0],-1000,500);
    ranges(h_deltas_C[1][1],-600,900);
    ranges(h_deltas_C[1][2],-800,700);
    ranges(h_deltas_C[1][3],-500,1000);
    ranges(h_deltas_C[1][4],-800,700);
    ranges(h_deltas_C[1][5],-1000,500);
    ranges(h_deltas_C[2][0],1000,2500);
    ranges(h_deltas_C[2][1],1700,3200);
    ranges(h_deltas_C[2][2],4000,5500);
    ranges(h_deltas_C[2][3],0,1500);
    ranges(h_deltas_C[2][4],2100,3600);
    ranges(h_deltas_C[2][5],1200,2700);
    ranges(h_deltas_C[3][0],10300,11800);
    ranges(h_deltas_C[3][1],-900,600);
    ranges(h_deltas_C[3][2],19700,21200);
    ranges(h_deltas_C[3][3],-12000,-10500);
    ranges(h_deltas_C[3][4],8700,10200);
    ranges(h_deltas_C[3][5],19800,21300);
    */
    
    //deltas 428770
    ranges(h_deltas_A[0][0],-700,800);
    ranges(h_deltas_A[0][1],-1000,500);
    ranges(h_deltas_A[0][2],-2200,-700);
    ranges(h_deltas_A[0][3],-1100,400);
    ranges(h_deltas_A[0][4],-2300,-800);
    ranges(h_deltas_A[0][5],-2000,-500);
    ranges(h_deltas_A[1][0],-800,700);
    ranges(h_deltas_A[1][1],-1100,400);
    ranges(h_deltas_A[1][2],-1100,400);
    ranges(h_deltas_A[1][3],-1000,500);
    ranges(h_deltas_A[1][4],-1100,400);
    ranges(h_deltas_A[1][5],-800,700);
    ranges(h_deltas_A[2][0],-1400,100);
    ranges(h_deltas_A[2][1],-4000,-2500);
    ranges(h_deltas_A[2][2],-3700,-2200);
    ranges(h_deltas_A[2][3],-3300,-1800);
    ranges(h_deltas_A[2][4],-3000,-1500);
    ranges(h_deltas_A[2][5],-400,1100);
    ranges(h_deltas_A[3][0],-1800,-300);
    ranges(h_deltas_A[3][1],-1200,300);
    ranges(h_deltas_A[3][2],-1100,400);
    ranges(h_deltas_A[3][3],-100,1400);
    ranges(h_deltas_A[3][4],0,1500);
    ranges(h_deltas_A[3][5],-700,800);

    ranges(h_deltas_C[0][0],-800,700);
    ranges(h_deltas_C[0][1],-800,700);
    ranges(h_deltas_C[0][2],-800,700);
    ranges(h_deltas_C[0][3],-800,700);
    ranges(h_deltas_C[0][4],-700,800);
    ranges(h_deltas_C[0][5],-800,700);
    ranges(h_deltas_C[1][0],-1000,500);
    ranges(h_deltas_C[1][1],-54800,-53300);
    ranges(h_deltas_C[1][2],-1200,300);
    ranges(h_deltas_C[1][3],-54600,-53100);
    ranges(h_deltas_C[1][4],-1000,500);
    ranges(h_deltas_C[1][5],53000,54500);
    ranges(h_deltas_C[2][0],-700,800);
    ranges(h_deltas_C[2][1],-300,1200);
    ranges(h_deltas_C[2][2],-1000,500);
    ranges(h_deltas_C[2][3],-500,1000);
    ranges(h_deltas_C[2][4],-1200,300);
    ranges(h_deltas_C[2][5],-1500,0);
    ranges(h_deltas_C[3][0],-1600,-100);
    ranges(h_deltas_C[3][1],-1200,300);
    ranges(h_deltas_C[3][2],-900,600);
    ranges(h_deltas_C[3][3],-400,1100);
    ranges(h_deltas_C[3][4],-300,1200);
    ranges(h_deltas_C[3][5],-700,800);

	//write in pdf
    c1->SaveAs((std::string("hptdc_new_raw_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < 2; i++)
        for (size_t j = 0; j < 16; j++)
        {
            hists_out_raw_times[i][j]->Draw("bar");
            drawATLASstuff2("hptdc_new_raw",0.19,0.83,stations[i],std::to_string(channels[j]));
        }  
    c1->SaveAs((std::string("hptdc_new_raw_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();
    
    //write in root
    TFile *outf_new_raw = new TFile((std::string("hptdc_new_raw_") + std::to_string(runNumber) + std::string(".root")).c_str(), "RECREATE");
    for (size_t i = 0; i < 2; i++)
        for (size_t j = 0; j < 16; j++)
        {
            hists_out_raw_times[i][j]->SetStats(0);
            hists_out_raw_times[i][j]->Draw("bar");
            hists_out_raw_times[i][j]->Write();
        }
    outf_new_raw->Close();
    
    TFile *outf_new_res = new TFile((std::string("hptdc_new_res_close_") + std::to_string(runNumber) + std::string(".root")).c_str(), "RECREATE");
    for (size_t i = 0; i < 4; i++)
        for (size_t j = 0; j < 6; j++)
        {
            h_deltas_A[i][j]->SetStats(0);
            h_deltas_A[i][j]->Draw("bar");
            h_deltas_A[i][j]->Write();

            h_deltas_C[i][j]->SetStats(0);
            h_deltas_C[i][j]->Draw("bar");
            h_deltas_C[i][j]->Write();
        }
    outf_new_res->Close();

	
}