#include <iostream>
#include <iomanip>
#include <sstream>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <cmath>
#include <limits>
#include <TROOT.h>
#include <TStyle.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLatex.h>
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

int main(int argc, char **argv)
{

    if (argc != 2)
    {
        std::cout << "Requires 2 parameters\n";
        return 1;
    }

    SetAtlasStyle();
    TCanvas *c1 = new TCanvas("c1", "c1", 0., 0., 800, 600);
    TFile inf(argv[1]); //delta_t distributions after likelihood, produced in "times_cut.cxx"

    // setup

    auto n_bin = 400;
    //auto n_bin = 100;

    auto names = std::vector<std::string>{
        "0AB", "0AC", "0AD", "0BC", "0BD", "0CD",
        "1AB", "1AC", "1AD", "1BC", "1BD", "1CD",
        "2AB", "2AC", "2AD", "2BC", "2BD", "2CD",
        "3AB", "3AC", "3AD", "3BC", "3BD", "3CD"};

    std::vector<TH1D *> hists_A(names.size()), hists_C(names.size());

    for (size_t i = 0; i < names.size(); i++)
    {
        hists_A[i] = (TH1D *)inf.Get((names[i] + std::string("_A_delta_t_cutted")).c_str());
        hists_C[i] = (TH1D *)inf.Get((names[i] + std::string("_C_delta_t_cutted")).c_str());
    }

    // start
    int runNumber = 428770;
    //int runNumber = 429027;

    std::vector<TH1D *> h_means_A(names.size()), h_means_C(names.size());
    for (size_t i = 0; i < 4; i++)
    { 
        h_means_A[i] = new TH1D((std::string("h_means_A_")+std::to_string(i)).c_str(), "", 6, 0, 6);
        h_means_C[i] = new TH1D((std::string("h_means_C_")+std::to_string(i)).c_str(), "", 6, 0, 6);
        h_means_A[i]->GetXaxis()->SetTitle("Channels combination");
        h_means_A[i]->GetYaxis()->SetTitle("Width [ps]");
        h_means_C[i]->GetXaxis()->SetTitle("Channels combination");
        h_means_C[i]->GetYaxis()->SetTitle("Width [ps]");
    }

    for (size_t i = 0; i < names.size(); ++i)
    {
        TF1* gaus_A = new TF1("gaus_A", "gaus");
        //gaus_A->SetLineColor(2);
        TFitResultPtr r_A = hists_A[i]->Fit("gaus_A","S");
        double sigma_A = r_A->Parameters()[2];
        double error_A = r_A->Errors()[2];

        TF1* gaus_C = new TF1("gaus_C", "gaus");
        //gaus_C->SetLineColor(2);
        TFitResultPtr r_C = hists_C[i]->Fit("gaus_C","S");
        double sigma_C = r_C->Parameters()[2];
        double error_C = r_C->Errors()[2];

        if (i<6)
        {
            h_means_A[0]->SetBinContent(i+1, sigma_A); 
            h_means_A[0]->SetBinError(i+1, error_A);
            h_means_C[0]->SetBinContent(i+1, sigma_C); 
            h_means_C[0]->SetBinError(i+1, error_C); 
        }
        if (i>5 && i<12)
        {
            h_means_A[1]->SetBinContent(i-5, sigma_A); 
            h_means_A[1]->SetBinError(i-5, error_A);
            h_means_C[1]->SetBinContent(i-5, sigma_C); 
            h_means_C[1]->SetBinError(i-5, error_C); 
        }
        if (i>11 && i<18)
        {
            h_means_A[2]->SetBinContent(i-11, sigma_A); 
            h_means_A[2]->SetBinError(i-11, error_A);
            h_means_C[2]->SetBinContent(i-11, sigma_C); 
            h_means_C[2]->SetBinError(i-11, error_C); 
        }
        if (i>17 && i<24)
        {
            h_means_A[3]->SetBinContent(i-17, sigma_A); 
            h_means_A[3]->SetBinError(i-17, error_A);
            h_means_C[3]->SetBinContent(i-17, sigma_C); 
            h_means_C[3]->SetBinError(i-17, error_C); 
        }
    }  

    auto vecA = std::vector<std::vector<float>>(4, std::vector<float>(4, 0));
    auto vecC = std::vector<std::vector<float>>(4, std::vector<float>(4, 0));

    auto drawATLASstuff = [&](float label_x, float label_y, std::string side, int train)
    {
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        ATLASLabel_WithoutATLAS(label_x,(label_y+0.11),"Internal");
        latex.DrawLatex(label_x,(label_y),(std::string("Run ") + std::to_string(runNumber)).c_str());
        latex.DrawLatex(label_x,(label_y-0.055),(std::string("Side ") + side + std::string(", ToF train ") + std::to_string(train)).c_str());
        c1->SaveAs((std::string("real_resolutions_cutted_") + side + std::string("_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    };

    c1->SaveAs((std::string("real_resolutions_cutted_A_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    TF1 * tf_means_A0 = new TF1("tf_means_A0","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_A0->SetParameter(0, 40); 
    tf_means_A0->SetParameter(1, 40); 
    tf_means_A0->SetParameter(2, 40); 
    tf_means_A0->SetParameter(3, 40);
    tf_means_A0->SetNpx(1000);
    tf_means_A0->SetLineColor(2);
    //h_means_A[0]->SetStats(0);
    h_means_A[0]->Fit(tf_means_A0,"R");
    for (size_t i = 0; i < 4; i++) 
        vecA[0][i]=tf_means_A0->GetParameter(i);
    h_means_A[0]->Draw(); 
    tf_means_A0->Draw("same"); 
    drawATLASstuff(0.19,0.82,"A",0);

    TF1 * tf_means_A1 = new TF1("tf_means_A1","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_A1->SetParameter(0, 40); 
    tf_means_A1->SetParameter(1, 40); 
    tf_means_A1->SetParameter(2, 40); 
    tf_means_A1->SetParameter(3, 40);
    tf_means_A1->SetNpx(1000);
    tf_means_A1->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_A[1]->Fit(tf_means_A1,"R");
    for (size_t i = 0; i < 4; i++) 
        vecA[1][i]=tf_means_A1->GetParameter(i);
    h_means_A[1]->Draw(); 
    tf_means_A1->Draw("same"); 
    drawATLASstuff(0.7,0.82,"A",1);

    TF1 * tf_means_A2 = new TF1("tf_means_A2","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_A2->SetParameter(0, 40); 
    tf_means_A2->SetParameter(1, 40); 
    tf_means_A2->SetParameter(2, 40); 
    tf_means_A2->SetParameter(3, 40);
    tf_means_A2->SetNpx(1000);
    tf_means_A2->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_A[2]->Fit(tf_means_A2,"R");
    for (size_t i = 0; i < 4; i++) 
        vecA[2][i]=tf_means_A2->GetParameter(i);
    h_means_A[2]->Draw(); 
    tf_means_A2->Draw("same"); 
    drawATLASstuff(0.2,0.82,"A",2);

    TF1 * tf_means_A3 = new TF1("tf_means_A3","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_A3->SetParameter(0, 40); 
    tf_means_A3->SetParameter(1, 40); 
    tf_means_A3->SetParameter(2, 40); 
    tf_means_A3->SetParameter(3, 40);
    tf_means_A3->SetNpx(1000);
    tf_means_A3->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_A[3]->Fit(tf_means_A3,"R");
    for (size_t i = 0; i < 4; i++) 
        vecA[3][i]=tf_means_A3->GetParameter(i);
    h_means_A[3]->Draw(); 
    tf_means_A3->Draw("same"); 
    drawATLASstuff(0.7,0.82,"A",3);

    c1->SaveAs((std::string("real_resolutions_cutted_A_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());


    c1->SaveAs((std::string("real_resolutions_cutted_C_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    TF1 * tf_means_C0 = new TF1("tf_means_C0","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_C0->SetParameter(0, 40); 
    tf_means_C0->SetParameter(1, 40); 
    tf_means_C0->SetParameter(2, 40); 
    tf_means_C0->SetParameter(3, 40);
    tf_means_C0->SetNpx(1000);
    tf_means_C0->SetLineColor(2);
    //h_means_A[0]->SetStats(0);
    h_means_C[0]->Fit(tf_means_C0,"R");
    for (size_t i = 0; i < 4; i++) 
        vecC[0][i]=tf_means_C0->GetParameter(i);
    h_means_C[0]->Draw(); 
    tf_means_C0->Draw("same"); 
    drawATLASstuff(0.19,0.82,"C",0);
    
    TF1 * tf_means_C1 = new TF1("tf_means_C1","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_C1->SetParameter(0, 40); 
    tf_means_C1->SetParameter(1, 40); 
    tf_means_C1->SetParameter(2, 40); 
    tf_means_C1->SetParameter(3, 40);
    tf_means_C1->SetNpx(1000);
    tf_means_C1->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_C[1]->Fit(tf_means_C1,"R");
    for (size_t i = 0; i < 4; i++) 
        vecC[1][i]=tf_means_C1->GetParameter(i);
    h_means_C[1]->Draw(); 
    tf_means_C1->Draw("same"); 
    drawATLASstuff(0.7,0.82,"C",1);

    TF1 * tf_means_C2 = new TF1("tf_means_C2","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_C2->SetParameter(0, 40); 
    tf_means_C2->SetParameter(1, 40); 
    tf_means_C2->SetParameter(2, 40); 
    tf_means_C2->SetParameter(3, 40);
    tf_means_C2->SetNpx(1000);
    tf_means_C2->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_C[2]->Fit(tf_means_C2,"R");
    for (size_t i = 0; i < 4; i++) 
        vecC[2][i]=tf_means_C2->GetParameter(i);
    h_means_C[2]->Draw(); 
    tf_means_C2->Draw("same"); 
    drawATLASstuff(0.3,0.82,"C",2);

    TF1 * tf_means_C3 = new TF1("tf_means_C3","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_C3->SetParameter(0, 40); 
    tf_means_C3->SetParameter(1, 40); 
    tf_means_C3->SetParameter(2, 40); 
    tf_means_C3->SetParameter(3, 40);
    tf_means_C3->SetNpx(1000);
    tf_means_C3->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_C[3]->Fit(tf_means_C3,"R");
    for (size_t i = 0; i < 4; i++) 
        vecC[3][i]=tf_means_C3->GetParameter(i);
    h_means_C[3]->Draw(); 
    tf_means_C3->Draw("same"); 
    drawATLASstuff(0.7,0.83,"C",3);

    c1->SaveAs((std::string("real_resolutions_cutted_C_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    
    // drawBox with res
    int Nbin_drawBox = 4;
    TCanvas *c2 = new TCanvas("c2", "c2", 400, 400);
    auto trains = std::vector<std::string>{"0", "1", "2", "3"};
    auto bars = std::vector<std::string>{"A", "B", "C", "D"};
    auto names_for_drawBox = std::vector<std::string>{"A", "C"};
    auto good_names_for_drawBox = std::vector<std::string>{"Resolutions, side-A,", "Resolutions, side-C,"};

    std::vector<TH2D *> hists2d_for_drawBox(names_for_drawBox.size());

    for (size_t i = 0; i < names_for_drawBox.size(); i++)
        hists2d_for_drawBox[i] = new TH2D((names_for_drawBox[i] + std::string("_h2d")).c_str(), (names_for_drawBox[i] + std::string("_res_2d")).c_str(), Nbin_drawBox, 0, 4, Nbin_drawBox, 0, 4);

    for (size_t i = 1; i < 5; i++)
        for (size_t j = 1; j < 5; j++)
        {
            float a=100*vecA[i-1][j-1];
            a=round(a);
            a=a/100;
            hists2d_for_drawBox[0]->SetBinContent(j, i, a);
            float b=100*vecC[i-1][j-1];
            b=round(b);
            b=b/100;
            hists2d_for_drawBox[1]->SetBinContent(j, i, b);
        }

    for (size_t general = 0; general < names_for_drawBox.size(); general++)
    {
        for (size_t i = 1; i < 5; i++)
        {
            hists2d_for_drawBox[general]->GetXaxis()->SetBinLabel(i, (bars[i-1]).c_str());
            hists2d_for_drawBox[general]->GetYaxis()->SetBinLabel(i, (trains[i-1]).c_str());
            hists2d_for_drawBox[general]->GetXaxis()->SetLabelSize(0.1);
            hists2d_for_drawBox[general]->GetYaxis()->SetLabelSize(0.1);
            hists2d_for_drawBox[general]->GetXaxis()->SetTitleSize(0.05);
            hists2d_for_drawBox[general]->GetYaxis()->SetTitleSize(0.05);
            hists2d_for_drawBox[general]->GetZaxis()->SetRangeUser(10,100);
            hists2d_for_drawBox[general]->SetMarkerSize(2);
        }
        hists2d_for_drawBox[general]->GetYaxis()->SetTitle("Trains");
        hists2d_for_drawBox[general]->GetXaxis()->SetTitle("Bars");
        hists2d_for_drawBox[general]->SetTitle((good_names_for_drawBox[general] + std::string(" Run ") + std::to_string(runNumber)  + std::string(", [ps]") ).c_str());
    }
    /*
    // write in pdf
    gStyle->SetPalette(55);
    c2->Clear();
    c2->SaveAs((std::string("DrawBox_resolutions_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t general = 0; general < names_for_drawBox.size(); general++)
    {
        hists2d_for_drawBox[general]->SetStats(0);
        hists2d_for_drawBox[general]->Draw("text colz");
        c2->SaveAs((std::string("DrawBox_resolutions_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c2->Clear();
    }   
    c2->SaveAs((std::string("DrawBox_resolutions_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    */
}