#include <iostream>
#include <iomanip>
#include <sstream>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <cmath>
#include <limits>
#include <TROOT.h>
#include <TStyle.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TRandom3.h>
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"


int main(int argc, char **argv)
{

    if (argc != 6)
    {
        std::cout << "Requires 6 parameters\n";
        return 1;
    }

    SetAtlasStyle();
    TCanvas *c1 = new TCanvas("c1", "c1", 0., 0., 800, 600);

    TFile *inf_width = new TFile(argv[1], "READ"); //width produced with FFT function
	TFile *inf_centers = new TFile(argv[2], "READ"); //centers produced with FFT function
	TFile *inf_raw = new TFile(argv[3], "READ"); //raw times before calib, full region, like in centers (can be out of 1-1024)
    TFile *inf_res = new TFile(argv[4], "READ"); //delta_t distributions before likelihood, produced in "hptdc.cxx"
	TFile *inf = new TFile(argv[5], "READ"); //data file
    TTree *tree_data = inf->Get<TTree>("CollectionTree");
    size_t nentries = tree_data->GetEntries();

    // setup
    auto n_bin = 500;
    //auto n_bin = 100;

    auto names = std::vector<std::string>{
        "0AB", "0AC", "0AD", "0BC", "0BD", "0CD",
        "1AB", "1AC", "1AD", "1BC", "1BD", "1CD",
        "2AB", "2AC", "2AD", "2BC", "2BD", "2CD",
        "3AB", "3AC", "3AD", "3BC", "3BD", "3CD"};

    auto hists_raw_times = std::vector<std::vector<TH1D *>>(2, std::vector<TH1D *>(16));
    auto stations = std::vector<std::string>{"A", "C"};
    auto channels = std::vector<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

	auto hists_width = std::vector<std::vector<TH1D *>>(2, std::vector<TH1D *>(16));
    auto hists_centers = std::vector<std::vector<TH1D *>>(2, std::vector<TH1D *>(16));

	auto h_deltas_A = std::vector<std::vector<TH1D *>>(4, std::vector<TH1D *>(6));
    auto h_deltas_C = std::vector<std::vector<TH1D *>>(4, std::vector<TH1D *>(6));
    auto h_deltas_out_A = std::vector<std::vector<TH1D *>>(4, std::vector<TH1D *>(6));
    auto h_deltas_out_C = std::vector<std::vector<TH1D *>>(4, std::vector<TH1D *>(6));
	auto combinations = std::vector<std::vector<std::string>>{
        {"0AB", "0AC", "0AD", "0BC", "0BD", "0CD"},
        {"1AB", "1AC", "1AD", "1BC", "1BD", "1CD"},
        {"2AB", "2AC", "2AD", "2BC", "2BD", "2CD"},
        {"3AB", "3AC", "3AD", "3BC", "3BD", "3CD"}};
	
    for (size_t i = 0; i < 2; i++)
        for (size_t j = 0; j < 16; j++)
		{
			hists_width[i][j] = (TH1D*)inf_width->Get(("h1_binwidthsBFFT_" + stations[i] + std::to_string(channels[j]) + "_magcut40").c_str());
			hists_centers[i][j] = (TH1D*)inf_centers->Get(("h1_bincentersBFFT_" + stations[i] + std::to_string(channels[j]) + "_magcut40").c_str());
		}
    
	for (size_t i = 0; i < 2; i++)
        for (size_t j = 0; j < 16; j++)
            hists_raw_times[i][j] = (TH1D*)inf_raw->Get((std::to_string(channels[j]) + "_" + stations[i] + std::string("_rawTime")).c_str());
    
    for (size_t i = 0; i < 4; i++)
        for (size_t j = 0; j < 6; j++)
        {
            h_deltas_A[i][j] = (TH1D*)inf_res->Get((combinations[i][j] + "_A_delta_t").c_str());
            h_deltas_C[i][j] = (TH1D*)inf_res->Get((combinations[i][j] + "_C_delta_t").c_str());
        }
    for (size_t i = 0; i < 4; i++)
        for (size_t j = 0; j < 6; j++)
        {
            //h_deltas_out_A[i][j] = new TH1D((combinations[i][j] + "_A_delta_t_cutted").c_str(), (combinations[i][j] + "_A_delta_t_cutted").c_str(), 1300, -6500, 6500); //429027
            //h_deltas_out_C[i][j] = new TH1D((combinations[i][j] + "_C_delta_t_cutted").c_str(), (combinations[i][j] + "_C_delta_t_cutted").c_str(), 3600, -13500, 22500); //429027
            h_deltas_out_A[i][j] = new TH1D((combinations[i][j] + "_A_delta_t_cutted").c_str(), (combinations[i][j] + "_A_delta_t_cutted").c_str(), 700, -4500, 2500); //428770
            h_deltas_out_C[i][j] = new TH1D((combinations[i][j] + "_C_delta_t_cutted").c_str(), (combinations[i][j] + "_C_delta_t_cutted").c_str(), 12000, -60000, 60000); //428770
        }

    auto f_gauses_A = std::vector<std::vector<TF1 *>>(4, std::vector<TF1 *>(6));
    auto f_gauses_C = std::vector<std::vector<TF1 *>>(4, std::vector<TF1 *>(6));
    auto main_means_A = std::vector<std::vector<Double_t>>(4, std::vector<Double_t>(6));
    auto main_means_C = std::vector<std::vector<Double_t>>(4, std::vector<Double_t>(6));

    std::vector<float> *toFHit_channel = nullptr, *toFHit_bar = nullptr, *aFPTrack_xLocal = nullptr, *toFHit_trainID = nullptr,
                       *toFHit_stationID = nullptr, *aFPTrack_stationID = nullptr, *toFHit_time = nullptr;

    int runNumber;
    
    tree_data->SetBranchAddress("ToFHit_channel", &toFHit_channel);
    tree_data->SetBranchAddress("AFPTrack_xLocal", &aFPTrack_xLocal);
    tree_data->SetBranchAddress("ToFHit_trainID", &toFHit_trainID);
    tree_data->SetBranchAddress("ToFHit_bar", &toFHit_bar);
    tree_data->SetBranchAddress("ToFHit_stationID", &toFHit_stationID);
    tree_data->SetBranchAddress("AFPTrack_stationID", &aFPTrack_stationID);
    tree_data->SetBranchAddress("ToFHit_time", &toFHit_time);
    tree_data->SetBranchAddress("RunNumber", &runNumber);
    TRandom3 * random = new TRandom3();
    tree_data->GetEntry(1);

    auto ranges = [&](TH1D *hist, int userRangeDown, int userRangeUp)
    {
        hist->GetXaxis()->SetRangeUser(userRangeDown, userRangeUp);
    };

    auto fitter = [&](TH1D *hist, TF1 *& gaus1, std::string station, int index, int gausRangeDown, 
    int gausRangeUp, int gaus1mean, int gaus1width, int gaus2mean, int gaus2width)
    {
        if (hist->GetEntries() > 0)
        {
            TF1 * totalFit = new TF1("totalFit","gaus(0)+gaus(3)", gausRangeDown,gausRangeUp); 
            totalFit->SetLineColor(2);
            gaus1 = new TF1("gaus1","gaus(0)", gausRangeDown,gausRangeUp);
            //TF1 * gaus1 = new TF1("gaus1","gaus(0)", gausRangeDown,gausRangeUp);
            gaus1->SetLineColor(4);
            gaus1->SetLineWidth(2);
            gaus1->SetLineStyle(4);
            TF1 * gaus2 = new TF1("gaus2","gaus(3)", gausRangeDown,gausRangeUp);
            gaus2->SetLineColor(6);
            gaus2->SetLineWidth(2);
            gaus2->SetLineStyle(2);

            totalFit->SetParameter(0, 10000); 
            totalFit->SetParameter(1, gaus1mean); 
            totalFit->SetParameter(2, gaus1width); 
            totalFit->SetParameter(3, 16000); 
            totalFit->SetParameter(4, gaus2mean); 
            totalFit->SetParameter(5, gaus2width);
            //std::cout<<"here0"<<std::endl;
            hist->Fit(totalFit,"R");
            //std::cout<<"here1"<<std::endl;
            Double_t par[6];
            Double_t er[6];
            //std::cout<<"here2"<<std::endl;
            totalFit->GetParameters(&par[0]);
            //std::cout<<"here3"<<std::endl;
            er[2]=totalFit->GetParError(2);
            er[5]=totalFit->GetParError(5);
            //std::cout<<"here_end"<<std::endl;
            gaus1->SetParameter(0,(par[0]));
            gaus1->SetParameter(1,(par[1]));
            gaus1->SetParameter(2,(par[2]));
            gaus2->SetParameter(3,(par[3]));
            gaus2->SetParameter(4,(par[4]));
            gaus2->SetParameter(5,(par[5]));

            Double_t main_mean = gaus1->Eval(par[1]);
            return main_mean;

            delete totalFit;
            delete gaus2;
        }  
    };

    auto fitter_x3 = [&](TH1D *hist, TF1 *& gaus1, std::string station, int index, int gausRangeDown, 
    int gausRangeUp, int gaus1mean, int gaus1width, int gaus2mean, int gaus2width, int gaus3mean, int gaus3width)
    {
        if (hist->GetEntries() > 0)
        {
            TF1 * totalFit = new TF1("totalFit","gaus(0)+gaus(3)+gaus(6)", gausRangeDown,gausRangeUp);
            totalFit->SetLineColor(2);
            gaus1 = new TF1("gaus1","gaus(0)", gausRangeDown,gausRangeUp);
            //TF1 * gaus1 = new TF1("gaus1","gaus(0)", gausRangeDown,gausRangeUp);
            gaus1->SetLineColor(4);
            gaus1->SetLineWidth(2);
            gaus1->SetLineStyle(4);
            TF1 * gaus2 = new TF1("gaus2","gaus(3)", gausRangeDown,gausRangeUp);
            gaus2->SetLineColor(6);
            gaus2->SetLineWidth(2);
            gaus2->SetLineStyle(2);
            TF1 * gaus3 = new TF1("gaus3","gaus(6)", gausRangeDown,gausRangeUp);
            gaus3->SetLineColor(8);
            gaus3->SetLineWidth(2);
            gaus3->SetLineStyle(2);

            totalFit->SetParameter(0, 10000); 
            totalFit->SetParameter(1, gaus1mean); 
            totalFit->SetParameter(2, gaus1width); 
            totalFit->SetParameter(3, 16000); 
            totalFit->SetParameter(4, gaus2mean); 
            totalFit->SetParameter(5, gaus2width);
            totalFit->SetParameter(6, 10000); 
            totalFit->SetParameter(7, gaus3mean); 
            totalFit->SetParameter(8, gaus3width);

            hist->Fit(totalFit,"R");
            Double_t par[9];
            Double_t er[9];
            totalFit->GetParameters(&par[0]);
            er[2]=totalFit->GetParError(2);
            er[5]=totalFit->GetParError(5);
            er[8]=totalFit->GetParError(8);

            gaus1->SetParameter(0,(par[0]));
            gaus1->SetParameter(1,(par[1]));
            gaus1->SetParameter(2,(par[2]));
            gaus2->SetParameter(3,(par[3]));
            gaus2->SetParameter(4,(par[4]));
            gaus2->SetParameter(5,(par[5]));
            gaus3->SetParameter(6,(par[6]));
            gaus3->SetParameter(7,(par[7]));
            gaus3->SetParameter(8,(par[8]));

            Double_t main_mean = gaus1->Eval(par[1]);
            return main_mean;

            delete totalFit;
            delete gaus2;
        }  
    };

    auto fitter_gaus2 = [&](TH1D *hist, TF1 *& gaus2, std::string station, int index, int gausRangeDown, 
    int gausRangeUp, int gaus1mean, int gaus1width, int gaus2mean, int gaus2width)
    {
        if (hist->GetEntries() > 0)
        {
            TF1 * totalFit = new TF1("totalFit","gaus(0)+gaus(3)", gausRangeDown,gausRangeUp); 
            totalFit->SetLineColor(2);
            //gaus1 = new TF1("gaus1","gaus(0)", gausRangeDown,gausRangeUp);
            TF1 * gaus1 = new TF1("gaus1","gaus(0)", gausRangeDown,gausRangeUp);
            gaus1->SetLineColor(4);
            gaus1->SetLineWidth(2);
            gaus1->SetLineStyle(4);
            gaus2 = new TF1("gaus2","gaus(3)", gausRangeDown,gausRangeUp);
            //TF1 * gaus2 = new TF1("gaus2","gaus(3)", gausRangeDown,gausRangeUp);
            gaus2->SetLineColor(6);
            gaus2->SetLineWidth(2);
            gaus2->SetLineStyle(2);

            totalFit->SetParameter(0, 10000); 
            totalFit->SetParameter(1, gaus1mean); 
            totalFit->SetParameter(2, gaus1width); 
            totalFit->SetParameter(3, 16000); 
            totalFit->SetParameter(4, gaus2mean); 
            totalFit->SetParameter(5, gaus2width);
            //std::cout<<"here0"<<std::endl;
            hist->Fit(totalFit,"R");
            //std::cout<<"here1"<<std::endl;
            Double_t par[6];
            Double_t er[6];
            //std::cout<<"here2"<<std::endl;
            totalFit->GetParameters(&par[0]);
            //std::cout<<"here3"<<std::endl;
            er[2]=totalFit->GetParError(2);
            er[5]=totalFit->GetParError(5);
            //std::cout<<"here_end"<<std::endl;
            gaus1->SetParameter(0,(par[0]));
            gaus1->SetParameter(1,(par[1]));
            gaus1->SetParameter(2,(par[2]));
            gaus2->SetParameter(3,(par[3]));
            gaus2->SetParameter(4,(par[4]));
            gaus2->SetParameter(5,(par[5]));

            Double_t main_mean = gaus1->Eval(par[1]);
            return main_mean;

            delete totalFit;
            delete gaus2;
        }  
    };

    
    //429027
    main_means_A[0][0]=fitter(h_deltas_A[0][0],f_gauses_A[0][0],"A",0,400,1000,720,40,720,80);
    main_means_A[0][1]=fitter(h_deltas_A[0][1],f_gauses_A[0][1],"A",1,0,600,300,40,350,80);
    main_means_A[0][2]=fitter(h_deltas_A[0][2],f_gauses_A[0][2],"A",2,0,800,400,60,400,100);
    main_means_A[0][3]=fitter(h_deltas_A[0][3],f_gauses_A[0][3],"A",3,-700,0,-400,40,-400,80);
    main_means_A[0][4]=fitter(h_deltas_A[0][4],f_gauses_A[0][4],"A",4,-800,0,-300,50,-550,50);
    main_means_A[0][5]=fitter(h_deltas_A[0][5],f_gauses_A[0][5],"A",5,-200,400,80,50,80,100);
    main_means_A[1][0]=fitter(h_deltas_A[1][0],f_gauses_A[1][0],"A",6,-1200,-400,-770,60,-770,100);
    main_means_A[1][1]=fitter(h_deltas_A[1][1],f_gauses_A[1][1],"A",7,-1300,-500,-900,50,-850,100);
    main_means_A[1][2]=fitter(h_deltas_A[1][2],f_gauses_A[1][2],"A",8,-1000,-300,-650,60,-650,100);
    main_means_A[1][3]=fitter(h_deltas_A[1][3],f_gauses_A[1][3],"A",9,-500,200,-150,40,-150,80);
    main_means_A[1][4]=fitter(h_deltas_A[1][4],f_gauses_A[1][4],"A",10,-200,400,-150,50,0,100);
    main_means_A[1][5]=fitter(h_deltas_A[1][5],f_gauses_A[1][5],"A",11,-100,600,270,40,270,100);
    main_means_A[2][0]=fitter(h_deltas_A[2][0],f_gauses_A[2][0],"A",12,-1400,0,-760,80,-760,200);
    main_means_A[2][1]=fitter(h_deltas_A[2][1],f_gauses_A[2][1],"A",13,-400,300,-30,70,-30,100);
    main_means_A[2][2]=fitter(h_deltas_A[2][2],f_gauses_A[2][2],"A",14,-700,0,-250,80,-300,100);
    main_means_A[2][3]=fitter_x3(h_deltas_A[2][3],f_gauses_A[2][3],"A",15,0,1200,600,90,800,70,300,100);//
    main_means_A[2][4]=fitter_x3(h_deltas_A[2][4],f_gauses_A[2][4],"A",16,-200,1000,600,80,350,100,150,100);//
    main_means_A[2][5]=fitter(h_deltas_A[2][5],f_gauses_A[2][5],"A",17,-600,100,-250,70,-250,100);
    main_means_A[3][0]=fitter(h_deltas_A[3][0],f_gauses_A[3][0],"A",18,4400,5000,4750,50,4750,100);
    main_means_A[3][1]=fitter(h_deltas_A[3][1],f_gauses_A[3][1],"A",19,1300,2200,1700,100,1900,80);
    main_means_A[3][2]=fitter(h_deltas_A[3][2],f_gauses_A[3][2],"A",20,-600,200,-90,70,-90,120);
    main_means_A[3][3]=fitter(h_deltas_A[3][3],f_gauses_A[3][3],"A",21,-3400,-2600,-3000,80,-2900,100);
    main_means_A[3][4]=fitter(h_deltas_A[3][4],f_gauses_A[3][4],"A",22,-5200,-4500,-4800,70,-4950,60);
    main_means_A[3][5]=fitter(h_deltas_A[3][5],f_gauses_A[3][5],"A",23,-2100,-1500,-1800,60,-1800,120);

    main_means_C[0][0]=fitter(h_deltas_C[0][0],f_gauses_C[0][0],"C",0,-8800,-8100,-8400,50,-8400,80);
    main_means_C[0][1]=fitter(h_deltas_C[0][1],f_gauses_C[0][1],"C",1,-9800,-9200,-9500,50,-9500,80);
    main_means_C[0][2]=fitter(h_deltas_C[0][2],f_gauses_C[0][2],"C",2,-9600,-8800,-9200,50,-9200,80);
    main_means_C[0][3]=fitter(h_deltas_C[0][3],f_gauses_C[0][3],"C",3,-1400,-800,-1000,30,-1200,50);
    main_means_C[0][4]=fitter(h_deltas_C[0][4],f_gauses_C[0][4],"C",4,-1300,-500,-800,50,-1050,40);
    main_means_C[0][5]=fitter(h_deltas_C[0][5],f_gauses_C[0][5],"C",5,-200,600,260,50,260,100);
    main_means_C[1][0]=fitter(h_deltas_C[1][0],f_gauses_C[1][0],"C",6,-500,200,-120,50,-120,120);
    main_means_C[1][1]=fitter(h_deltas_C[1][1],f_gauses_C[1][1],"C",7,-200,600,400,50,200,100);
    main_means_C[1][2]=fitter(h_deltas_C[1][2],f_gauses_C[1][2],"C",8,-400,200,-30,60,-50,100);
    main_means_C[1][3]=fitter(h_deltas_C[1][3],f_gauses_C[1][3],"C",9,0,600,300,40,200,100);
    main_means_C[1][4]=fitter(h_deltas_C[1][4],f_gauses_C[1][4],"C",10,-300,400,90,40,0,100);
    main_means_C[1][5]=fitter(h_deltas_C[1][5],f_gauses_C[1][5],"C",11,-500,100,-200,40,-100,80);
    main_means_C[2][0]=fitter(h_deltas_C[2][0],f_gauses_C[2][0],"C",12,1000,2500,1700,90,1700,120);
    main_means_C[2][1]=fitter(h_deltas_C[2][1],f_gauses_C[2][1],"C",13,2200,2800,2500,60,2500,120);
    main_means_C[2][2]=fitter(h_deltas_C[2][2],f_gauses_C[2][2],"C",14,4400,5000,4750,50,4750,80);
    main_means_C[2][3]=fitter_x3(h_deltas_C[2][3],f_gauses_C[2][3],"C",15,0,1500,900,60,630,50,400,100);//
    main_means_C[2][4]=fitter_x3(h_deltas_C[2][4],f_gauses_C[2][4],"C",16,2000,3500,3100,80,2750,40,2450,90);
    main_means_C[2][5]=fitter(h_deltas_C[2][5],f_gauses_C[2][5],"C",17,-1700,2400,2100,50,2000,100);
    main_means_C[3][0]=fitter(h_deltas_C[3][0],f_gauses_C[3][0],"C",18,10700,11400,11100,50,11100,80);
    main_means_C[3][1]=fitter(h_deltas_C[3][1],f_gauses_C[3][1],"C",19,-400,300,-50,50,-50,120);
    main_means_C[3][2]=fitter(h_deltas_C[3][2],f_gauses_C[3][2],"C",20,20200,20800,20600,50,20600,80);
    main_means_C[3][3]=fitter(h_deltas_C[3][3],f_gauses_C[3][3],"C",21,-11400,-10800,-11100,50,-11100,100);
    main_means_C[3][4]=fitter(h_deltas_C[3][4],f_gauses_C[3][4],"C",22,9200,9800,9500,50,9500,80);
    main_means_C[3][5]=fitter(h_deltas_C[3][5],f_gauses_C[3][5],"C",23,20400,20900,20600,80,20600,100);
    
    //deltas 429027
    ranges(h_deltas_out_A[0][0],0,1500);
    ranges(h_deltas_out_A[0][1],-500,1000);
    ranges(h_deltas_out_A[0][2],-500,1000);
    ranges(h_deltas_out_A[0][3],-1200,300);
    ranges(h_deltas_out_A[0][4],-1100,400);
    ranges(h_deltas_out_A[0][5],-800,700);
    ranges(h_deltas_out_A[1][0],-1500,0);
    ranges(h_deltas_out_A[1][1],-1800,-300);
    ranges(h_deltas_out_A[1][2],-1500,0);
    ranges(h_deltas_out_A[1][3],-900,600);
    ranges(h_deltas_out_A[1][4],-700,800);
    ranges(h_deltas_out_A[1][5],-500,1000);
    ranges(h_deltas_out_A[2][0],-1600,-100);
    ranges(h_deltas_out_A[2][1],-900,600);
    ranges(h_deltas_out_A[2][2],-1000,500);
    ranges(h_deltas_out_A[2][3],-100,1400);
    ranges(h_deltas_out_A[2][4],-400,1100);
    ranges(h_deltas_out_A[2][5],-1000,500);
    ranges(h_deltas_out_A[3][0],4000,5500);
    ranges(h_deltas_out_A[3][1],900,2400);
    ranges(h_deltas_out_A[3][2],-1000,500);
    ranges(h_deltas_out_A[3][3],-3800,-2300);
    ranges(h_deltas_out_A[3][4],-5500,-4000);
    ranges(h_deltas_out_A[3][5],-2600,-1100);

    ranges(h_deltas_out_C[0][0],-9200,-7700);
    ranges(h_deltas_out_C[0][1],-10300,-8800);
    ranges(h_deltas_out_C[0][2],-10000,-8500);
    ranges(h_deltas_out_C[0][3],-1900,-400);
    ranges(h_deltas_out_C[0][4],-1700,-200);
    ranges(h_deltas_out_C[0][5],-500,1000);
    ranges(h_deltas_out_C[1][0],-1000,500);
    ranges(h_deltas_out_C[1][1],-600,900);
    ranges(h_deltas_out_C[1][2],-800,700);
    ranges(h_deltas_out_C[1][3],-500,1000);
    ranges(h_deltas_out_C[1][4],-800,700);
    ranges(h_deltas_out_C[1][5],-1000,500);
    ranges(h_deltas_out_C[2][0],1000,2500);
    ranges(h_deltas_out_C[2][1],1700,3200);
    ranges(h_deltas_out_C[2][2],4000,5500);
    ranges(h_deltas_out_C[2][3],0,1500);
    ranges(h_deltas_out_C[2][4],2100,3600);
    ranges(h_deltas_out_C[2][5],1200,2700);
    ranges(h_deltas_out_C[3][0],10300,11800);
    ranges(h_deltas_out_C[3][1],-900,600);
    ranges(h_deltas_out_C[3][2],19700,21200);
    ranges(h_deltas_out_C[3][3],-12000,-10500);
    ranges(h_deltas_out_C[3][4],8700,10200);
    ranges(h_deltas_out_C[3][5],19800,21300);
    
    /*
    //428770
    main_means_A[0][0]=fitter(h_deltas_A[0][0],f_gauses_A[0][0],"A",0,0,300,175,40,175,80);
    main_means_A[0][1]=fitter(h_deltas_A[0][1],f_gauses_A[0][1],"A",1,-300,0,-160,40,-160,80);
    main_means_A[0][2]=fitter(h_deltas_A[0][2],f_gauses_A[0][2],"A",2,-1700,1300,-1400,60,-1400,100);
    main_means_A[0][3]=fitter(h_deltas_A[0][3],f_gauses_A[0][3],"A",3,-500,200,-350,40,-350,80);
    main_means_A[0][4]=fitter(h_deltas_A[0][4],f_gauses_A[0][4],"A",4,-1900,-1300,-1500,40,-1500,120);
    main_means_A[0][5]=fitter(h_deltas_A[0][5],f_gauses_A[0][5],"A",5,-1500,-1000,-1200,120,-1200,40);
    main_means_A[1][0]=fitter(h_deltas_A[1][0],f_gauses_A[1][0],"A",6,-300,200,-70,60,-40,100);
    main_means_A[1][1]=fitter(h_deltas_A[1][1],f_gauses_A[1][1],"A",7,-600,0,-300,50,-250,100);
    main_means_A[1][2]=fitter(h_deltas_A[1][2],f_gauses_A[1][2],"A",8,-600,0,-350,50,-200,100);
    main_means_A[1][3]=fitter(h_deltas_A[1][3],f_gauses_A[1][3],"A",9,-500,100,-250,40,-300,80);
    main_means_A[1][4]=fitter(h_deltas_A[1][4],f_gauses_A[1][4],"A",10,-600,-100,-300,40,-500,150);
    main_means_A[1][5]=fitter(h_deltas_A[1][5],f_gauses_A[1][5],"A",11,-200,100,-30,40,-30,100);
    main_means_A[2][0]=fitter(h_deltas_A[2][0],f_gauses_A[2][0],"A",12,-1000,-400,-650,60,-650,100);
    main_means_A[2][1]=fitter(h_deltas_A[2][1],f_gauses_A[2][1],"A",13,-3500,-2900,-3200,70,-3200,100);
    main_means_A[2][2]=fitter(h_deltas_A[2][2],f_gauses_A[2][2],"A",14,-3100,-2600,-2800,60,-2800,100);
    main_means_A[2][3]=fitter(h_deltas_A[2][3],f_gauses_A[2][3],"A",15,-3100,-2200,-2500,80,-2700,150);
    main_means_A[2][4]=fitter(h_deltas_A[2][4],f_gauses_A[2][4],"A",16,-2500,-1900,-2100,70,-2300,100);
    main_means_A[2][5]=fitter(h_deltas_A[2][5],f_gauses_A[2][5],"A",17,100,600,350,100,350,70);
    main_means_A[3][0]=fitter(h_deltas_A[3][0],f_gauses_A[3][0],"A",18,-1300,-700,-1000,70,-1000,100);
    main_means_A[3][1]=fitter(h_deltas_A[3][1],f_gauses_A[3][1],"A",19,-700,100,-400,70,-150,70);
    main_means_A[3][2]=fitter(h_deltas_A[3][2],f_gauses_A[3][2],"A",20,-600,0,-300,70,-300,120);
    main_means_A[3][3]=fitter(h_deltas_A[3][3],f_gauses_A[3][3],"A",21,400,1000,650,80,700,100);
    main_means_A[3][4]=fitter(h_deltas_A[3][4],f_gauses_A[3][4],"A",22,400,1000,700,50,700,100);
    main_means_A[3][5]=fitter(h_deltas_A[3][5],f_gauses_A[3][5],"A",23,-200,400,90,60,90,120);
 
    main_means_C[0][0]=fitter(h_deltas_C[0][0],f_gauses_C[0][0],"C",0,-300,100,-70,80,-70,40);
    main_means_C[0][1]=fitter(h_deltas_C[0][1],f_gauses_C[0][1],"C",1,-300,200,-30,50,-30,80);
    main_means_C[0][2]=fitter(h_deltas_C[0][2],f_gauses_C[0][2],"C",2,-300,400,30,60,30,80);
    main_means_C[0][3]=fitter(h_deltas_C[0][3],f_gauses_C[0][3],"C",3,-100,200,40,30,40,50);
    main_means_C[0][4]=fitter(h_deltas_C[0][4],f_gauses_C[0][4],"C",4,-200,400,100,100,100,60);
    main_means_C[0][5]=fitter(h_deltas_C[0][5],f_gauses_C[0][5],"C",5,-200,300,70,100,70,50);
    main_means_C[1][0]=fitter(h_deltas_C[1][0],f_gauses_C[1][0],"C",6,-500,200,-120,70,-120,120);
    main_means_C[1][1]=fitter(h_deltas_C[1][1],f_gauses_C[1][1],"C",7,-54600,-53400,-54000,80,-54000,100);
    main_means_C[1][2]=fitter(h_deltas_C[1][2],f_gauses_C[1][2],"C",8,-600,0,-300,80,-300,150);
    main_means_C[1][3]=fitter_gaus2(h_deltas_C[1][3],f_gauses_C[1][3],"C",9,-54400,-53400,-53800,40,-53900,140);//
    main_means_C[1][4]=fitter(h_deltas_C[1][4],f_gauses_C[1][4],"C",10,-500,100,-100,50,-200,80);
    main_means_C[1][5]=fitter_gaus2(h_deltas_C[1][5],f_gauses_C[1][5],"C",11,53200,54000,53700,40,53700,120);//
    main_means_C[2][0]=fitter(h_deltas_C[2][0],f_gauses_C[2][0],"C",12,-300,500,150,90,100,120);
    main_means_C[2][1]=fitter(h_deltas_C[2][1],f_gauses_C[2][1],"C",13,200,800,550,60,500,100);
    main_means_C[2][2]=fitter(h_deltas_C[2][2],f_gauses_C[2][2],"C",14,-500,200,-100,70,-200,100);
    main_means_C[2][3]=fitter(h_deltas_C[2][3],f_gauses_C[2][3],"C",15,0,800,400,90,150,120);
    main_means_C[2][4]=fitter(h_deltas_C[2][4],f_gauses_C[2][4],"C",16,-700,200,-200,110,-500,150);
    main_means_C[2][5]=fitter(h_deltas_C[2][5],f_gauses_C[2][5],"C",17,-900,-400,-600,100,-600,50);
    main_means_C[3][0]=fitter(h_deltas_C[3][0],f_gauses_C[3][0],"C",18,-1000,-300,-700,60,-700,100);
    main_means_C[3][1]=fitter(h_deltas_C[3][1],f_gauses_C[3][1],"C",19,-600,0,-270,60,-270,100);
    main_means_C[3][2]=fitter(h_deltas_C[3][2],f_gauses_C[3][2],"C",20,-400,150,-100,60,-100,100);
    main_means_C[3][3]=fitter(h_deltas_C[3][3],f_gauses_C[3][3],"C",21,150,650,400,50,400,80);
    main_means_C[3][4]=fitter(h_deltas_C[3][4],f_gauses_C[3][4],"C",22,300,800,580,50,550,80);
    main_means_C[3][5]=fitter(h_deltas_C[3][5],f_gauses_C[3][5],"C",23,-100,350,150,30,150,80);

    //deltas 428770
    ranges(h_deltas_out_A[0][0],-500,1000);
    ranges(h_deltas_out_A[0][1],-1000,500);
    ranges(h_deltas_out_A[0][2],-2200,-700);
    ranges(h_deltas_out_A[0][3],-1100,400);
    ranges(h_deltas_out_A[0][4],-2300,-800);
    ranges(h_deltas_out_A[0][5],-2000,-500);
    ranges(h_deltas_out_A[1][0],-800,700);
    ranges(h_deltas_out_A[1][1],-1100,400);
    ranges(h_deltas_out_A[1][2],-1100,400);
    ranges(h_deltas_out_A[1][3],-1000,500);
    ranges(h_deltas_out_A[1][4],-1000,500);
    ranges(h_deltas_out_A[1][5],-800,700);
    ranges(h_deltas_out_A[2][0],-1400,100);
    ranges(h_deltas_out_A[2][1],-4000,-2500);
    ranges(h_deltas_out_A[2][2],-3700,-2200);
    ranges(h_deltas_out_A[2][3],-3300,-1800);
    ranges(h_deltas_out_A[2][4],-3000,-1500);
    ranges(h_deltas_out_A[2][5],-400,1100);
    ranges(h_deltas_out_A[3][0],-1800,-300);
    ranges(h_deltas_out_A[3][1],-1100,400);
    ranges(h_deltas_out_A[3][2],-1000,500);
    ranges(h_deltas_out_A[3][3],-100,1400);
    ranges(h_deltas_out_A[3][4],0,1500);
    ranges(h_deltas_out_A[3][5],-700,800);

    ranges(h_deltas_out_C[0][0],-800,700);
    ranges(h_deltas_out_C[0][1],-800,700);
    ranges(h_deltas_out_C[0][2],-800,700);
    ranges(h_deltas_out_C[0][3],-800,700);
    ranges(h_deltas_out_C[0][4],-700,800);
    ranges(h_deltas_out_C[0][5],-800,700);
    ranges(h_deltas_out_C[1][0],-1000,500);
    ranges(h_deltas_out_C[1][1],-54800,-53300);
    ranges(h_deltas_out_C[1][2],-1000,500);
    ranges(h_deltas_out_C[1][3],-54600,-53100);
    ranges(h_deltas_out_C[1][4],-1000,500);
    ranges(h_deltas_out_C[1][5],53000,54500);
    ranges(h_deltas_out_C[2][0],-700,800);
    ranges(h_deltas_out_C[2][1],-300,1200);
    ranges(h_deltas_out_C[2][2],-1000,500);
    ranges(h_deltas_out_C[2][3],-500,1000);
    ranges(h_deltas_out_C[2][4],-1200,300);
    ranges(h_deltas_out_C[2][5],-1500,0);
    ranges(h_deltas_out_C[3][0],-1400,100);
    ranges(h_deltas_out_C[3][1],-1000,500);
    ranges(h_deltas_out_C[3][2],-900,600);
    ranges(h_deltas_out_C[3][3],-400,1100);
    ranges(h_deltas_out_C[3][4],-300,1200);
    ranges(h_deltas_out_C[3][5],-700,800);
    */

    //analysis
	for (size_t i = 0; i < nentries; i++)
    //for (size_t i = 0; i < nentries/100; i++)
    {
        
        if (i % 1000000 == 0)
            std::cout << i/1000000 << " from " << nentries/1000000 << "\n";
        tree_data->GetEntry(i);

        if (toFHit_channel->size() == 0 || aFPTrack_xLocal->size() == 0)
            continue;

        //cut on only 1 train per event
        std::vector<int> vec_train_A(4, 0), vec_train_C(4, 0);
        for (size_t j = 0; j < toFHit_trainID->size(); j++)
        {
            for (size_t h = 0; h < 4; h++)
            {
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 0)
                    vec_train_A[h] = 1;
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 3)
                    vec_train_C[h] = 1;
            }
        }

        int sum_train_A = 0, sum_train_C = 0;
        for (size_t h = 0; h < 4; h++)
        {
            sum_train_A += vec_train_A[h];
            sum_train_C += vec_train_C[h];
        }

        //cut in only 1 track per event
        int sum_tracks_A = 0, sum_tracks_C = 0;
        for (size_t j = 0; j < aFPTrack_stationID->size(); j++)
        {
            if (aFPTrack_stationID->at(j) == 0)
                sum_tracks_A++;
            if (aFPTrack_stationID->at(j) == 3)
                sum_tracks_C++;
        }
        if (sum_tracks_A > 1 || sum_tracks_C > 1)
            continue;

        //cut on only 1 hit in 1 channel
        auto count_chans_A = std::vector<int>{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        auto count_chans_C = std::vector<int>{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        for (size_t j = 0; j < toFHit_channel->size(); j++)
        {
            if (toFHit_stationID->at(j) == 0)
            {
                size_t k = toFHit_channel->at(j);
                count_chans_A[k]++;
            }
            if (toFHit_stationID->at(j) == 3)
            {
                size_t k = toFHit_channel->at(j);
                count_chans_C[k]++;
            }
        }
        int count_hitsInChans=0;
        for (size_t k = 0; k < 16; k++)              
            if (count_chans_A[k] > 1 || count_chans_C[k] > 1)
                count_hitsInChans++;
        if (count_hitsInChans>0)
            continue;
        
		auto times_A = std::vector<std::vector<float>>(4, std::vector<float>(4, -10000));
        auto times_C = std::vector<std::vector<float>>(4, std::vector<float>(4, -10000));
		for (size_t j = 0; j < toFHit_channel->size(); j++)
        {
            int chan = (int)toFHit_channel->at(j);
			int train = (int)toFHit_trainID->at(j);
            int bar = (int)toFHit_bar->at(j);
            if (chan==16)
                continue;
            float rawTime=(toFHit_time->at(j))/(25.0/1024.0);
            float rawTimePs=(toFHit_time->at(j))*1000;
			
            if ((rawTime>200 && rawTime<300) || (rawTime>900 && rawTime<1000))
            {
                if ((toFHit_stationID->at(j))==0 && rawTime>900 && rawTime<1000 && sum_tracks_A==1 && sum_train_A==1 && chan < 16)
                {
					double uncalibin = hists_raw_times[0][chan]->FindBin(rawTime);
					float bin_center = hists_centers[0][chan]->GetBinContent(uncalibin);
              		float bin_width = hists_width[0][chan]->GetBinContent(uncalibin);
					bin_center = bin_center + random->Uniform(-0.5,0.5)*bin_width;
                    bin_center = bin_center/(25.0/1024.0*1000);
					times_A[train][bar]=bin_center;
                    
                }
                if ((toFHit_stationID->at(j))==3 && rawTime>200 && rawTime<300 && sum_tracks_C==1 && sum_train_C==1 && chan < 16)
                {
                    double uncalibin = hists_raw_times[1][chan]->FindBin(rawTime);
					float bin_center = hists_centers[1][chan]->GetBinContent(uncalibin);
              		float bin_width = hists_width[1][chan]->GetBinContent(uncalibin);
					bin_center = bin_center + random->Uniform(-0.5,0.5)*bin_width;
                    bin_center = bin_center/(25.0/1024.0*1000);
					times_C[train][bar]=bin_center;
                }
            }
        }
        
        //fill histos for resolutions
        for (size_t train = 0; train < 4; ++train)
            for (size_t bar1 = 0; bar1 < 4; ++bar1)
                for (size_t bar2 = 0; bar2 < 4; bar2++)
                    if (bar2>bar1)
                    {
                        int comb = bar1*bar2+bar2-1;
                        if (comb==5) {comb=4;}
                        if (comb==8) {comb=5;}
                        if (times_A[train][bar1]>-10000 && times_A[train][bar2]>-10000)
                        {
                            float temp_raw_time = (times_A[train][bar1] - times_A[train][bar2])*(25.0/1024.0)*(-1000);
                            Double_t local_mean = f_gauses_A[train][comb]->Eval(temp_raw_time);
                            float rnd_val = random->Uniform(0,1);
                            if (local_mean/main_means_A[train][comb] > rnd_val)
                                h_deltas_out_A[train][comb]->Fill(temp_raw_time);
                        }
                        if (times_C[train][bar1]>-10000 && times_C[train][bar2]>-10000)
                        {
                            float temp_raw_time = (times_C[train][bar1] - times_C[train][bar2])*(25.0/1024.0)*(-1000);
                            Double_t local_mean = f_gauses_C[train][comb]->Eval(temp_raw_time);
                            float rnd_val = random->Uniform(0,1);
                            if (local_mean/main_means_C[train][comb] > rnd_val)
                                h_deltas_out_C[train][comb]->Fill(temp_raw_time);
                        }   
                    } 
        for (size_t j = 0; j < 4; ++j)
        {
            times_A[j].clear();
            times_C[j].clear();
        } 
	}

    for (size_t i = 0; i < 4; i++)
        for (size_t j = 0; j < 6; j++)
        {
            int bar1=-1, bar2=-1;
            if (j==0) 
            {
                bar1=0;
                bar2=1;
            }
            if (j==1) 
            {
                bar1=0;
                bar2=2;
            }
            if (j==2) 
            {
                bar1=0;
                bar2=3;
            }
            if (j==3) 
            {
                bar1=1;
                bar2=2;
            }
            if (j==4) 
            {
                bar1=1;
                bar2=3;
            }
            if (j==5) 
            {
                bar1=2;
                bar2=3;
            }

            h_deltas_out_A[i][j]->GetXaxis()->SetTitle("#Delta t [ps]");
            h_deltas_out_A[i][j]->GetYaxis()->SetTitle("Events");

            h_deltas_out_C[i][j]->GetXaxis()->SetTitle("#Delta t [ps]");
            h_deltas_out_C[i][j]->GetYaxis()->SetTitle("Events");
        }
    
    auto fitter_simple = [&](TH1D *hist, std::string fileName, float label_x, float label_y, std::string side, std::string combination)
    {
        if (hist->GetEntries() > 0)
        {
            TF1* gaus1 = new TF1("gaus1", "gaus");
            gaus1->SetLineColor(2);
            //hist->SetFillColor(kBlue);
            //TF1 *total = new TF1("total", "gaus(0)+gaus(3)");
            //total->SetLineColor(6);    
            //hist->GetXaxis()->SetTitle( (name + " [mm]").c_str());
            //hist->GetYaxis()->SetTitle("Events / (1.66 mm)"); 
            TFitResultPtr r = hist->Fit("gaus1","S");
            double sigma = r->Parameters()[2];
            double error = r->Errors()[2];
            double mean = r->Parameters()[1];
            double error_mean = r->Errors()[1];
            std::stringstream stream_s;
            stream_s << std::fixed << std::setprecision(2) << sigma;
            std::string sigma_str = stream_s.str();
            std::stringstream stream_e;
            stream_e << std::fixed << std::setprecision(2) << error;
            std::string error_str = stream_e.str();
            std::stringstream stream_m;
            stream_m << std::fixed << std::setprecision(2) << mean;
            std::string mean_str = stream_m.str();
            std::stringstream stream_em;
            stream_em << std::fixed << std::setprecision(2) << error_mean;
            std::string error_mean_str = stream_em.str();
            
            TLegend *legend = new TLegend(0.18, 0.72, 0.45, 0.82);
            legend->SetBorderSize(0);
            legend->AddEntry(hist, "Data 2022", "pe");
            //legend->AddEntry(gaus1,("m = (" + mean_str + " #pm " + error_mean_str + ") mm").c_str(),"l");
            legend->AddEntry(gaus1,("#sigma = (" + sigma_str + " #pm " + error_str + ") ps").c_str(),"l");
            legend->SetTextSize(0.04);

			TLatex latex;
			latex.SetNDC();
			latex.SetTextSize(0.04);
			hist->Draw("eX0");
			legend->Draw();

			ATLASLabel_WithoutATLAS(0.75,0.9,"Internal");
			latex.DrawLatex(0.2, 0.85, (std::string("Run ") + std::to_string(runNumber) + std::string(", Far-") + side + std::string(", ") + combination).c_str());
			c1->SaveAs((fileName + "_" + side + "_" + std::to_string(runNumber) + std::string(".pdf")).c_str());
			c1->Clear();

            delete gaus1;
            delete legend;
        }
    };
    
    //write in pdf
    c1->SaveAs((std::string("hptdc_cutted_delta_t_A_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < 4; i++)
        for (size_t j = 0; j < 6; j++)
            fitter_simple(h_deltas_out_A[i][j],"hptdc_cutted_delta_t",0.75,0.85,"A",combinations[i][j]);
    c1->SaveAs((std::string("hptdc_cutted_delta_t_A_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->SaveAs((std::string("hptdc_cutted_delta_t_C_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < 4; i++)
        for (size_t j = 0; j < 6; j++)
            fitter_simple(h_deltas_out_C[i][j],"hptdc_cutted_delta_t",0.75,0.85,"C",combinations[i][j]);
    c1->SaveAs((std::string("hptdc_cutted_delta_t_C_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    
    //write in root
    TFile *outf_new_res = new TFile((std::string("hptdc_new_res_close_cutted_") + std::to_string(runNumber) + std::string(".root")).c_str(), "RECREATE");
    for (size_t i = 0; i < 4; i++)
        for (size_t j = 0; j < 6; j++)
        {
            h_deltas_out_A[i][j]->SetStats(0);
            h_deltas_out_A[i][j]->Draw("bar");
            h_deltas_out_A[i][j]->Write();

            h_deltas_out_C[i][j]->SetStats(0);
            h_deltas_out_C[i][j]->Draw("bar");
            h_deltas_out_C[i][j]->Write();
        }
    outf_new_res->Close();
}