#include <iostream>
#include <iomanip>
#include <sstream>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <cmath>
#include <limits>
#include <TROOT.h>
#include <TStyle.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLatex.h>
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"


int main(int argc, char **argv)
{

    if (argc != 2)
    {
        std::cout << "Requires 2 parameters\n";
        return 1;
    }

    SetAtlasStyle();
    TCanvas *c1 = new TCanvas("c1", "c1", 0., 0., 800, 600);
    TFile *inf = new TFile(argv[1], "READ"); //delta_t distributions before likelihood, produced in "hptdc.cxx"

    // setup

    auto n_bin = 500;
    //auto n_bin = 100;

    auto names = std::vector<std::string>{
        "0AB", "0AC", "0AD", "0BC", "0BD", "0CD",
        "1AB", "1AC", "1AD", "1BC", "1BD", "1CD",
        "2AB", "2AC", "2AD", "2BC", "2BD", "2CD",
        "3AB", "3AC", "3AD", "3BC", "3BD", "3CD"};

    std::vector<TH1D *> hists_A(names.size()), hists_C(names.size());

    for (size_t i = 0; i < names.size(); i++)
    {
        hists_A[i] = (TH1D*)inf->Get((names[i] + "_A_delta_t").c_str());
        hists_C[i] = (TH1D*)inf->Get((names[i] + "_C_delta_t").c_str());
    }

    // start
    int runNumber=429027;
    //int runNumber=428770;

    for (size_t i = 0; i < names.size(); ++i)
    {
        hists_A[i]->GetXaxis()->SetTitle("#Delta t [ps]");
        hists_A[i]->GetYaxis()->SetTitle("Events");
        //hists_A[i]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", Far-A, Time difference, channels ") + names[i]).c_str());
        // hists_A[i]->SetFillColor(kBlue-6);
        // hists_A[i]->SetMarkerStyle(20);
        // hists_A[i]->SetMarkerSize(0.8);

        hists_C[i]->GetXaxis()->SetTitle("#Delta t [ps]");
        hists_C[i]->GetYaxis()->SetTitle("Events");
        //hists_C[i]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", Far-C, Time difference, channels ") + names[i]).c_str());
        // hists_C[i]->SetFillColor(kBlue-6);
        // hists_C[i]->SetMarkerStyle(20);
        // hists_C[i]->SetMarkerSize(0.8);
    }

    auto fitter = [&](TH1D *hist, std::string station, int index, int gausRangeDown, 
    int gausRangeUp, int gaus1mean, int gaus1width, int gaus2mean, int gaus2width)
    {
        TF1 * totalFit = new TF1("totalFit","gaus(0)+gaus(3)", gausRangeDown,gausRangeUp); 
        totalFit->SetLineColor(2);
        TF1 * gaus1 = new TF1("gaus1","gaus(0)", gausRangeDown,gausRangeUp);
        gaus1->SetLineColor(4);
        gaus1->SetLineWidth(2);
        gaus1->SetLineStyle(4);
        TF1 * gaus2 = new TF1("gaus2","gaus(3)", gausRangeDown,gausRangeUp);
        gaus2->SetLineColor(6);
        gaus2->SetLineWidth(2);
        gaus2->SetLineStyle(2);

        totalFit->SetParameter(0, 10000); 
        totalFit->SetParameter(1, gaus1mean); 
        totalFit->SetParameter(2, gaus1width); 
        totalFit->SetParameter(3, 16000); 
        totalFit->SetParameter(4, gaus2mean); 
        totalFit->SetParameter(5, gaus2width);

        hist->Fit(totalFit,"R");
        Double_t par[6];
        Double_t er[6];
        totalFit->GetParameters(&par[0]);
        er[2]=totalFit->GetParError(2);
        er[5]=totalFit->GetParError(5);

        gaus1->SetParameter(0,(par[0]));
        gaus1->SetParameter(1,(par[1]));
        gaus1->SetParameter(2,(par[2]));
        gaus2->SetParameter(3,(par[3]));
        gaus2->SetParameter(4,(par[4]));
        gaus2->SetParameter(5,(par[5]));

        std::stringstream stream_s1;
        stream_s1 << std::fixed << std::setprecision(2) << par[2];
        std::string sigma1_str = stream_s1.str();
        std::stringstream stream_e1;
        stream_e1 << std::fixed << std::setprecision(2) << er[2];
        std::string error1_str = stream_e1.str();
        std::stringstream stream_s2;
        stream_s2 << std::fixed << std::setprecision(2) << par[5];
        std::string sigma2_str = stream_s2.str();
        std::stringstream stream_e2;
        stream_e2 << std::fixed << std::setprecision(2) << er[5];
        std::string error2_str = stream_e2.str();
        TLegend *legend = new TLegend(0.2, 0.7, 0.45, 0.82);
        legend->SetBorderSize(0);
        legend->AddEntry(hist, "Data 2022", "pe");
        legend->AddEntry(gaus1, ("#sigma = (" + sigma1_str + " #pm " + error1_str + ") ps").c_str(), "l");
        legend->AddEntry(gaus2, ("#sigma = (" + sigma2_str + " #pm " + error2_str + ") ps").c_str(), "l");
        legend->SetTextSize(0.04);
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);

        hist->Draw("eX0");
        totalFit->Draw("same"); 
        gaus1->Draw("same"); 
        gaus2->Draw("same");
        legend->Draw();
        //ATLASLabel(0.63, 0.85, "work in progress");
        ATLASLabel_WithoutATLAS(0.75,0.9,"Internal");
        latex.DrawLatex(0.2, 0.85, (std::string("Run ") + std::to_string(runNumber) + std::string(", Far-") + station + std::string(", ") + names[index]).c_str());
        c1->SaveAs((std::string("hptdc_doubleSumGausAll_400b_withoutATLAS_") + station + std::string("_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();

        delete totalFit;
        delete gaus1;
        delete gaus2;
        delete legend;
    };

    auto fitter_x3 = [&](TH1D *hist, std::string station, int index, int gausRangeDown, 
    int gausRangeUp, int gaus1mean, int gaus1width, int gaus2mean, int gaus2width, 
    int gaus3mean, int gaus3width)
    {
        TF1 * totalFit = new TF1("totalFit","gaus(0)+gaus(3)+gaus(6)", gausRangeDown,gausRangeUp); 
        totalFit->SetLineColor(2);
        TF1 * gaus1 = new TF1("gaus1","gaus(0)", gausRangeDown,gausRangeUp);
        gaus1->SetLineColor(4);
        gaus1->SetLineWidth(2);
        gaus1->SetLineStyle(4);
        TF1 * gaus2 = new TF1("gaus2","gaus(3)", gausRangeDown,gausRangeUp);
        gaus2->SetLineColor(6);
        gaus2->SetLineWidth(2);
        gaus2->SetLineStyle(2);
        TF1 * gaus3 = new TF1("gaus3","gaus(6)", gausRangeDown,gausRangeUp);
        gaus3->SetLineColor(8);
        gaus3->SetLineWidth(2);
        gaus3->SetLineStyle(2);

        totalFit->SetParameter(0, 10000); 
        totalFit->SetParameter(1, gaus1mean); 
        totalFit->SetParameter(2, gaus1width); 
        totalFit->SetParameter(3, 16000); 
        totalFit->SetParameter(4, gaus2mean); 
        totalFit->SetParameter(5, gaus2width);
        totalFit->SetParameter(6, 10000); 
        totalFit->SetParameter(7, gaus3mean); 
        totalFit->SetParameter(8, gaus3width);

        hist->Fit(totalFit,"R");
        Double_t par[9];
        Double_t er[9];
        totalFit->GetParameters(&par[0]);
        er[2]=totalFit->GetParError(2);
        er[5]=totalFit->GetParError(5);
        er[8]=totalFit->GetParError(8);

        gaus1->SetParameter(0,(par[0]));
        gaus1->SetParameter(1,(par[1]));
        gaus1->SetParameter(2,(par[2]));
        gaus2->SetParameter(3,(par[3]));
        gaus2->SetParameter(4,(par[4]));
        gaus2->SetParameter(5,(par[5]));
        gaus3->SetParameter(6,(par[6]));
        gaus3->SetParameter(7,(par[7]));
        gaus3->SetParameter(8,(par[8]));

        std::stringstream stream_s1;
        stream_s1 << std::fixed << std::setprecision(2) << par[2];
        std::string sigma1_str = stream_s1.str();
        std::stringstream stream_e1;
        stream_e1 << std::fixed << std::setprecision(2) << er[2];
        std::string error1_str = stream_e1.str();
        std::stringstream stream_s2;
        stream_s2 << std::fixed << std::setprecision(2) << par[5];
        std::string sigma2_str = stream_s2.str();
        std::stringstream stream_e2;
        stream_e2 << std::fixed << std::setprecision(2) << er[5];
        std::string error2_str = stream_e2.str();

        std::stringstream stream_s3;
        stream_s3 << std::fixed << std::setprecision(2) << par[8];
        std::string sigma3_str = stream_s3.str();
        std::stringstream stream_e3;
        stream_e3 << std::fixed << std::setprecision(2) << er[8];
        std::string error3_str = stream_e3.str();
        TLegend *legend = new TLegend(0.2, 0.7, 0.45, 0.82);
        legend->SetBorderSize(0);
        legend->AddEntry(hist, "Data 2022", "pe");
        legend->AddEntry(gaus1, ("#sigma = (" + sigma1_str + " #pm " + error1_str + ") ps").c_str(), "l");
        legend->AddEntry(gaus2, ("#sigma = (" + sigma2_str + " #pm " + error2_str + ") ps").c_str(), "l");
        legend->AddEntry(gaus3, ("#sigma = (" + sigma3_str + " #pm " + error3_str + ") ps").c_str(), "l");
        legend->SetTextSize(0.04);
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);

        hist->Draw("eX0");
        totalFit->Draw("same"); 
        gaus1->Draw("same"); 
        gaus2->Draw("same");
        gaus3->Draw("same");
        legend->Draw();
        //ATLASLabel(0.63, 0.85, "work in progress");
        ATLASLabel_WithoutATLAS(0.75,0.9,"Internal");
        latex.DrawLatex(0.2, 0.85, (std::string("Run ") + std::to_string(runNumber) + std::string(", Far-") + station + std::string(", ") + names[index]).c_str());
        c1->SaveAs((std::string("hptdc_doubleSumGausAll_400b_withoutATLAS_") + station + std::string("_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();

        delete totalFit;
        delete gaus1;
        delete gaus2;
        delete legend;
    };

    // 2 gaus fit 
    
    //429027
    c1->SaveAs((std::string("hptdc_doubleSumGausAll_400b_withoutATLAS_A_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    fitter(hists_A[0],"A",0,400,1000,720,40,720,80);
    fitter(hists_A[1],"A",1,0,600,300,40,350,80);
    fitter(hists_A[2],"A",2,0,800,400,60,400,100);
    fitter(hists_A[3],"A",3,-700,0,-400,40,-400,80);
    fitter(hists_A[4],"A",4,-800,0,-300,50,-550,50);
    fitter(hists_A[5],"A",5,-200,400,80,50,80,100);
    fitter(hists_A[6],"A",6,-1200,-400,-770,60,-770,100);
    fitter(hists_A[7],"A",7,-1300,-500,-900,50,-850,100);
    fitter(hists_A[8],"A",8,-1000,-300,-650,60,-650,100);
    fitter(hists_A[9],"A",9,-500,200,-150,40,-150,80);
    fitter(hists_A[10],"A",10,-200,400,-150,50,0,100);
    fitter(hists_A[11],"A",11,-100,600,270,40,270,100);
    fitter(hists_A[12],"A",12,-1400,0,-760,80,-760,200);
    fitter(hists_A[13],"A",13,-400,300,-30,70,-30,100);
    fitter(hists_A[14],"A",14,-700,0,-250,80,-300,100);
    fitter_x3(hists_A[15],"A",15,0,1200,600,90,800,70,300,100);//
    fitter_x3(hists_A[16],"A",16,-200,1000,600,80,350,100,150,100);//
    fitter(hists_A[17],"A",17,-600,100,-250,70,-250,100);
    fitter(hists_A[18],"A",18,4400,5000,4750,50,4750,100);
    fitter(hists_A[19],"A",19,1300,2200,1700,100,1900,80);
    fitter(hists_A[20],"A",20,-600,200,-90,70,-90,120);
    fitter(hists_A[21],"A",21,-3400,-2600,-3000,80,-2900,100);
    fitter(hists_A[22],"A",22,-5200,-4500,-4800,70,-4950,60);
    fitter(hists_A[23],"A",23,-2100,-1500,-1800,60,-1800,120);
    c1->SaveAs((std::string("hptdc_doubleSumGausAll_400b_withoutATLAS_A_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    
    c1->SaveAs((std::string("hptdc_doubleSumGausAll_400b_withoutATLAS_C_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    fitter(hists_C[0],"C",0,-8800,-8100,-8400,50,-8400,80);
    fitter(hists_C[1],"C",1,-9800,-9200,-9500,50,-9500,80);
    fitter(hists_C[2],"C",2,-9600,-8800,-9200,50,-9200,80);
    fitter(hists_C[3],"C",3,-1400,-800,-1000,30,-1200,50);
    fitter(hists_C[4],"C",4,-1300,-500,-800,50,-1050,40);
    fitter(hists_C[5],"C",5,-200,600,260,50,260,100);
    fitter(hists_C[6],"C",6,-500,200,-120,50,-120,120);
    fitter(hists_C[7],"C",7,-200,600,400,50,200,100);
    fitter(hists_C[8],"C",8,-400,200,-30,60,-50,100);
    fitter(hists_C[9],"C",9,0,600,300,40,200,100);
    fitter(hists_C[10],"C",10,-300,400,90,40,0,100);
    fitter(hists_C[11],"C",11,-500,100,-200,40,-100,80);
    fitter(hists_C[12],"C",12,1000,2500,1700,90,1700,120);
    fitter(hists_C[13],"C",13,2200,2800,2500,60,2500,120);
    fitter(hists_C[14],"C",14,4400,5000,4750,50,4750,80);
    fitter_x3(hists_C[15],"C",15,0,1500,900,60,630,50,400,100);//
    fitter_x3(hists_C[16],"C",16,2000,3500,3100,80,2750,40,2450,90);//
    fitter(hists_C[17],"C",17,-1700,2400,2100,50,2000,100);
    fitter(hists_C[18],"C",18,10700,11400,11100,50,11100,80);
    fitter(hists_C[19],"C",19,-400,300,-50,50,-50,120);
    fitter(hists_C[20],"C",20,20200,20800,20600,50,20600,80);
    fitter(hists_C[21],"C",21,-11400,-10800,-11100,50,-11100,100);
    fitter(hists_C[22],"C",22,9200,9800,9500,50,9500,80);
    fitter(hists_C[23],"C",23,20400,20900,20600,80,20600,100);
    c1->SaveAs((std::string("hptdc_doubleSumGausAll_400b_withoutATLAS_C_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    
    /*
    //428770
    c1->SaveAs((std::string("hptdc_doubleSumGausAll_400b_withoutATLAS_A_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    fitter(hists_A[0],"A",0,0,300,175,40,175,80);
    fitter(hists_A[1],"A",1,-300,0,-160,40,-160,80);
    fitter(hists_A[2],"A",2,-1700,1300,-1400,60,-1400,100);
    fitter(hists_A[3],"A",3,-500,200,-350,40,-350,80);
    fitter(hists_A[4],"A",4,-1900,-1300,-1500,40,-1500,120);
    fitter(hists_A[5],"A",5,-1500,-1000,-1200,120,-1200,40);
    fitter(hists_A[6],"A",6,-300,200,-70,60,-40,100);
    fitter(hists_A[7],"A",7,-600,0,-300,50,-250,100);
    fitter(hists_A[8],"A",8,-600,0,-350,50,-200,100);
    fitter(hists_A[9],"A",9,-500,100,-250,40,-300,80);
    fitter(hists_A[10],"A",10,-600,-100,-300,40,-500,150);
    fitter(hists_A[11],"A",11,-200,100,-30,40,-30,100);
    fitter(hists_A[12],"A",12,-1000,-400,-650,60,-650,100);
    fitter(hists_A[13],"A",13,-3500,-2900,-3200,70,-3200,100);
    fitter(hists_A[14],"A",14,-3100,-2600,-2800,60,-2800,100);
    fitter(hists_A[15],"A",15,-3100,-2200,-2500,80,-2700,150);
    fitter(hists_A[16],"A",16,-2500,-1900,-2100,70,-2300,100);
    fitter(hists_A[17],"A",17,100,600,350,100,350,70);//
    fitter(hists_A[18],"A",18,-1300,-700,-1000,70,-1000,100);
    fitter(hists_A[19],"A",19,-700,100,-400,70,-150,70);
    fitter(hists_A[20],"A",20,-600,0,-300,70,-300,120);
    fitter(hists_A[21],"A",21,400,1000,650,80,700,100);
    fitter(hists_A[22],"A",22,400,1000,700,50,700,100);
    fitter(hists_A[23],"A",23,-200,400,90,60,90,120);
    c1->SaveAs((std::string("hptdc_doubleSumGausAll_400b_withoutATLAS_A_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    
    c1->SaveAs((std::string("hptdc_doubleSumGausAll_400b_withoutATLAS_C_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    fitter(hists_C[0],"C",0,-300,100,-70,80,-70,40);
    fitter(hists_C[1],"C",1,-300,200,-30,50,-30,80);
    fitter(hists_C[2],"C",2,-300,400,30,60,30,80);
    fitter(hists_C[3],"C",3,-100,200,40,30,40,50);
    fitter(hists_C[4],"C",4,-200,400,100,100,100,60);
    fitter(hists_C[5],"C",5,-200,300,70,100,70,50);
    fitter(hists_C[6],"C",6,-500,200,-120,70,-120,120);
    fitter(hists_C[7],"C",7,-54600,-53400,-54000,80,-54000,100);
    fitter(hists_C[8],"C",8,-600,0,-300,80,-300,150);
    fitter(hists_C[9],"C",9,-54400,-53400,-53800,40,-53900,140);//
    fitter(hists_C[10],"C",10,-500,100,-100,50,-200,80);
    fitter(hists_C[11],"C",11,53200,54000,53700,40,53700,120);//
    fitter(hists_C[12],"C",12,-300,500,150,90,100,120);
    fitter(hists_C[13],"C",13,200,800,550,60,500,100);
    fitter(hists_C[14],"C",14,-500,200,-100,70,-200,100);
    fitter(hists_C[15],"C",15,0,800,400,90,150,120);
    fitter(hists_C[16],"C",16,-700,200,-200,110,-500,150);
    fitter(hists_C[17],"C",17,-900,-400,-600,100,-600,50);
    fitter(hists_C[18],"C",18,-1000,-300,-700,60,-700,100);
    fitter(hists_C[19],"C",19,-600,0,-270,60,-270,100);
    fitter(hists_C[20],"C",20,-400,150,-100,60,-100,100);
    fitter(hists_C[21],"C",21,150,650,400,50,400,80);
    fitter(hists_C[22],"C",22,300,800,580,50,550,80);
    fitter(hists_C[23],"C",23,-100,350,150,30,150,80);
    c1->SaveAs((std::string("hptdc_doubleSumGausAll_400b_withoutATLAS_C_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    */
}