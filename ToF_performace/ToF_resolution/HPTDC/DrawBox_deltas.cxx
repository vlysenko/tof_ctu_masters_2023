#include <iostream>
#include <iomanip>
#include <sstream>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <cmath>
#include <limits>
#include <TROOT.h>
#include <TStyle.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLatex.h>
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"


int main(int argc, char **argv)
{

    if (argc != 2)
    {
        std::cout << "Requires 2 parameters\n";
        return 1;
    }

    //SetAtlasStyle();
    TCanvas *c1 = new TCanvas("c1", "c1", 0., 0., 800, 600);
    TFile inf(argv[1]); //delta_t distributions after likelihood, produced in "times_cut.cxx"

    // setup

    auto n_bin = 400;
    //auto n_bin = 200;

    auto trains = std::vector<std::string>{"0","1","2","3"};
    auto bars = std::vector<std::string>{"AB", "AC", "AD", "BC", "BD", "CD"};
    
    std::vector<std::vector<TH1D *>> hists_A(trains.size(),std::vector<TH1D *>(bars.size())), hists_C(trains.size(),std::vector<TH1D *>(bars.size()));

    for (size_t i = 0; i < trains.size(); i++)
        for (size_t j = 0; j < bars.size(); j++)
        {
            hists_A[i][j] = (TH1D *)inf.Get((trains[i] + bars[j] + std::string("_A_delta_t_cutted")).c_str());
            hists_C[i][j] = (TH1D *)inf.Get((trains[i] + bars[j] + std::string("_C_delta_t_cutted")).c_str());
        }
    
    // start
    int runNumber = 428770;
    //int runNumber = 429027;
    
    auto vecA = std::vector<std::vector<float>>(4, std::vector<float>(6, 0));
    auto vecC = std::vector<std::vector<float>>(4, std::vector<float>(6, 0));

    
    auto fitter = [&](TH1D *hist)
    {
        TF1* gaus1 = new TF1("gaus1", "gaus");
        gaus1->SetLineColor(2);  
        TFitResultPtr r = hist->Fit("gaus1","S");
        float cell = r->Parameters()[2];
        return cell;
    };

    
    for (size_t i = 0; i < trains.size(); i++)
        for (size_t j = 0; j < bars.size(); j++)
        {
            vecA[i][j]=fitter(hists_A[i][j]);
            vecC[i][j]=fitter(hists_C[i][j]);
        }
    
    // drawBox with res
    TCanvas *c2 = new TCanvas("c2", "c2", 600, 400);
    auto names_for_drawBox = std::vector<std::string>{"A", "C"};
    auto good_names_for_drawBox = std::vector<std::string>{"#Delta t, side-A,", "#Delta t, side-C,"};

    std::vector<TH2D *> hists2d_for_drawBox(names_for_drawBox.size());

    for (size_t i = 0; i < names_for_drawBox.size(); i++)
        hists2d_for_drawBox[i] = new TH2D((names_for_drawBox[i] + std::string("_h2d")).c_str(), (names_for_drawBox[i] + std::string("_res_2d")).c_str(), 6, 0, 4, 4, 0, 4);

    for (size_t i = 1; i < 5; i++)
        for (size_t j = 1; j < 7; j++)
        {
            float a=100*vecA[i-1][j-1];
            a=round(a);
            a=a/100;
            hists2d_for_drawBox[0]->SetBinContent(j, i, a);
            float b=100*vecC[i-1][j-1];
            b=round(b);
            b=b/100;
            hists2d_for_drawBox[1]->SetBinContent(j, i, b);
        }

    for (size_t general = 0; general < names_for_drawBox.size(); general++)
    {
        for (size_t i = 1; i < 5; i++)
        {
            hists2d_for_drawBox[general]->GetYaxis()->SetBinLabel(i, (trains[i-1]).c_str());
            hists2d_for_drawBox[general]->GetXaxis()->SetLabelSize(0.1);
            hists2d_for_drawBox[general]->GetYaxis()->SetLabelSize(0.1);
            hists2d_for_drawBox[general]->GetXaxis()->SetTitleSize(0.05);
            hists2d_for_drawBox[general]->GetYaxis()->SetTitleSize(0.05);
            hists2d_for_drawBox[general]->GetZaxis()->SetRangeUser(20,160);
            //hists2d_for_drawBox[general]->GetZaxis()->SetRangeUser(20,90);
            hists2d_for_drawBox[general]->SetMarkerSize(2);
        }
        hists2d_for_drawBox[general]->GetYaxis()->SetTitle("Trains");
        hists2d_for_drawBox[general]->GetXaxis()->SetTitle("Bar combinations");
        hists2d_for_drawBox[general]->SetTitle((good_names_for_drawBox[general] + std::string(" Run ") + std::to_string(runNumber)  + std::string(", [ps]") ).c_str());
    }
    for (size_t general = 0; general < names_for_drawBox.size(); general++)
        for (size_t i = 1; i < 7; i++)
            hists2d_for_drawBox[general]->GetXaxis()->SetBinLabel(i, (bars[i-1]).c_str());

    // write in pdf
    gStyle->SetPalette(55);
    c2->Clear();
    c2->SaveAs((std::string("DrawBox_resolution_deltas_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t general = 0; general < names_for_drawBox.size(); general++)
    {
        hists2d_for_drawBox[general]->SetStats(0);
        hists2d_for_drawBox[general]->Draw("text colz");
        c2->SaveAs((std::string("DrawBox_resolution_deltas_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c2->Clear();
    }   
    c2->SaveAs((std::string("DrawBox_resolution_deltas_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());

}