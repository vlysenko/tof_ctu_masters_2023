#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <limits>

#include <TRandom3.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TFormula.h>
#include <TF1.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TColor.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TLatex.h>
#include <TMath.h>
#include <TVirtualFFT.h> 

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

int main(int argc, char **argv)
{

    if (argc != 3)
    {
        std::cout << "Requires 3 parameters\n";
        return 1;
    }

    // setup
    SetAtlasStyle();

    TCanvas* c1 = new TCanvas("c1","Cc1",0.,0.,800,600);
	TFile *inf_raw_hptdc = new TFile(argv[1], "READ"); //raw times after calib, produced with "hptdc.cxx"
    TFile *inf_raw = new TFile(argv[2], "READ"); //raw times before calib, full region, like in centers (can be out of 1-1024)
	
    auto hists_raw_times = std::vector<std::vector<TH1D *>>(2, std::vector<TH1D *>(16));
    auto hists_raw_times_before = std::vector<std::vector<TH1D *>>(2, std::vector<TH1D *>(16));

    auto stations = std::vector<std::string>{"A", "C"};
    auto chan_names = std::vector<std::string>{"0A", "0B", "0C", "0D", "1A", "1B", "1C", "1D", "2A", "2B", "2C", "2D", "3A", "3B", "3C", "3D"};
    auto channels = std::vector<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

	auto combinations = std::vector<std::vector<std::string>>{
        {"0AB", "0AC", "0AD", "0BC", "0BD", "0CD"},
        {"1AB", "1AC", "1AD", "1BC", "1BD", "1CD"},
        {"2AB", "2AC", "2AD", "2BC", "2BD", "2CD"},
        {"3AB", "3AC", "3AD", "3BC", "3BD", "3CD"}};
	

	for (size_t i = 0; i < 2; i++)
        for (size_t j = 0; j < 16; j++)
            hists_raw_times[i][j] = (TH1D*)inf_raw_hptdc->Get((std::to_string(channels[j]) + "_" + stations[i] + std::string("_out_rawTime")).c_str());

    for (size_t i = 0; i < 2; i++)
        for (size_t j = 0; j < 16; j++)
            hists_raw_times_before[i][j] = (TH1D*)inf_raw->Get((std::to_string(channels[j]) + "_" + stations[i] + std::string("_rawTime")).c_str());

	int runNumber=429027;
    //int runNumber=428770;

	//analysis
	auto ranges = [&](TH1D *hist, int userRangeDown, int userRangeUp)
    {
        hist->GetXaxis()->SetRangeUser(userRangeDown, userRangeUp);
    };
    auto ranges_x2 = [&](TH1D *hist, TH1D *hist2, int userRangeDown, int userRangeUp)
    {
        hist->GetXaxis()->SetRangeUser(userRangeDown, userRangeUp);
        hist2->GetXaxis()->SetRangeUser(userRangeDown, userRangeUp);
    };
    auto drawATLASstuff2 = [&](std::string fileName, float label_x, float label_y, std::string side, std::string channel)
    {
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        ATLASLabel_WithoutATLAS(label_x,(label_y+0.11),"Internal");
        latex.DrawLatex(label_x,(label_y),(std::string("Run ") + std::to_string(runNumber)).c_str());
        latex.DrawLatex(label_x,(label_y-0.055),(std::string("Side ") + side + ", " + channel).c_str());
        c1->SaveAs((fileName + "_" + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    };

    
    ranges(hists_raw_times[0][0],800,1000);
    ranges(hists_raw_times[0][1],800,1000);
    ranges(hists_raw_times[0][2],800,1000);
    ranges(hists_raw_times[0][3],800,1000);
    ranges(hists_raw_times[0][4],800,1000);
    ranges(hists_raw_times[0][5],800,1000);
    ranges(hists_raw_times[0][6],800,1000);
    ranges(hists_raw_times[0][7],800,1000);
    //ranges(hists_raw_times[0][8],800,1000); //429027
    ranges(hists_raw_times[0][8],1000,1200); //428770
    //ranges(hists_raw_times[0][9],800,1000); //429027
    ranges(hists_raw_times[0][9],1000,1200); //428770
    ranges(hists_raw_times[0][10],800,1000);
    //ranges(hists_raw_times[0][11],800,1000); //429027
    ranges(hists_raw_times[0][11],800,1100); //428770
    ranges(hists_raw_times[0][12],800,1000);
    //ranges(hists_raw_times[0][13],1000,1200); //429027
    ranges(hists_raw_times[0][13],800,1000); //428770
    ranges(hists_raw_times[0][14],900,1100);
    ranges(hists_raw_times[0][15],800,1000);

    //ranges(hists_raw_times[1][0],500,700); //429027
    ranges(hists_raw_times[1][0],150,350); //428770
    ranges(hists_raw_times[1][1],150,350);
    ranges(hists_raw_times[1][2],150,350);
    ranges(hists_raw_times[1][3],150,350);
    ranges(hists_raw_times[1][4],150,350);
    ranges(hists_raw_times[1][5],150,350);
    ranges(hists_raw_times[1][6],-2100,-1800);
    ranges(hists_raw_times[1][7],150,350);
    ranges(hists_raw_times[1][8],100,300);
    ranges(hists_raw_times[1][9],150,350);
    ranges(hists_raw_times[1][10],150,350);
    //ranges(hists_raw_times[1][11],250,450); //429027
    ranges(hists_raw_times[1][11],150,350); //428770
    ranges(hists_raw_times[1][12],150,350);
    //ranges(hists_raw_times[1][13],600,800); //429027
    ranges(hists_raw_times[1][13],150,350); //428770
    ranges(hists_raw_times[1][14],150,350);
    ranges(hists_raw_times[1][15],150,350);

    //previous
    ranges(hists_raw_times_before[0][0],900,1024);
    ranges(hists_raw_times_before[0][1],900,1024);
    ranges(hists_raw_times_before[0][2],900,1024);
    ranges(hists_raw_times_before[0][3],900,1024);
    ranges(hists_raw_times_before[0][4],900,1024);
    ranges(hists_raw_times_before[0][5],900,1024);
    ranges(hists_raw_times_before[0][6],900,1024);
    ranges(hists_raw_times_before[0][7],900,1024);
    ranges(hists_raw_times_before[0][8],900,1024);
    ranges(hists_raw_times_before[0][9],900,1024);
    ranges(hists_raw_times_before[0][10],900,1024);
    ranges(hists_raw_times_before[0][11],900,1024);
    ranges(hists_raw_times_before[0][12],900,1024);
    ranges(hists_raw_times_before[0][13],900,1024);
    ranges(hists_raw_times_before[0][14],900,1024);
    ranges(hists_raw_times_before[0][15],900,1024);

    ranges(hists_raw_times_before[1][0],190,310);
    ranges(hists_raw_times_before[1][1],190,310);
    ranges(hists_raw_times_before[1][2],190,310);
    ranges(hists_raw_times_before[1][3],190,310);
    ranges(hists_raw_times_before[1][4],190,310);
    ranges(hists_raw_times_before[1][5],190,310);
    ranges(hists_raw_times_before[1][6],190,310);
    ranges(hists_raw_times_before[1][7],190,310);
    ranges(hists_raw_times_before[1][8],190,310);
    ranges(hists_raw_times_before[1][9],190,310);
    ranges(hists_raw_times_before[1][10],190,310);
    ranges(hists_raw_times_before[1][11],190,310);
    ranges(hists_raw_times_before[1][12],190,310);
    ranges(hists_raw_times_before[1][13],190,310);
    ranges(hists_raw_times_before[1][14],190,310);
    ranges(hists_raw_times_before[1][15],190,310);

    
    c1->SaveAs((std::string("hptdc_raw_close_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < 2; i++)
        for (size_t j = 0; j < 16; j++)
        {
            hists_raw_times[i][j]->SetFillColor(kBlue);
            hists_raw_times[i][j]->Draw("bar");
            drawATLASstuff2("hptdc_raw_close",0.19,0.83,stations[i],chan_names[j]);
        }  
    c1->SaveAs((std::string("hptdc_raw_close_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();

    c1->SaveAs((std::string("hptdc_raw_before_close_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < 2; i++)
        for (size_t j = 0; j < 16; j++)
        {
            hists_raw_times_before[i][j]->SetFillColor(kBlue);
            hists_raw_times_before[i][j]->Draw("bar");
            drawATLASstuff2("hptdc_raw_before_close",0.19,0.83,stations[i],chan_names[j]);
        }  
    c1->SaveAs((std::string("hptdc_raw_before_close_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();
    
    //both
    ranges_x2(hists_raw_times_before[0][0],hists_raw_times[0][0],860,1024); //429027
    //ranges_x2(hists_raw_times_before[0][0],hists_raw_times[0][0],900,1024); //428770
    ranges_x2(hists_raw_times_before[0][1],hists_raw_times[0][1],900,1024);
    ranges_x2(hists_raw_times_before[0][2],hists_raw_times[0][2],860,1024); //429027
    //ranges_x2(hists_raw_times_before[0][2],hists_raw_times[0][2],900,1024); //428770
    ranges_x2(hists_raw_times_before[0][3],hists_raw_times[0][3],860,1024);
    ranges_x2(hists_raw_times_before[0][4],hists_raw_times[0][4],900,1024);
    ranges_x2(hists_raw_times_before[0][5],hists_raw_times[0][5],860,1024); //429027
    ranges_x2(hists_raw_times_before[0][6],hists_raw_times[0][6],860,1024); //429027
    //ranges_x2(hists_raw_times_before[0][5],hists_raw_times[0][5],900,1024); //428770
    //ranges_x2(hists_raw_times_before[0][6],hists_raw_times[0][6],900,1024); //428770
    ranges_x2(hists_raw_times_before[0][7],hists_raw_times[0][7],900,1024);
    ranges_x2(hists_raw_times_before[0][8],hists_raw_times[0][8],900,1050); //429027
    //ranges_x2(hists_raw_times_before[0][8],hists_raw_times[0][8],900,1150); //428770
    ranges_x2(hists_raw_times_before[0][9],hists_raw_times[0][9],860,1050); //429027
    //ranges_x2(hists_raw_times_before[0][9],hists_raw_times[0][9],900,1150); //428770
    ranges_x2(hists_raw_times_before[0][10],hists_raw_times[0][10],900,1024);
    ranges_x2(hists_raw_times_before[0][11],hists_raw_times[0][11],900,1024);
    ranges_x2(hists_raw_times_before[0][12],hists_raw_times[0][12],900,1024);
    ranges_x2(hists_raw_times_before[0][13],hists_raw_times[0][13],860,1200); //429027
    //ranges_x2(hists_raw_times_before[0][13],hists_raw_times[0][13],900,1024); //428770
    ranges_x2(hists_raw_times_before[0][14],hists_raw_times[0][14],900,1100); //429027
    //ranges_x2(hists_raw_times_before[0][14],hists_raw_times[0][14],900,1024); //428770
    ranges_x2(hists_raw_times_before[0][15],hists_raw_times[0][15],900,1024);

    ranges_x2(hists_raw_times_before[1][0],hists_raw_times[1][0],150,700); //429027
    //ranges_x2(hists_raw_times_before[1][0],hists_raw_times[1][0],150,350); //428770
    ranges_x2(hists_raw_times_before[1][1],hists_raw_times[1][1],150,350);
    ranges_x2(hists_raw_times_before[1][2],hists_raw_times[1][2],150,350);
    ranges_x2(hists_raw_times_before[1][3],hists_raw_times[1][3],150,350);
    ranges_x2(hists_raw_times_before[1][4],hists_raw_times[1][4],150,350);
    ranges_x2(hists_raw_times_before[1][5],hists_raw_times[1][5],150,350);
    ranges_x2(hists_raw_times_before[1][6],hists_raw_times[1][6],-2100,350);
    ranges_x2(hists_raw_times_before[1][7],hists_raw_times[1][7],150,350);
    ranges_x2(hists_raw_times_before[1][8],hists_raw_times[1][8],0,350); //429027
    //ranges_x2(hists_raw_times_before[1][8],hists_raw_times[1][8],150,350); //428770
    ranges_x2(hists_raw_times_before[1][9],hists_raw_times[1][9],150,350);
    ranges_x2(hists_raw_times_before[1][10],hists_raw_times[1][10],150,350);
    ranges_x2(hists_raw_times_before[1][11],hists_raw_times[1][11],150,450);
    ranges_x2(hists_raw_times_before[1][12],hists_raw_times[1][12],150,350);
    ranges_x2(hists_raw_times_before[1][13],hists_raw_times[1][13],150,800); //429027
    //ranges_x2(hists_raw_times_before[1][13],hists_raw_times[1][13],150,350); //428770
    ranges_x2(hists_raw_times_before[1][14],hists_raw_times[1][14],150,350);
    ranges_x2(hists_raw_times_before[1][15],hists_raw_times[1][15],150,1200);

    c1->SaveAs((std::string("hptdc_raw_both_close_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < 2; i++)
        for (size_t j = 0; j < 16; j++)
        {
            hists_raw_times_before[i][j]->SetFillColor(60);
            hists_raw_times[i][j]->SetFillColor(2);
            hists_raw_times[i][j]->SetFillStyle(3001);

            hists_raw_times_before[i][j]->Draw("bar");
            hists_raw_times[i][j]->Draw("same bar");
            TLegend* legend = new TLegend(0.69,0.79,0.94,0.93);
            legend->SetBorderSize(0);
            legend->AddEntry(hists_raw_times[i][j],"Calibrated","f");
            legend->AddEntry(hists_raw_times_before[i][j],"Uncalibrated","f");
            legend->SetTextSize(0.04);
            legend->Draw();
            drawATLASstuff2("hptdc_raw_both_close",0.19,0.83,stations[i],chan_names[j]);
        }  
    c1->SaveAs((std::string("hptdc_raw_both_close_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();
    

}