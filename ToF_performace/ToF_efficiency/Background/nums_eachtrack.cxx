#include <iostream>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <cmath>
#include <limits>
#include <TROOT.h>
#include <TStyle.h>

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        std::cout << "Requires 2 parameters\n";
        return 1;
    }
    
    TCanvas *c1=new TCanvas("c1", "c1", 600, 400);
    TFile *inf = new TFile(argv[1], "READ");
    TTree *tree_data = inf->Get<TTree>("CollectionTree");
    size_t nentries = tree_data->GetEntries();

    //setup

    auto n_bin = 200;
    TFile *outf = new TFile("out.root", "RECREATE");

    auto names = std::vector<std::string>{"0A","0B","0C","0D","1A","1B","1C","1D","2A","2B","2C","2D","3A","3B","3C","3D"};
    auto channels = std::vector<int>{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};

    //start

    std::vector<float> *toFHit_channel = nullptr, *aFPTrack_xLocal = nullptr, *toFHit_trainID = nullptr,
                       *toFHit_stationID = nullptr, *aFPTrack_stationID = nullptr, *toFHit_time = nullptr;

    int runNumber;

    tree_data->SetBranchAddress("ToFHit_channel", &toFHit_channel);
    tree_data->SetBranchAddress("AFPTrack_xLocal", &aFPTrack_xLocal);
    tree_data->SetBranchAddress("ToFHit_trainID", &toFHit_trainID);
    tree_data->SetBranchAddress("ToFHit_stationID", &toFHit_stationID);
    tree_data->SetBranchAddress("AFPTrack_stationID", &aFPTrack_stationID);
    tree_data->SetBranchAddress("ToFHit_time", &toFHit_time);
    tree_data->SetBranchAddress("RunNumber", &runNumber);

    std::vector<int> N_sitA(4, 0), N_sitC(4, 0);
    auto N_sit_tof_A_ON = std::vector<std::vector<int>>(4, std::vector<int>(16, 0));
    auto N_sit_tof_C_ON = std::vector<std::vector<int>>(4, std::vector<int>(16, 0));
    auto N_sit_tof_A_OFF = std::vector<std::vector<int>>(4, std::vector<int>(16, 0));
    auto N_sit_tof_C_OFF = std::vector<std::vector<int>>(4, std::vector<int>(16, 0));
    auto eff_A_ON = std::vector<std::vector<float>>(4, std::vector<float>(16, 0));
    auto eff_C_ON = std::vector<std::vector<float>>(4, std::vector<float>(16, 0));
    auto eff_A_OFF = std::vector<std::vector<float>>(4, std::vector<float>(16, 0));
    auto eff_C_OFF = std::vector<std::vector<float>>(4, std::vector<float>(16, 0));

    for (size_t i = 0; i < nentries; i++)
    //for (size_t i = 0; i < 10; i++)
    {
        auto vecToFA_ON = std::vector<std::vector<std::vector<float>>>(4, std::vector<std::vector<float>>(16, std::vector<float>()));
        auto vecToFC_ON = std::vector<std::vector<std::vector<float>>>(4, std::vector<std::vector<float>>(16, std::vector<float>()));
        auto vecToFA_OFF = std::vector<std::vector<std::vector<float>>>(4, std::vector<std::vector<float>>(16, std::vector<float>()));
        auto vecToFC_OFF = std::vector<std::vector<std::vector<float>>>(4, std::vector<std::vector<float>>(16, std::vector<float>()));
        auto vecSiTA = std::vector<std::vector<float>>(4, std::vector<float>());
        auto vecSiTC = std::vector<std::vector<float>>(4, std::vector<float>());
        if (i % 500000 == 0)
            std::cout << i/500000 << " from " << nentries/500000 << "\n";
        tree_data->GetEntry(i);
        if (toFHit_channel->size() == 0 || aFPTrack_xLocal->size() == 0)
            continue;
        //cut on only 1 train per event
        std::vector<int> vec_train_A(4, 0), vec_train_C(4, 0);
        for (size_t j = 0; j < toFHit_trainID->size(); j++)
        {
            for (size_t h = 0; h < 4; h++)
            {
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 0)
                    vec_train_A[h] = 1;
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 3)
                    vec_train_C[h] = 1;
            }
        }

        int sum_train_A = 0, sum_train_C = 0;
        for (size_t h = 0; h < 4; h++)
        {
            sum_train_A += vec_train_A[h];
            sum_train_C += vec_train_C[h];
        }

        //cut in only 1 track per event
        int sum_tracks_A = 0, sum_tracks_C = 0;
        for (size_t j = 0; j < aFPTrack_stationID->size(); j++)
        {
            if (aFPTrack_stationID->at(j) == 0)
                sum_tracks_A++;
            if (aFPTrack_stationID->at(j) == 3)
                sum_tracks_C++;
        }

        //cut on only 1 hit in 1 channel
        auto count_chans_A = std::vector<int>{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        auto count_chans_C = std::vector<int>{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        for (size_t j = 0; j < toFHit_channel->size(); j++)
        {
            if (toFHit_stationID->at(j) == 0)
            {
                size_t k = toFHit_channel->at(j);
                count_chans_A[k]++;
            }
            if (toFHit_stationID->at(j) == 3)
            {
                size_t k = toFHit_channel->at(j);
                count_chans_C[k]++;
            }
        }
        int count_hitsInChans=0;
        for (size_t k = 0; k < 16; k++)              
            if (count_chans_A[k] > 1 || count_chans_C[k] > 1)
                count_hitsInChans++;
        if (count_hitsInChans>0)
            continue;

        //main loop for SiT&&ToF
        
        if (sum_tracks_A == 1 || sum_tracks_C == 1)
        {
            for (size_t j = 0; j < toFHit_channel->size(); ++j)
            {
                int chan = int(toFHit_channel->at(j));
                for (size_t k = 0; k < aFPTrack_xLocal->size(); ++k)
                {
                    if (aFPTrack_stationID->at(k) == toFHit_stationID->at(j) && chan < 16)
                    {
                        //side-A
                        if (toFHit_stationID->at(j) == 0 && sum_tracks_A == 1)
                        {
                            if (aFPTrack_xLocal->at(k)<-2.0 && aFPTrack_xLocal->at(k)>-4.9)
                                for (size_t h = 0; h < 16; ++h)
                                    if (chan==h)
                                        vecToFA_OFF[0][h].push_back(aFPTrack_xLocal->at(k));
                            if (aFPTrack_xLocal->at(k)<-5.0 && aFPTrack_xLocal->at(k)>-8.0)
                                for (size_t h = 0; h < 16; ++h)
                                    if (chan==h)
                                        vecToFA_OFF[1][h].push_back(aFPTrack_xLocal->at(k));
                            if (aFPTrack_xLocal->at(k)<-8.1 && aFPTrack_xLocal->at(k)>-13.0)
                                for (size_t h = 0; h < 16; ++h)
                                    if (chan==h)
                                        vecToFA_OFF[2][h].push_back(aFPTrack_xLocal->at(k));
                            if (aFPTrack_xLocal->at(k)<-13.1 && aFPTrack_xLocal->at(k)>-15.0)
                                for (size_t h = 0; h < 16; ++h)
                                    if (chan==h)
                                        vecToFA_OFF[3][h].push_back(aFPTrack_xLocal->at(k));
                            //1 train ON
                            if (sum_train_A == 1)
                            {
                                if (aFPTrack_xLocal->at(k)<-2.0 && aFPTrack_xLocal->at(k)>-4.9)
                                    for (size_t h = 0; h < 16; ++h)
                                        if (chan==h)
                                            vecToFA_ON[0][h].push_back(aFPTrack_xLocal->at(k));
                                if (aFPTrack_xLocal->at(k)<-5.0 && aFPTrack_xLocal->at(k)>-8.0)
                                    for (size_t h = 0; h < 16; ++h)
                                        if (chan==h)
                                            vecToFA_ON[1][h].push_back(aFPTrack_xLocal->at(k));
                                if (aFPTrack_xLocal->at(k)<-8.1 && aFPTrack_xLocal->at(k)>-13.0)
                                    for (size_t h = 0; h < 16; ++h)
                                        if (chan==h)
                                            vecToFA_ON[2][h].push_back(aFPTrack_xLocal->at(k));
                                if (aFPTrack_xLocal->at(k)<-13.1 && aFPTrack_xLocal->at(k)>-15.0)
                                    for (size_t h = 0; h < 16; ++h)
                                        if (chan==h)
                                            vecToFA_ON[3][h].push_back(aFPTrack_xLocal->at(k));
                            }
                        }
                        //side-C
                        if (toFHit_stationID->at(j) == 3 && sum_tracks_C == 1)
                        {
                            if (aFPTrack_xLocal->at(k)<-2.0 && aFPTrack_xLocal->at(k)>-5.3)
                                for (size_t h = 0; h < 16; ++h)
                                    if (chan==h)
                                        vecToFC_OFF[0][h].push_back(aFPTrack_xLocal->at(k));
                            if (aFPTrack_xLocal->at(k)<-5.4 && aFPTrack_xLocal->at(k)>-8.4)
                                for (size_t h = 0; h < 16; ++h)
                                    if (chan==h)
                                        vecToFC_OFF[1][h].push_back(aFPTrack_xLocal->at(k));
                            if (aFPTrack_xLocal->at(k)<-8.5 && aFPTrack_xLocal->at(k)>-13.4)
                                for (size_t h = 0; h < 16; ++h)
                                    if (chan==h)
                                        vecToFC_OFF[2][h].push_back(aFPTrack_xLocal->at(k));
                            if (aFPTrack_xLocal->at(k)<-13.5 && aFPTrack_xLocal->at(k)>-15.0)
                                for (size_t h = 0; h < 16; ++h)
                                    if (chan==h)
                                        vecToFC_OFF[3][h].push_back(aFPTrack_xLocal->at(k));
                            //1 train ON
                            if (sum_train_C == 1)
                            {
                                if (aFPTrack_xLocal->at(k)<-2.0 && aFPTrack_xLocal->at(k)>-5.3)
                                    for (size_t h = 0; h < 16; ++h)
                                        if (chan==h)
                                            vecToFC_ON[0][h].push_back(aFPTrack_xLocal->at(k));
                                if (aFPTrack_xLocal->at(k)<-5.4 && aFPTrack_xLocal->at(k)>-8.4)
                                    for (size_t h = 0; h < 16; ++h)
                                        if (chan==h)
                                            vecToFC_ON[1][h].push_back(aFPTrack_xLocal->at(k));
                                if (aFPTrack_xLocal->at(k)<-8.5 && aFPTrack_xLocal->at(k)>-13.4)
                                    for (size_t h = 0; h < 16; ++h)
                                        if (chan==h)
                                            vecToFC_ON[2][h].push_back(aFPTrack_xLocal->at(k));
                                if (aFPTrack_xLocal->at(k)<-13.5 && aFPTrack_xLocal->at(k)>-15.0)
                                    for (size_t h = 0; h < 16; ++h)
                                        if (chan==h)
                                            vecToFC_ON[3][h].push_back(aFPTrack_xLocal->at(k));
                            }
                        }
                    }
                }
            }
        }

        
        //main loop for  SiT only
        for (size_t k = 0; k < aFPTrack_stationID->size(); ++k)
        {
            if (aFPTrack_stationID->at(k)==0 && sum_tracks_A==1)
            {
                if (aFPTrack_xLocal->at(k)<-2.0 && aFPTrack_xLocal->at(k)>-4.9)
                    vecSiTA[0].push_back(aFPTrack_xLocal->at(k));
                if (aFPTrack_xLocal->at(k)<-5.0 && aFPTrack_xLocal->at(k)>-8.0)
                    vecSiTA[1].push_back(aFPTrack_xLocal->at(k));
                if (aFPTrack_xLocal->at(k)<-8.1 && aFPTrack_xLocal->at(k)>-13.0)
                    vecSiTA[2].push_back(aFPTrack_xLocal->at(k));
                if (aFPTrack_xLocal->at(k)<-13.1 && aFPTrack_xLocal->at(k)>-15.0)
                    vecSiTA[3].push_back(aFPTrack_xLocal->at(k));
            }
            if (aFPTrack_stationID->at(k)==3 && sum_tracks_C==1)
            {
                if (aFPTrack_xLocal->at(k)<-2.0 && aFPTrack_xLocal->at(k)>-5.3)
                    vecSiTC[0].push_back(aFPTrack_xLocal->at(k));
                if (aFPTrack_xLocal->at(k)<-5.4 && aFPTrack_xLocal->at(k)>-8.4)
                    vecSiTC[1].push_back(aFPTrack_xLocal->at(k));
                if (aFPTrack_xLocal->at(k)<-8.5 && aFPTrack_xLocal->at(k)>-13.4)
                    vecSiTC[2].push_back(aFPTrack_xLocal->at(k));
                if (aFPTrack_xLocal->at(k)<-13.5 && aFPTrack_xLocal->at(k)>-15.0)
                    vecSiTC[3].push_back(aFPTrack_xLocal->at(k));
            }
        }

        //preparation for efficiency
        for (size_t h = 0; h < 4; ++h)
        {
            for (size_t j = 0; j < 16; ++j)
            {
                if (vecToFA_ON[h][j].size()>0)
                    N_sit_tof_A_ON[h][j]++;
                if (vecToFC_ON[h][j].size()>0)
                    N_sit_tof_C_ON[h][j]++;
                if (vecToFA_OFF[h][j].size()>0)
                    N_sit_tof_A_OFF[h][j]++;
                if (vecToFC_OFF[h][j].size()>0)
                    N_sit_tof_C_OFF[h][j]++;
            }
        }
        for (size_t j = 0; j < 4; ++j)
        {
            if (vecSiTA[j].size()>0)
                N_sitA[j]++;
            if (vecSiTC[j].size()>0)
                N_sitC[j]++;
        }
    }

    //calculating efficiency
    for (size_t h = 0; h < 4; ++h)
    {
        for (size_t j = 0; j < 16; ++j)
        {
            eff_A_ON[h][j]=N_sit_tof_A_ON[h][j]/static_cast<float>(N_sitA[(j/4)]);
            eff_C_ON[h][j]=N_sit_tof_C_ON[h][j]/static_cast<float>(N_sitC[(j/4)]);
            eff_A_OFF[h][j]=N_sit_tof_A_OFF[h][j]/static_cast<float>(N_sitA[(j/4)]);
            eff_C_OFF[h][j]=N_sit_tof_C_OFF[h][j]/static_cast<float>(N_sitC[(j/4)]);
        }
    }

    /*
    // output numbers
    std::cout << "1 Train cut OFF" << std::endl;
    std::cout << "side-A: ";
    for (size_t i = 0; i < names.size(); ++i)
        std::cout << eff_A_OFF[i] << ", ";
    std::cout << std::endl;
    std::cout << "side-C: ";
    for (size_t i = 0; i < names.size(); ++i)
        std::cout << eff_C_OFF[i] << ", ";
    std::cout << std::endl;

    std::cout << "1 Train cut ON" << std::endl;
    std::cout << "side-A: ";
    for (size_t i = 0; i < names.size(); ++i)
        std::cout << eff_A_ON[i] << ", ";
    std::cout << std::endl;
    std::cout << "side-C: ";
    for (size_t i = 0; i < names.size(); ++i)
        std::cout << eff_C_ON[i] << ", ";
    std::cout << std::endl;
    */

    // drawBox with effs
    int Nbin_drawBox = 4;
    TCanvas *c2 = new TCanvas("c2", "c2", 400, 400);
    auto trains = std::vector<std::string>{"0", "1", "2", "3"};
    auto bars = std::vector<std::string>{"A", "B", "C", "D"};
    /*
    auto names_for_drawBox = std::vector<std::vector<std::string>>{{"nums_1tr_A_Tr0","nums_1tr_C_Tr0", "nums_lot_tr_A_Tr0", "nums_lot_tr_C_Tr0"},
        {"nums_1tr_A_Tr1", "nums_1tr_C_Tr1","nums_lot_tr_A_Tr1","nums_lot_tr_C_Tr1"},{"nums_1tr_A_Tr2","nums_lot_tr_C_Tr2","nums_1tr_C_Tr2","nums_lot_tr_A_Tr2"},
        {"nums_1tr_A_Tr3", "nums_1tr_C_Tr3","nums_lot_tr_A_Tr3","nums_lot_tr_C_Tr3"}};
    auto good_names_for_drawBox = std::vector<std::vector<std::string>>{{"Numbers, ON, side-A, Train 0","Numbers, ON, side-C, Train 0", "Numbers, OFF, side-A, Train 0","Numbers, OFF, side-C, Train 0"},
        {"Numbers, ON, side-A, Train 1","Numbers, ON, side-C, Train 1","Numbers, OFF, side-A, Train 1","Numbers, OFF, side-C, Train 1"},
        {"Numbers, ON, side-A, Train 2","Numbers, ON, side-C, Train 2","Numbers, OFF, side-A, Train 2","Numbers, OFF, side-C, Train 2"},
        {"Numbers, ON, side-A, Train 3","Numbers, ON, side-C, Train 3","Numbers, OFF, side-A, Train 3","Numbers, OFF, side-C, Train 3"}};
    */
    auto names_for_drawBox = std::vector<std::vector<std::string>>{{"nums_1tr_A_Tr0","nums_1tr_A_Tr1", "nums_1tr_A_Tr2", "nums_1tr_A_Tr3"},
        {"nums_1tr_C_Tr0","nums_1tr_C_Tr1","nums_1tr_C_Tr2","nums_1tr_C_Tr3"},{"nums_lot_tr_A_Tr0","nums_lot_tr_A_Tr1","nums_lot_tr_A_Tr2","nums_lot_tr_A_Tr3"},
        {"nums_lot_tr_C_Tr0","nums_lot_tr_C_Tr1","nums_lot_tr_C_Tr2","nums_lot_tr_C_Tr3"}};
    auto good_names_for_drawBox = std::vector<std::vector<std::string>>{{"Numbers, ON, side-A, Train 0","Numbers, ON, side-A, Train 1","Numbers, ON, side-A, Train 2","Numbers, ON, side-A, Train 3"},
        {"Numbers, ON, side-C, Train 0","Numbers, ON, side-C, Train 1","Numbers, ON, side-C, Train 2","Numbers, ON, side-C, Train 3"},
        {"Numbers, OFF, side-A, Train 0","Numbers, OFF, side-A, Train 1","Numbers, OFF, side-A, Train 2","Numbers, OFF, side-A, Train 3"},
        {"Numbers, OFF, side-C, Train 0","Numbers, OFF, side-C, Train 1","Numbers, OFF, side-C, Train 2","Numbers, OFF, side-C, Train 3"}};

    auto hists2d_for_drawBox = std::vector<std::vector<TH2D *>>(4, std::vector<TH2D *>(4));

    for (size_t i = 0; i < 4; i++)
        for (size_t j = 0; j < 4; j++)
            hists2d_for_drawBox[i][j] = new TH2D((names_for_drawBox[i][j] + std::string("_h2d")).c_str(), (names_for_drawBox[i][j] + std::string("_eff_2d")).c_str(), Nbin_drawBox, 0, 4, Nbin_drawBox, 0, 4);

    auto effs_for_drawBox = std::vector<std::vector<std::vector<float>>>{eff_A_ON,eff_C_ON,eff_A_OFF,eff_C_OFF};

    for (size_t general_i = 0; general_i < 4; general_i++)
        for (size_t general_j = 0; general_j < 4; general_j++)
            for (size_t i = 1; i < 5; i++)
                for (size_t j = 1; j < 5; j++)
                {
                    float a=100*100*effs_for_drawBox[general_i][general_j][(4*(i-1))+(j-1)];
                    a=round(a);
                    a=a/100;
                    if (a>100) {a=100;}
                    hists2d_for_drawBox[general_i][general_j]->SetBinContent(j, i, a);
                }

    for (size_t general_i = 0; general_i < 4; general_i++)
        for (size_t general_j = 0; general_j < 4; general_j++)
        {
            for (size_t i = 1; i < 5; i++)
            {
                hists2d_for_drawBox[general_i][general_j]->GetXaxis()->SetBinLabel(i, (bars[i-1]).c_str());
                hists2d_for_drawBox[general_i][general_j]->GetYaxis()->SetBinLabel(i, (trains[i-1]).c_str());
                hists2d_for_drawBox[general_i][general_j]->GetXaxis()->SetLabelSize(0.1);
                hists2d_for_drawBox[general_i][general_j]->GetYaxis()->SetLabelSize(0.1);
                hists2d_for_drawBox[general_i][general_j]->GetXaxis()->SetTitleSize(0.05);
                hists2d_for_drawBox[general_i][general_j]->GetYaxis()->SetTitleSize(0.05);
                hists2d_for_drawBox[general_i][general_j]->GetZaxis()->SetRangeUser(0,100);
                hists2d_for_drawBox[general_i][general_j]->SetMarkerSize(2);
            }
            hists2d_for_drawBox[general_i][general_j]->GetYaxis()->SetTitle("Trains");
            hists2d_for_drawBox[general_i][general_j]->GetXaxis()->SetTitle("Bars");
            hists2d_for_drawBox[general_i][general_j]->SetTitle((std::string("%, ") + std::to_string(runNumber) + std::string(" Run, ") + good_names_for_drawBox[general_i][general_j]).c_str());
        }
        

    /*
    //write in root file
    for (size_t general = 0; general < names_for_drawBox.size(); general++)
    {
        hists2d_for_drawBox[general]->SetStats(0);
        hists2d_for_drawBox[general]->Draw("text colz");
        hists2d_for_drawBox[general]->Write();
    }   
    outf->Close();
    */

    // write in pdf
    gStyle->SetPalette(55);
    c2->Clear();
    c2->SaveAs((std::string("DrawBox_nums_oncpp_EachTrack_") + std::to_string(runNumber) + std::string("_ph_main.pdf[")).c_str());
    for (size_t general_i = 0; general_i < 4; general_i++)
        for (size_t general_j = 0; general_j < 4; general_j++)
        {
            hists2d_for_drawBox[general_i][general_j]->SetStats(0);
            hists2d_for_drawBox[general_i][general_j]->Draw("text colz");
            c2->SaveAs((std::string("DrawBox_nums_oncpp_EachTrack_") + std::to_string(runNumber) + std::string("_ph_main.pdf")).c_str());
            c2->Clear();
        }   
    c2->SaveAs((std::string("DrawBox_nums_oncpp_EachTrack_") + std::to_string(runNumber) + std::string("_ph_main.pdf]")).c_str());
}