#include <iostream>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <cmath>
#include <limits>
#include <TROOT.h>
#include <TStyle.h>

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        std::cout << "Requires 3 parameters\n";
        return 1;
    }

    TCanvas *c1=new TCanvas("c1", "c1", 600, 400);
    auto in_files = std::vector<TFile *>(2);
    in_files[0]=new TFile(argv[1], "READ");
    in_files[1]=new TFile(argv[2], "READ");

    //setup

    auto n_bin = 200;
    //TFile *outf = new TFile("out.root", "RECREATE");

    auto names = std::vector<std::string>{"0A","0B","0C","0D","1A","1B","1C","1D","2A","2B","2C","2D","3A","3B","3C","3D"};
    auto channels = std::vector<int>{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};

    //start

    std::vector<float> *toFHit_channel = nullptr, *aFPTrack_xLocal = nullptr, *toFHit_trainID = nullptr,
                       *toFHit_stationID = nullptr, *aFPTrack_stationID = nullptr, *toFHit_time = nullptr;

    int runNumber;
    int Nbin_drawBox = 16;
    TCanvas *c2 = new TCanvas("c2", "c2", 400, 400);
    auto names_for_drawBox = std::vector<std::vector<std::string>>{{"nums_lot_tr_A_Tr0","nums_lot_tr_A_Tr1","nums_lot_tr_A_Tr2","nums_lot_tr_A_Tr3"},
        {"nums_lot_tr_C_Tr0","nums_lot_tr_C_Tr1","nums_lot_tr_C_Tr2","nums_lot_tr_C_Tr3"}};
    auto good_names_for_drawBox = std::vector<std::vector<std::string>>{{"Hit corr, side-A, Train 0","Hit corr, side-A, Train 1","Hit corr, side-A, Train 2","Hit corr, side-A, Train 3"},
        {"Hit corr, side-C, Train 0","Hit corr, side-C, Train 1","Hit corr, side-C, Train 2","Hit corr, side-C, Train 3"}};

    auto hists2d_for_drawBox = std::vector<std::vector<TH2D *>>(2, std::vector<TH2D *>(4));

    for (size_t i = 0; i < 2; i++)
        for (size_t j = 0; j < 4; j++)
            hists2d_for_drawBox[i][j] = new TH2D((names_for_drawBox[i][j] + std::string("_h2d")).c_str(), (names_for_drawBox[i][j] + std::string("_eff_2d")).c_str(), Nbin_drawBox, 0, 16, Nbin_drawBox, 0, 16);

    for (size_t files = 0; files<in_files.size(); files++)
    {
        TTree *tree_data = in_files[files]->Get<TTree>("CollectionTree");
        size_t nentries = tree_data->GetEntries();
        tree_data->SetBranchAddress("ToFHit_channel", &toFHit_channel);
        tree_data->SetBranchAddress("AFPTrack_xLocal", &aFPTrack_xLocal);
        tree_data->SetBranchAddress("ToFHit_trainID", &toFHit_trainID);
        tree_data->SetBranchAddress("ToFHit_stationID", &toFHit_stationID);
        tree_data->SetBranchAddress("AFPTrack_stationID", &aFPTrack_stationID);
        tree_data->SetBranchAddress("ToFHit_time", &toFHit_time);
        tree_data->SetBranchAddress("RunNumber", &runNumber);
    
        for (size_t i = 0; i < nentries; i++)
        //for (size_t i = 0; i < 15000; i++)
        {
            if (i % 1000000 == 0)
                std::cout << i/1000000 << " from " << nentries/1000000 << "\n";
            tree_data->GetEntry(i);

            if (toFHit_channel->size() == 0 || aFPTrack_xLocal->size() == 0)
                continue;

            //cut on only 1 train per event
            std::vector<int> vec_train_A(4, 0), vec_train_C(4, 0);
            for (size_t j = 0; j < toFHit_trainID->size(); j++)
            {
                for (size_t h = 0; h < 4; h++)
                {
                    if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 0)
                        vec_train_A[h] = 1;
                    if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 3)
                        vec_train_C[h] = 1;
                }
            }
            int sum_train_A = 0, sum_train_C = 0;
            for (size_t h = 0; h < 4; h++)
            {
                sum_train_A += vec_train_A[h];
                sum_train_C += vec_train_C[h];
            }

            //cut in only 1 track per event
            int sum_tracks_A = 0, sum_tracks_C = 0;
            for (size_t j = 0; j < aFPTrack_stationID->size(); j++)
            {
                if (aFPTrack_stationID->at(j) == 0)
                    sum_tracks_A++;
                if (aFPTrack_stationID->at(j) == 3)
                    sum_tracks_C++;
            }
            if (sum_tracks_A > 1 || sum_tracks_C > 1)
                continue;

            //cut on only 1 hit in 1 channel
            auto count_chans_A = std::vector<int>{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            auto count_chans_C = std::vector<int>{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            for (size_t j = 0; j < toFHit_channel->size(); j++)
            {
                if (toFHit_stationID->at(j) == 0)
                {
                    size_t k = toFHit_channel->at(j);
                    count_chans_A[k]++;
                }
                if (toFHit_stationID->at(j) == 3)
                {
                    size_t k = toFHit_channel->at(j);
                    count_chans_C[k]++;
                }
            }
            int count_hitsInChans=0;
            for (size_t k = 0; k < 16; k++)              
                if (count_chans_A[k] > 1 || count_chans_C[k] > 1)
                    count_hitsInChans++;
            if (count_hitsInChans>0)
                continue;

            //main loop
            for (size_t j = 0; j < toFHit_channel->size(); ++j)
            {
                int chan_1 = int(toFHit_channel->at(j));
                for (size_t h = 0; h < toFHit_channel->size(); ++h)
                {
                    int chan_2 = int(toFHit_channel->at(h));
                    for (size_t k = 0; k < aFPTrack_xLocal->size(); ++k)
                    {
                        if (aFPTrack_stationID->at(k) == toFHit_stationID->at(j) && aFPTrack_stationID->at(k) == toFHit_stationID->at(h) && chan_1 < 16 && chan_2 < 16)
                        {
                            //side-A
                            if (toFHit_stationID->at(j) == 0)
                            {
                                if (aFPTrack_xLocal->at(k)<-2.0 && aFPTrack_xLocal->at(k)>-4.9)
                                    hists2d_for_drawBox[0][0]->Fill(chan_1, chan_2);
                                if (aFPTrack_xLocal->at(k)<-5.0 && aFPTrack_xLocal->at(k)>-8.0)
                                    hists2d_for_drawBox[0][1]->Fill(chan_1, chan_2);
                                if (aFPTrack_xLocal->at(k)<-8.1 && aFPTrack_xLocal->at(k)>-13.0)
                                    hists2d_for_drawBox[0][2]->Fill(chan_1, chan_2);
                                if (aFPTrack_xLocal->at(k)<-13.1 && aFPTrack_xLocal->at(k)>-15.0)
                                    hists2d_for_drawBox[0][3]->Fill(chan_1, chan_2);
                            }
                            //side-C
                            if (toFHit_stationID->at(j) == 3)
                            {
                                if (aFPTrack_xLocal->at(k)<-2.0 && aFPTrack_xLocal->at(k)>-5.3)
                                    hists2d_for_drawBox[1][0]->Fill(chan_1, chan_2);
                                if (aFPTrack_xLocal->at(k)<-5.4 && aFPTrack_xLocal->at(k)>-8.4)
                                    hists2d_for_drawBox[1][1]->Fill(chan_1, chan_2);
                                if (aFPTrack_xLocal->at(k)<-8.5 && aFPTrack_xLocal->at(k)>-13.4)
                                    hists2d_for_drawBox[1][2]->Fill(chan_1, chan_2);
                                if (aFPTrack_xLocal->at(k)<-13.5 && aFPTrack_xLocal->at(k)>-15.0)
                                    hists2d_for_drawBox[1][3]->Fill(chan_1, chan_2);
                            }
                        }
                    }
                }
            }    
        }
    }

    // drawBox
    auto trains = std::vector<std::string>{"0", "1", "2", "3"};
    auto bars = std::vector<std::string>{"A", "B", "C", "D"};

    for (size_t general_i = 0; general_i < 2; general_i++)
        for (size_t general_j = 0; general_j < 4; general_j++)
        {
            for (size_t i = 1; i < 17; i++)
            {
                hists2d_for_drawBox[general_i][general_j]->GetXaxis()->SetBinLabel(i, (names[i-1]).c_str());
                hists2d_for_drawBox[general_i][general_j]->GetYaxis()->SetBinLabel(i, (names[i-1]).c_str());
                //hists2d_for_drawBox[general_i][general_j]->GetXaxis()->SetLabelSize(0.1);
                //hists2d_for_drawBox[general_i][general_j]->GetYaxis()->SetLabelSize(0.1);
                //hists2d_for_drawBox[general_i][general_j]->GetXaxis()->SetTitleSize(0.05);
                //hists2d_for_drawBox[general_i][general_j]->GetYaxis()->SetTitleSize(0.05);
                //hists2d_for_drawBox[general_i][general_j]->GetZaxis()->SetRangeUser(0,100);
                //hists2d_for_drawBox[general_i][general_j]->SetMarkerSize(2);
            }
            hists2d_for_drawBox[general_i][general_j]->GetYaxis()->SetTitle("Channels");
            hists2d_for_drawBox[general_i][general_j]->GetXaxis()->SetTitle("Channels");
            hists2d_for_drawBox[general_i][general_j]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", ") + good_names_for_drawBox[general_i][general_j]).c_str());
        }
        

    /*
    //write in root file
    for (size_t general = 0; general < names_for_drawBox.size(); general++)
    {
        hists2d_for_drawBox[general]->SetStats(0);
        hists2d_for_drawBox[general]->Draw("text colz");
        hists2d_for_drawBox[general]->Write();
    }   
    outf->Close();
    */

    // write in pdf
    gStyle->SetPalette(55);
    c2->Clear();
    c2->SaveAs((std::string("DrawBox_hit_corr_EachTrack_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t general_i = 0; general_i < 2; general_i++)
        for (size_t general_j = 0; general_j < 4; general_j++)
        {
            hists2d_for_drawBox[general_i][general_j]->SetStats(0);
            hists2d_for_drawBox[general_i][general_j]->Draw("colz");
            c2->SaveAs((std::string("DrawBox_hit_corr_EachTrack_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
            c2->Clear();
        }   
    c2->SaveAs((std::string("DrawBox_hit_corr_EachTrack_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
}