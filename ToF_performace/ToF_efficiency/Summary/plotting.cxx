#include <iostream>
#include <fstream>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TCanvas.h>
#include <cmath>
#include <limits>
#include <TROOT.h>
#include <TStyle.h>
#include <TPad.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TClass.h>
#include <TAxis.h>
#include <string>
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        std::cout << "Requires 3 parameters\n";
        return 1;
    }
    SetAtlasStyle();
    //read list of files
    std::ifstream textfile;
    textfile.open(argv[1]);
    std::string key(argv[2]);

    auto in_files = std::vector<TFile *>();
    auto runs = std::vector<int>();
    std::string line;
    while(getline(textfile, line))
    { 
        auto temppos = line.find(".root");
        std::string temprun = line.substr(temppos-6,temppos);
        auto pos = temprun.find(".");
        std::string thisrun = temprun.substr(pos-6,pos);
        runs.push_back(stoi(thisrun));
        //here adding run numbers with cutting string 
        TFile *inf = new TFile(line.c_str(), "READ");
        in_files.push_back(inf);
    }
    textfile.close(); 

    //start
    TCanvas *c1=new TCanvas("c1", "c1", 1200, 200);
    std::vector<TH1D *> h_trains_A(4), h_trains_C(4);

    auto colors = std::vector<int>{1,2,4,8};
    auto markers = std::vector<int>{24,25,26,27};
    
    for (size_t i = 0; i < 4; i++)
    {
        h_trains_A[i] = new TH1D(("h_train_A_" + std::to_string(i)).c_str(), ("h_train_A_" + std::to_string(i)).c_str(), in_files.size(), 0, in_files.size());
        h_trains_C[i] = new TH1D(("h_train_C_" + std::to_string(i)).c_str(), ("h_train_C_" + std::to_string(i)).c_str(), in_files.size(), 0, in_files.size());

    }

    //main loop by files
    for (int file = 0; file<in_files.size(); file++)
    //for (size_t file = 8; file<9; file++)
    {
        if (file % 40 == 0)
                std::cout << file/40 << " from " << in_files.size()/40 << "\n";

        std::vector<float> eff_A_chans(16, 0), eff_C_chans(16, 0), eff_A_trains(4, 0), eff_C_trains(4, 0);
        TH2D* h2_chans_A = (TH2D*)in_files[file]->Get("eff_chans_A_h2d");
        TH2D* h2_chans_C = (TH2D*)in_files[file]->Get("eff_chans_C_h2d");
        TH2D* h2_trains_A = (TH2D*)in_files[file]->Get("eff_trains_A_h2d");
        TH2D* h2_trains_C = (TH2D*)in_files[file]->Get("eff_trains_C_h2d");

        for (size_t i = 1; i<5; i++)
        {
            if (std::isnan(h2_trains_A->GetBinContent(1,i))) 
            {eff_A_trains[i-1]=0;}
            else {eff_A_trains[i-1] = h2_trains_A->GetBinContent(1,i);}
            if (std::isnan(h2_trains_C->GetBinContent(1,i))) 
            {eff_C_trains[i-1]=0;}
            else {eff_C_trains[i-1] = h2_trains_C->GetBinContent(1,i);}
            
            for (size_t j = 1; j<5; j++)
            {
                if (std::isnan(h2_chans_A->GetBinContent(j,i))) 
                {eff_A_chans[(4*(i-1))+(j-1)]=0;}
                else {eff_A_chans[(4*(i-1))+(j-1)] = h2_chans_A->GetBinContent(j,i);}
                if (std::isnan(h2_chans_C->GetBinContent(j,i))) 
                {eff_C_chans[(4*(i-1))+(j-1)]=0;}
                else {eff_C_chans[(4*(i-1))+(j-1)] = h2_chans_C->GetBinContent(j,i);}
            }

            h_trains_A[i-1]->SetBinContent(file+1,eff_A_trains[i-1]);
            h_trains_C[i-1]->SetBinContent(file+1,eff_C_trains[i-1]);
        }      
    }

    int mysize=in_files.size();
    int x[2] = {0, mysize};
    int y0[2] = {0, 0};
    int y1[2] = {50, 50};
    int y2[2] = {100, 100};
    int x3[3] = {156, 156, 156};
    int y3[3] = {0, 50, 100};
    TGraph *gr_0 = new TGraph(2,x,y0);
    TGraph *gr_50 = new TGraph(2,x,y1);
    TGraph *gr_100 = new TGraph(2,x,y2);
    TGraph *gr_2023 = new TGraph(3,x3,y3);
    gr_0->SetMarkerStyle(20);
    gr_0->SetMarkerSize(0.01);
    gr_0->SetMarkerColor(17);
    gr_0->SetLineColor(17);
    gr_0->SetLineStyle(3);

    gr_50->SetMarkerStyle(20);
    gr_50->SetMarkerSize(0.01);
    gr_50->SetMarkerColor(17);
    gr_50->SetLineColor(17);
    gr_50->SetLineStyle(3);

    gr_100->SetMarkerStyle(20);
    gr_100->SetMarkerSize(0.01);
    gr_100->SetMarkerColor(17);
    gr_100->SetLineColor(17);
    gr_100->SetLineStyle(3);

    gr_2023->SetMarkerStyle(20);
    gr_2023->SetMarkerSize(0.01);
    gr_2023->SetMarkerColor(17);
    gr_2023->SetLineColor(17);
    gr_2023->SetLineStyle(2);

    //nice hists
    gStyle->SetLineWidth(1);
    for (size_t i = 0; i < 4; i++)
    {
        h_trains_A[i]->GetXaxis()->SetLabelSize(0.17);
        h_trains_A[i]->GetYaxis()->SetLabelSize(0.12);
        h_trains_A[i]->SetMarkerStyle(markers[i]);
        h_trains_A[i]->SetMarkerSize(1);
        h_trains_A[i]->SetMarkerColor(colors[i]);
        h_trains_A[i]->SetLineColor(colors[i]);
        h_trains_A[i]->SetLineWidth(2);
        h_trains_A[i]->GetYaxis()->SetRangeUser(-10,110);
        h_trains_A[i]->GetYaxis()->SetNdivisions(4, 5, 0, kTRUE);
        
        if (key == "2022")
        {
            h_trains_A[i]->GetXaxis()->SetBinLabel(1,"Jul");
            h_trains_A[i]->GetXaxis()->ChangeLabel(1,40,-1,-1,-1,-1,"Jul");
            h_trains_A[i]->GetXaxis()->ChangeLabel(37,40,-1,-1,-1,-1,"Aug");
            h_trains_A[i]->GetXaxis()->ChangeLabel(70,40,-1,-1,-1,-1,"Sep");
            h_trains_A[i]->GetXaxis()->ChangeLabel(73,40,-1,-1,-1,-1,"Oct");
            h_trains_A[i]->GetXaxis()->ChangeLabel(129,40,-1,-1,-1,-1,"Nov");
        }
        
        h_trains_C[i]->GetXaxis()->SetLabelSize(0.17);
        h_trains_C[i]->GetYaxis()->SetLabelSize(0.12);
        h_trains_C[i]->SetMarkerStyle(markers[i]);
        h_trains_C[i]->SetMarkerSize(1);
        h_trains_C[i]->SetMarkerColor(colors[i]);
        h_trains_C[i]->SetLineColor(colors[i]);
        h_trains_C[i]->SetLineWidth(2);
        h_trains_C[i]->GetYaxis()->SetRangeUser(0,100);
        h_trains_C[i]->GetYaxis()->SetNdivisions(4, 5, 0, kTRUE);
        
    }
    

    // write in pdf hists
    c1->SaveAs((std::string("Hist_effs_clean_") + key + std::string(".pdf[")).c_str());

    h_trains_A[0]->Draw("LP0");
    gr_0->Draw("same LP");
    gr_50->Draw("same LP");
    gr_100->Draw("same LP");
    if (key =="full")
        gr_2023->Draw("same LP");
    for (size_t i = 1; i < 4; i++)
        h_trains_A[i]->Draw("same LP0");
    
    TLatex latex1;
    latex1.SetNDC();
    latex1.SetTextSize(0.15);
    latex1.DrawLatex(0.04,0.8,"Side A");
    TLegend* legend1 = new TLegend(0.04,0.3,0.14,0.7);
    legend1->SetFillStyle(0);
    legend1->SetBorderSize(0);
    legend1->SetTextSize(0.1);
    for (size_t i = 0; i < 4; i++)
        legend1->AddEntry(h_trains_A[i],("Train " + std::to_string(i)).c_str(),"pl");
    legend1->Draw();
    c1->SaveAs((std::string("Hist_effs_clean_") + key + std::string(".pdf")).c_str());  
    c1->Clear();

    h_trains_A[0]->Draw("LP0");
    gr_0->Draw("same LP");
    gr_50->Draw("LP");
    gr_100->Draw("same LP");
    if (key =="full")
        gr_2023->Draw("same LP");
    for (size_t i = 1; i < 4; i++)
        h_trains_C[i]->Draw("same LP0");
    

    TLatex latex2;
    latex2.SetNDC();
    latex2.SetTextSize(0.15);
    latex2.DrawLatex(0.04,0.8,"Side C");
    TLegend* legend2 = new TLegend(0.04,0.3,0.14,0.7);
    legend2->SetFillStyle(0);
    legend2->SetBorderSize(0);
    legend2->SetTextSize(0.1);
    for (size_t i = 0; i < 4; i++)
        legend2->AddEntry(h_trains_C[i],("Train " + std::to_string(i)).c_str(),"pl");
    legend2->Draw();
    c1->SaveAs((std::string("Hist_effs_clean_") + key + std::string(".pdf")).c_str()); 
    c1->SaveAs((std::string("Hist_effs_clean_") + key + std::string(".pdf]")).c_str());

}