#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <limits>

#include <TRandom3.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TFormula.h>
#include <TF1.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TColor.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TLatex.h>
#include <TMath.h>

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

Double_t myFunc (Double_t *x, Double_t *pars) 
    {
        Float_t xx =x[0];
        Double_t f = 0;
        for (size_t i = 0; i < 16; i++)
            for (size_t j = 0; j < 16; j++)
                f = f + ( ((xx>i*16+j) && (xx<i*16+j+1)) * (pars[16+j]-pars[i]));      
        return f;
    }

int main(int argc, char **argv)
{

    if (argc != 3)
    {
        std::cout << "Requires 3 parameters\n";
        return 1;
    }

    // setup
    SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c1","Cc1",0.,0.,800,600);
    TFile *inf_sh = new TFile(argv[1], "READ"); //tof minus bs, produced in "all_tof_newT.cxx"

    TFile *inf = new TFile(argv[2], "READ"); //data file
    TTree *tree_data = inf->Get<TTree>("CollectionTree");
    size_t nentries = tree_data->GetEntries();

    std::vector<float> *toFHit_channel = nullptr, *toFHit_trainID = nullptr, *toFHit_stationID = nullptr, 
                       *toFHit_time = nullptr, *toFHit_bar = nullptr, *pv_z = nullptr, *pv_vertexType = nullptr,
                       *aFPTrack_xLocal = nullptr, *aFPTrack_stationID = nullptr;

    int runNumber;
    int eventNumber;
    int lumi_block;
    float beam_pos;
    float speed_of_light = TMath::C();

    tree_data->SetBranchAddress("ToFHit_channel", &toFHit_channel);
    tree_data->SetBranchAddress("ToFHit_trainID", &toFHit_trainID);
    tree_data->SetBranchAddress("ToFHit_stationID", &toFHit_stationID);
    tree_data->SetBranchAddress("ToFHit_time", &toFHit_time);
    tree_data->SetBranchAddress("ToFHit_bar", &toFHit_bar);
    tree_data->SetBranchAddress("AFPTrack_xLocal", &aFPTrack_xLocal);
    tree_data->SetBranchAddress("AFPTrack_stationID", &aFPTrack_stationID);
    tree_data->SetBranchAddress("RunNumber", &runNumber);
    tree_data->SetBranchAddress("EventNumber", &eventNumber);
    tree_data->SetBranchAddress("lumiBlock", &lumi_block);
    tree_data->SetBranchAddress("beam_pos_z", &beam_pos);
    tree_data->SetBranchAddress("PV_vertexType", &pv_vertexType);
    tree_data->SetBranchAddress("PV_z", &pv_z);

    auto hists_tof_minus_bs = std::vector<std::vector<TH1D *>>(16, std::vector<TH1D *>(16));
    auto shifts_tof = std::vector<std::vector<float>>(16, std::vector<float>(16));
    auto stations = std::vector<std::string>{"A", "C"};
    auto channels = std::vector<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    auto params = std::vector<std::vector<float>>(2, std::vector<float>(16));

    for (size_t i = 0; i < 16; i++)
        for (size_t j = 0; j < 16; j++)
            hists_tof_minus_bs[i][j] = (TH1D*)inf_sh->Get(("tof_bs" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str());

    auto fitter_simple = [&](TH1D *hist)
    {
        if (hist->GetEntries() > 0)
        {
            TF1* gaus1 = new TF1("gaus1", "gaus");
            gaus1->SetLineColor(2);    
            TFitResultPtr r = hist->Fit("gaus1","S");
            double mean = r->Parameters()[1];
            delete gaus1;

            return mean;
        }
        else
        {return 0.0;}   
    };

    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
            shifts_tof[i][j]=fitter_simple(hists_tof_minus_bs[i][j]);

    //parametrization from 256 pairs to 32 channels
    TH1D *h1_shifts = new TH1D("h1_shifts", "h1_shifts", 256, 0, 256);
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
            h1_shifts->SetBinContent((i*16+j+1), shifts_tof[i][j]);

    //get 32 params
    TF1 * tf_parsmetrazation = new TF1("tf_parsmetrazation",myFunc, 0.1,255.9,32);
    for (int i = 0; i < 32; i++)
        tf_parsmetrazation->SetParameter(i, 1500);

    tf_parsmetrazation->SetNpx(1000);
    h1_shifts->Fit(tf_parsmetrazation,"R");

    for (int i = 0; i < 2; i++)
        for (int j = 0; j < 16; j++)
            params[i][j] = tf_parsmetrazation->GetParameter(i*16+j);

    for (int i = 0; i < 2; i++)
        for (int j = 0; j < 16; j++)
            params[i][j]=params[i][j]*2/(speed_of_light*pow(10,-6));


    auto n_bin = 150;
    TH1D *h_pv_atlas = new TH1D("pv_atlas", "pv_atlas", n_bin, -250, 250);
    TH1D *h_tof_vertex = new TH1D("vertex_tof", "vertex_tof", n_bin, -250, 250);
    TH1D *h_atlas_minus_tof = new TH1D("atlas_minus_tof", "atlas_minus_tof", n_bin*2, -250, 250);
    //TH1D *h_atlas_minus_tof = new TH1D("atlas_minus_tof", "atlas_minus_tof", 420, 2300, 3000); //before calib
    TH1D *h_atlas_minus_tof_close = new TH1D("atlas_minus_tof_close", "atlas_minus_tof_close", n_bin*2, -50, 50);
    
    float tof_vertex=0;
    float time=0;
    float tof_minus_bs;
    int chan;
    int i_A;
    int i_C;
    float ver_time_A=0;
    float ver_time_C=0;
    
    //main loop
    for (size_t i = 0; i < nentries; i++)
    {   
        if (i % 3000000 == 0)
            std::cout << i/3000000 << " from " << nentries/3000000 << "\n";

        tree_data->GetEntry(i);
            
        if (toFHit_channel->size() == 0 || aFPTrack_xLocal->size() == 0)
            continue;

        // cut in only 1 track per event
        int sum_tracks_A = 0, sum_tracks_C = 0;
        for (size_t j = 0; j < aFPTrack_stationID->size(); j++)
        {
            if (aFPTrack_stationID->at(j) == 0)
                sum_tracks_A++;
            if (aFPTrack_stationID->at(j) == 3)
                sum_tracks_C++;
        }
        if (sum_tracks_A != 1 || sum_tracks_C != 1)
            continue;

        //cut on only 1 train per event
        std::vector<int> vec_train_A(4, 0), vec_train_C(4, 0);
        for (size_t j = 0; j < toFHit_trainID->size(); j++)
        {
            for (size_t h = 0; h < 4; h++)
            {
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 0)
                    vec_train_A[h] = 1;
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 3)
                    vec_train_C[h] = 1;
            }
        }
        int sum_train_A = 0, sum_train_C = 0;
        for (size_t h = 0; h < 4; h++)
        {
            sum_train_A += vec_train_A[h];
            sum_train_C += vec_train_C[h];
        }
        if ( (sum_train_A != 1) || (sum_train_C != 1) ) 
            continue;

        //cut on arrival time
        int count_times=0;
        for (size_t count = 0; count < toFHit_channel->size(); ++count)
        {
            time=0;
            if (toFHit_channel->at(count)>=16)
                continue;
            if (toFHit_stationID->at(count) == 0)
            {
                time = toFHit_time->at(count);
                if (1024 / 25 * time > 1000 || 1024 / 25 * time < 900) 
                    count_times++;
            }
            if (toFHit_stationID->at(count) == 3)
            {
                time = toFHit_time->at(count);
                if (1024 / 25 * time > 300 || 1024 / 25 * time < 200) 
                    count_times++;
            }
        }
        if (count_times>0)
            continue;

        //cut on only 1 hit in 1 bar
        auto count_bars_A = std::vector<int>{0, 0, 0, 0};
        auto count_bars_C = std::vector<int>{0, 0, 0, 0};
        for (size_t j = 0; j < toFHit_bar->size(); j++)
        {
            if (toFHit_stationID->at(j) == 0)
            {
                size_t k = toFHit_bar->at(j);
                count_bars_A[k]++;
            }
            if (toFHit_stationID->at(j) == 3)
            {
                size_t k = toFHit_bar->at(j);
                count_bars_C[k]++;
            }
        }
        int count_hitsInBars=0;
        for (size_t k = 0; k < 4; k++)              
            if (count_bars_A[k] > 1 || count_bars_C[k] > 1)
                count_hitsInBars++;
        if (count_hitsInBars>0)
            continue;   

        //prep for all chans tof vertex, separate A and C
        std::vector<float> chans_A, chans_C, times_A(16,-1), times_C(16,-1);
        for (size_t j = 0; j < toFHit_channel->size(); ++j)
        {
            chan = toFHit_channel->at(j);
            if (toFHit_stationID->at(j) == 0)
            {
                chans_A.push_back(chan);
                times_A[chan]=toFHit_time->at(j);
            }
            if (toFHit_stationID->at(j) == 3)
            {
                chans_C.push_back(chan);
                times_C[chan]=toFHit_time->at(j);
            }
        }

        //all chans, no event mixing for pv-tof
        for (size_t k = 0; k < pv_vertexType->size(); k++)
        {
            if (pv_vertexType->at(k)==1 && chans_A.size()!=0 && chans_C.size()!=0)
            {
                i_A=0;
                i_C=0;
                ver_time_A=0;
                ver_time_C=0;
                for (size_t j = 0; j < chans_A.size(); ++j)
                {
                    i_A=chans_A.at(j);
                    ver_time_A = ver_time_A + ( times_A.at(i_A) - params[0][i_A] );
                    //ver_time_A = ver_time_A + times_A.at(i_A);
                }
                ver_time_A = ver_time_A/chans_A.size();
                for (size_t j = 0; j < chans_C.size(); ++j)
                {
                    i_C=chans_C.at(j);
                    ver_time_C = ver_time_C + ( times_C.at(i_C) - params[1][i_C] );
                    //ver_time_C = ver_time_C + times_C.at(i_C);
                }
                ver_time_C = ver_time_C/chans_C.size();
                
                tof_vertex = (ver_time_C-ver_time_A)*speed_of_light/2*pow(10,-6); 
                h_pv_atlas->Fill(pv_z->at(k));
                h_tof_vertex->Fill(tof_vertex);
                h_atlas_minus_tof->Fill(((pv_z->at(k))-(tof_vertex)));
                h_atlas_minus_tof_close->Fill(((pv_z->at(k))-(tof_vertex)));
                
                /*
                for (size_t j = 0; j < chans_A.size(); ++j)
                    for (size_t h = 0; h < chans_C.size(); ++h)
                    {
                        i_A=chans_A.at(j);
                        ver_time_A = times_A.at(i_A) - params[0][i_A];
                        i_C=chans_C.at(h);
                        ver_time_C = times_C.at(i_C) - params[1][i_C];
                        tof_vertex = (ver_time_C-ver_time_A)*speed_of_light/2*pow(10,-6);
                        h_atlas_minus_tof->Fill(((pv_z->at(k))-(tof_vertex)));
                    }
                */
            }
        }
        //clear prep for all chans tof vertex
        chans_A.clear();
        chans_C.clear();
    }
    
    
    TFile *outf4 = new TFile((std::string("out_ME_atlas_minus_tof_") + std::to_string(runNumber) + std::string(".root")).c_str(), "RECREATE");
    h_atlas_minus_tof->Draw();
    h_atlas_minus_tof->Write();  
    outf4->Close();

}