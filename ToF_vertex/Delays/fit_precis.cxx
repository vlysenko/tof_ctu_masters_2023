#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <limits>

#include <TRandom3.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TFormula.h>
#include <TF1.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TColor.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TLatex.h>
#include <TMath.h>

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

Double_t myFunc (Double_t *x, Double_t *pars) 
    {
        Float_t xx =x[0];
        Double_t f = 0;
        for (size_t i = 0; i < 16; i++)
            for (size_t j = 0; j < 16; j++)
                f = f + ( ((xx>i*16+j) && (xx<i*16+j+1)) * (pars[16+j]-pars[i]));      
        return f;
    }

int main(int argc, char **argv)
{

    if (argc != 2)
    {
        std::cout << "Requires 2 parameters\n";
        return 1;
    }

    // setup
    SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c1","Cc1",0.,0.,800,600);
    TFile *inf_sh = new TFile(argv[1], "READ"); //tof minus bs, produced in "all_tof_newT.cxx"

    std::vector<float> *toFHit_channel = nullptr, *toFHit_trainID = nullptr, *toFHit_stationID = nullptr, 
                       *toFHit_time = nullptr, *toFHit_bar = nullptr, *pv_z = nullptr, *pv_vertexType = nullptr,
                       *aFPTrack_xLocal = nullptr, *aFPTrack_stationID = nullptr;

    int runNumber = 428770;
    //int runNumber=429027;
    int eventNumber;
    int lumi_block;
    float beam_pos;
    float speed_of_light = TMath::C();

    auto hists_tof_minus_bs = std::vector<std::vector<TH1D *>>(16, std::vector<TH1D *>(16));
    auto shifts_tof = std::vector<std::vector<float>>(16, std::vector<float>(16));
    auto stations = std::vector<std::string>{"A", "C"};
    auto channels = std::vector<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    auto params = std::vector<std::vector<float>>(2, std::vector<float>(16));

    for (size_t i = 0; i < 16; i++)
        for (size_t j = 0; j < 16; j++)
            hists_tof_minus_bs[i][j] = (TH1D*)inf_sh->Get(("tof_bs" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str());

    auto fitter_simple = [&](TH1D *hist)
    {
        if (hist->GetEntries() > 0)
        {
            TF1* gaus1 = new TF1("gaus1", "gaus");
            gaus1->SetLineColor(2);    
            TFitResultPtr r = hist->Fit("gaus1","S");
            double mean = r->Parameters()[1];
            delete gaus1;

            return mean;
        }
        else
        {return 0.0;}   
    };

    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
            shifts_tof[i][j]=fitter_simple(hists_tof_minus_bs[i][j]);

    //parametrization from 256 pairs to 32 channels
    TH1D *h1_shifts = new TH1D("h1_shifts", "h1_shifts", 256, 0, 256);
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
            h1_shifts->SetBinContent((i*16+j+1), shifts_tof[i][j]);

    //get 32 params
    TF1 * tf_parsmetrazation = new TF1("tf_parsmetrazation",myFunc, 0.1,255.9,32);
    for (int i = 0; i < 32; i++)
        tf_parsmetrazation->SetParameter(i, 1500);

    tf_parsmetrazation->SetNpx(1000);
    h1_shifts->Fit(tf_parsmetrazation,"R");

    for (int i = 0; i < 2; i++)
        for (int j = 0; j < 16; j++)
            params[i][j] = tf_parsmetrazation->GetParameter(i*16+j);

    float a=0;
    TH1D *h1_precis = new TH1D("h1_precis", "h1_precis", 256, 0, 256);
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
        {
            a=shifts_tof[i][j]-(params[1][j]-params[0][i]);
            a=a*2/(speed_of_light*pow(10,-6));
            h1_precis->SetBinContent((i*16+j+1), a);
        }

    h1_precis->GetXaxis()->SetTitle("Index");
    h1_precis->GetYaxis()->SetTitle("Mean - (D_{C_{j}} - D_{A_{i}}) [mm]");  
    h1_precis->GetYaxis()->SetRangeUser(-0.01,0.01); 
    h1_precis->SetMarkerColor(65);
    h1_precis->SetMarkerStyle(21);

    TGraph *gr = new TGraph();
    gr->AddPoint(0,0);
    gr->AddPoint(260,0);
    
    auto drawer = [&](TH1D *hist)
    {
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        hist->Draw("P");
        gr->Draw("sameL");
        ATLASLabel_WithoutATLAS(0.2,0.95,"Internal");
        latex.DrawLatex(0.2,0.85,(std::string("Run ") + std::to_string(runNumber)).c_str());
        c1->SaveAs((std::string("fit_precision_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    };
    
    c1->SaveAs((std::string("fit_precision_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    drawer(h1_precis);
    c1->SaveAs((std::string("fit_precision_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());

}