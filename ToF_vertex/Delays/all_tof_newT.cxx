#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <limits>

#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TColor.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TRandom3.h>
#include <TMath.h>
#include <TGraph.h>
#include <TMultiGraph.h>

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"


int main(int argc, char **argv)
{
    if (argc != 3)
    {
        std::cout << "Requires 3 parameters\n";
        return 1;
    }

    // setup
    SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c1","Cc1",0.,0.,800,600);

    TFile *inf = new TFile(argv[1], "READ"); //data file
    TTree *tree_data = inf->Get<TTree>("CollectionTree");
    size_t nentries = tree_data->GetEntries();

    bool ATLASkey;
    std::string arg1(argv[2]);

    if (arg1=="atlas") {ATLASkey = true;}
    if (arg1=="noatlas") {ATLASkey = false;}

    auto n_bin = 200;
    auto n_bin_tof_lb_42_1=400;
    auto n_bin_tof_lb_42_2=1000;

    TH2D *h2_tof_lb_42 = new TH2D("2_tof_lb_42", "2_tof_lb_42", n_bin_tof_lb_42_1, 0, 330, n_bin_tof_lb_42_2, -500, 600);
    TH2D *h2_tof_lb_before_42 = new TH2D("2_tof_lb_before_42", "2_tof_lb_before_42", n_bin_tof_lb_42_1, 0, 330, n_bin_tof_lb_42_2, -3000, 100);

    std::vector<float> *toFHit_channel = nullptr, *toFHit_trainID = nullptr, *toFHit_stationID = nullptr, 
                       *toFHit_time = nullptr, *toFHit_bar = nullptr, *pv_z = nullptr, *pv_vertexType = nullptr,
                       *aFPTrack_xLocal = nullptr, *aFPTrack_stationID = nullptr;

    int runNumber;
    int eventNumber;
    int lumi_block;
    float beam_pos;

    tree_data->SetBranchAddress("ToFHit_channel", &toFHit_channel);
    tree_data->SetBranchAddress("ToFHit_trainID", &toFHit_trainID);
    tree_data->SetBranchAddress("ToFHit_stationID", &toFHit_stationID);
    tree_data->SetBranchAddress("ToFHit_time", &toFHit_time);
    tree_data->SetBranchAddress("ToFHit_bar", &toFHit_bar);
    tree_data->SetBranchAddress("AFPTrack_xLocal", &aFPTrack_xLocal);
    tree_data->SetBranchAddress("AFPTrack_stationID", &aFPTrack_stationID);
    tree_data->SetBranchAddress("RunNumber", &runNumber);
    tree_data->SetBranchAddress("EventNumber", &eventNumber);
    tree_data->SetBranchAddress("lumiBlock", &lumi_block);
    tree_data->SetBranchAddress("beam_pos_z", &beam_pos);
    tree_data->SetBranchAddress("PV_vertexType", &pv_vertexType);
    tree_data->SetBranchAddress("PV_z", &pv_z);

    float tof_vertex=0;
    float time=0;
    float speed_of_light = TMath::C();
    float tof_minus_bs;

    TGraph *g_beam_z_lb = new TGraph();

    int chan;
    int i_A;
    int i_C;

    tree_data->GetEntry(1);
    int lumi_block_prev=lumi_block;

    auto stations = std::vector<std::string>{"A", "C"};
    auto channels = std::vector<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    auto hists_tof_vertex = std::vector<std::vector<TH1D *>>(16, std::vector<TH1D *>(16));
    auto hists_tof_minus_bs = std::vector<std::vector<TH1D *>>(16, std::vector<TH1D *>(16));
    auto hists2D_tof_lb_42 = std::vector<std::vector<TH2D *>>(16, std::vector<TH2D *>(16));

    auto shifts_tof = std::vector<std::vector<float>>(16, std::vector<float>(16));
    auto buffer_times_A = std::vector<std::vector<float>>(16, std::vector<float>());
    auto buffer_times_C = std::vector<std::vector<float>>(16, std::vector<float>());
    
    for (size_t i = 0; i < 16; i++)
        for (size_t j = 0; j < 16; j++)
        {
            hists_tof_vertex[i][j] = new TH1D(("tof" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str(), 
            ("tof" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str(), n_bin, -3200, -2000);
            
            hists_tof_minus_bs[i][j] = new TH1D(("tof_bs" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str(), 
            ("tof_bs" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str(), n_bin, -3200, -2000);
            
            hists2D_tof_lb_42[i][j] = new TH2D(("2D_tof_lb_42" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str(), 
            ("2D_tof_lb_42" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str(), 
            n_bin_tof_lb_42_1, 200, 1000, n_bin_tof_lb_42_2, -500, 600);
        }
            

    //1st main loop
    for (size_t i = 0; i < nentries; i++)
    {   
        if (i % 3000000 == 0)
            std::cout << i/3000000 << " from " << nentries/3000000 << "\n";

        tree_data->GetEntry(i);

        if (toFHit_channel->size() == 0 || aFPTrack_xLocal->size() == 0)
            continue;

        // cut in only 1 track per event
        int sum_tracks_A = 0, sum_tracks_C = 0;
        for (size_t j = 0; j < aFPTrack_stationID->size(); j++)
        {
            if (aFPTrack_stationID->at(j) == 0)
                sum_tracks_A++;
            if (aFPTrack_stationID->at(j) == 3)
                sum_tracks_C++;
        }
        if (sum_tracks_A > 1 || sum_tracks_C > 1)
            continue;

        //cut on only 1 train per event
        std::vector<int> vec_train_A(4, 0), vec_train_C(4, 0);
        for (size_t j = 0; j < toFHit_trainID->size(); j++)
        {
            for (size_t h = 0; h < 4; h++)
            {
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 0)
                    vec_train_A[h] = 1;
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 3)
                    vec_train_C[h] = 1;
            }
        }
        int sum_train_A = 0, sum_train_C = 0;
        for (size_t h = 0; h < 4; h++)
        {
            sum_train_A += vec_train_A[h];
            sum_train_C += vec_train_C[h];
        }
        if (sum_train_A > 1 || sum_train_C > 1)
            continue;

        //cut on arrival time
        int count_times=0;
        for (size_t count = 0; count < toFHit_channel->size(); ++count)
        {
            time=0;
            if (toFHit_channel->at(count)>=16)
                continue;
            if (toFHit_stationID->at(count) == 0)
            {
                time = toFHit_time->at(count);
                if (1024 / 25 * time > 1000 || 1024 / 25 * time < 900) 
                    count_times++;
            }
            if (toFHit_stationID->at(count) == 3)
            {
                time = toFHit_time->at(count);
                if (1024 / 25 * time > 300 || 1024 / 25 * time < 200) 
                    count_times++;
            }
        }
        if (count_times>0)
            continue;

        //cut on only 1 hit in 1 bar
        auto count_bars_A = std::vector<int>{0, 0, 0, 0};
        auto count_bars_C = std::vector<int>{0, 0, 0, 0};
        for (size_t j = 0; j < toFHit_bar->size(); j++)
        {
            if (toFHit_stationID->at(j) == 0)
            {
                size_t k = toFHit_bar->at(j);
                count_bars_A[k]++;
            }
            if (toFHit_stationID->at(j) == 3)
            {
                size_t k = toFHit_bar->at(j);
                count_bars_C[k]++;
            }
        }
        int count_hitsInBars=0;
        for (size_t k = 0; k < 4; k++)              
            if (count_bars_A[k] > 1 || count_bars_C[k] > 1)
                count_hitsInBars++;
        if (count_hitsInBars>0)
            continue;   
        
        //prep for all chans tof vertex, separate A and C
        std::vector<float> chans_A, chans_C, times_A(16,-1), times_C(16,-1);
        for (size_t j = 0; j < toFHit_channel->size(); ++j)
        {
            chan = toFHit_channel->at(j);
            if (toFHit_stationID->at(j) == 0 && sum_tracks_A==1 && chan < 16 && sum_train_A==1)
            {
                chans_A.push_back(chan);
                times_A[chan]=toFHit_time->at(j);
            }
            if (toFHit_stationID->at(j) == 3 && sum_tracks_C==1 && chan < 16 && sum_train_C==1)
            {
                chans_C.push_back(chan);
                times_C[chan]=toFHit_time->at(j);
            }
        }

        //graph bs vs lb
        g_beam_z_lb->AddPoint(double(lumi_block),double(beam_pos));
        
        //all chans event mixing: tof vertex, tof-bs 
        if (lumi_block != lumi_block_prev)
        {
            for (size_t j = 0; j < 16; ++j)
                for (size_t h = 0; h < 16; ++h)
                    for (size_t buf_k_A = 0; buf_k_A < buffer_times_A[j].size(); ++buf_k_A)
                        for (size_t buf_k_C = 0; buf_k_C < buffer_times_C[h].size(); ++buf_k_C)
                        {
                            tof_vertex=(buffer_times_C[h].at(buf_k_C)-buffer_times_A[j].at(buf_k_A))*1000*speed_of_light/2*pow(10,-9);
                            hists_tof_vertex[j][h]->Fill(tof_vertex);
                            tof_minus_bs=float(tof_vertex)-float(beam_pos);
                            hists_tof_minus_bs[j][h]->Fill(tof_minus_bs);
                        }
            for (size_t j = 0; j < 16; ++j)
            {
                buffer_times_A[j].clear();
                buffer_times_C[j].clear();
            }
        }
        
        lumi_block_prev=lumi_block;
        for (size_t k = 0; k < chans_A.size(); ++k)
        {
            i_A=chans_A.at(k);
            if (buffer_times_A[i_A].size()<1000000)
                buffer_times_A[i_A].push_back(times_A.at(i_A));
        }
        for (size_t k = 0; k < chans_C.size(); ++k)
        {
            i_C=chans_C.at(k);
            if (buffer_times_C[i_C].size()<1000000)
                buffer_times_C[i_C].push_back(times_C.at(i_C));
        }

        //clear prep for all chans tof vertex
        chans_A.clear();
        chans_C.clear();
    }


    auto fitter_simple = [&](TH1D *hist)
    {
        if (hist->GetEntries() > 0)
        {
            TF1* gaus1 = new TF1("gaus1", "gaus");
            gaus1->SetLineColor(2);    
            TFitResultPtr r = hist->Fit("gaus1","S");
            double mean = r->Parameters()[1];
            delete gaus1;

            return mean;
        }
        else
        {return 0.0;}   
    };
    
    TFile *outf2 = new TFile((std::string("out_ME_tof_minus_bs_") + std::to_string(runNumber) + std::string(".root")).c_str(), "RECREATE");
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
        {
            hists_tof_minus_bs[i][j]->Draw();
            hists_tof_minus_bs[i][j]->Write();
        }
    outf2->Close();
    
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
            shifts_tof[i][j]=fitter_simple(hists_tof_minus_bs[i][j]);
   
    //2nd main loop for new z_tof
    tree_data->GetEntry(1);
    lumi_block_prev=lumi_block;

    for (size_t j = 0; j < 16; ++j)
    {
        buffer_times_A[j].clear();
        buffer_times_C[j].clear();
    }
    
    for (size_t i = 0; i < nentries; i++)
    {   
        if (i % 3000000 == 0)
            std::cout << i/3000000 << " from " << nentries/3000000 << "\n";

        tree_data->GetEntry(i);

        if (toFHit_channel->size() == 0 || aFPTrack_xLocal->size() == 0)
            continue;

        // cut in only 1 track per event
        int sum_tracks_A = 0, sum_tracks_C = 0;
        for (size_t j = 0; j < aFPTrack_stationID->size(); j++)
        {
            if (aFPTrack_stationID->at(j) == 0)
                sum_tracks_A++;
            if (aFPTrack_stationID->at(j) == 3)
                sum_tracks_C++;
        }
        if (sum_tracks_A > 1 || sum_tracks_C > 1)
            continue;

        //cut on only 1 train per event
        std::vector<int> vec_train_A(4, 0), vec_train_C(4, 0);
        for (size_t j = 0; j < toFHit_trainID->size(); j++)
        {
            for (size_t h = 0; h < 4; h++)
            {
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 0)
                    vec_train_A[h] = 1;
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 3)
                    vec_train_C[h] = 1;
            }
        }
        int sum_train_A = 0, sum_train_C = 0;
        for (size_t h = 0; h < 4; h++)
        {
            sum_train_A += vec_train_A[h];
            sum_train_C += vec_train_C[h];
        }
        if ( (sum_train_A > 1) || (sum_train_C > 1) ) 
            continue;

        //cut on arrival time
        int count_times=0;
        for (size_t count = 0; count < toFHit_channel->size(); ++count)
        {
            time=0;
            if (toFHit_channel->at(count)>=16)
                continue;
            if (toFHit_stationID->at(count) == 0)
            {
                time = toFHit_time->at(count);
                if (1024 / 25 * time > 1000 || 1024 / 25 * time < 900) 
                    count_times++;
            }
            if (toFHit_stationID->at(count) == 3)
            {
                time = toFHit_time->at(count);
                if (1024 / 25 * time > 300 || 1024 / 25 * time < 200) 
                    count_times++;
            }
        }
        if (count_times>0)
            continue;

        //cut on only 1 hit in 1 bar
        auto count_bars_A = std::vector<int>{0, 0, 0, 0};
        auto count_bars_C = std::vector<int>{0, 0, 0, 0};
        for (size_t j = 0; j < toFHit_bar->size(); j++)
        {
            if (toFHit_stationID->at(j) == 0)
            {
                size_t k = toFHit_bar->at(j);
                count_bars_A[k]++;
            }
            if (toFHit_stationID->at(j) == 3)
            {
                size_t k = toFHit_bar->at(j);
                count_bars_C[k]++;
            }
        }
        int count_hitsInBars=0;
        for (size_t k = 0; k < 4; k++)              
            if (count_bars_A[k] > 1 || count_bars_C[k] > 1)
                count_hitsInBars++;
        if (count_hitsInBars>0)
            continue;   

        //prep for all chans tof vertex, separate A and C
        std::vector<float> chans_A, chans_C, times_A(16,-1), times_C(16,-1);
        for (size_t j = 0; j < toFHit_channel->size(); ++j)
        {
            chan = toFHit_channel->at(j);
            if (toFHit_stationID->at(j) == 0 && sum_tracks_A==1 && chan < 16 && sum_train_A==1)
            {
                chans_A.push_back(chan);
                times_A[chan]=toFHit_time->at(j);
            }
            if (toFHit_stationID->at(j) == 3 && sum_tracks_C==1 && chan < 16 && sum_train_C==1)
            {
                chans_C.push_back(chan);
                times_C[chan]=toFHit_time->at(j);
            }
        }

        //all chans event mixing new tof vertex 
        if (lumi_block != lumi_block_prev)
        {
            for (size_t j = 0; j < 16; ++j)
                for (size_t h = 0; h < 16; ++h)
                    for (size_t buf_k_A = 0; buf_k_A < buffer_times_A[j].size(); ++buf_k_A)
                        for (size_t buf_k_C = 0; buf_k_C < buffer_times_C[h].size(); ++buf_k_C)
                        {
                            tof_vertex=(buffer_times_C[h].at(buf_k_C)-buffer_times_A[j].at(buf_k_A))*1000*speed_of_light/2*pow(10,-9);
                            hists2D_tof_lb_42[j][h]->Fill(lumi_block,tof_vertex-shifts_tof[j][h]);
                            h2_tof_lb_42->Fill(lumi_block,tof_vertex-shifts_tof[j][h]);
                            h2_tof_lb_before_42->Fill(lumi_block,tof_vertex);
                        }
            for (size_t j = 0; j < 16; ++j)
            {
                buffer_times_A[j].clear();
                buffer_times_C[j].clear();
            }
        }
        
        lumi_block_prev=lumi_block;
        for (size_t k = 0; k < chans_A.size(); ++k)
        {
            i_A=chans_A.at(k);
            //if (buffer_times_A[i_A].size()<1000000)
                buffer_times_A[i_A].push_back(times_A.at(i_A));
        }
        for (size_t k = 0; k < chans_C.size(); ++k)
        {
            i_C=chans_C.at(k);
            //if (buffer_times_C[i_C].size()<1000000)
                buffer_times_C[i_C].push_back(times_C.at(i_C));
        }

        //clear prep for all chans tof vertex
        chans_A.clear();
        chans_C.clear();
    }
    
    g_beam_z_lb->SetName("g_beam_lb");
    
    //drawing  
    TFile *outf4 = new TFile((std::string("out_ME_newOne_tof_") + std::to_string(runNumber) + std::string(".root")).c_str(), "RECREATE");
    h2_tof_lb_42->Draw();
    h2_tof_lb_42->Write();  

    g_beam_z_lb->Draw("same");
    g_beam_z_lb->Write();
    outf4->Close();

    TFile *outf41 = new TFile((std::string("out_ME_oldOne_tof_") + std::to_string(runNumber) + std::string(".root")).c_str(), "RECREATE");
    h2_tof_lb_before_42->Draw();
    h2_tof_lb_before_42->Write();  

    g_beam_z_lb->Draw("same");
    g_beam_z_lb->Write();
    outf41->Close();
    
}